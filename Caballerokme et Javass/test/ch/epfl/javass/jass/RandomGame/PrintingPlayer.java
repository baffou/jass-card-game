package ch.epfl.javass.jass.RandomGame;

import java.util.Map;
import java.util.Map.Entry;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.Card.Color;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.Player;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.Score;
import ch.epfl.javass.jass.TeamId;
import ch.epfl.javass.jass.Trick;
import ch.epfl.javass.jass.TurnState;

public final class PrintingPlayer implements Player {
	private final Player underlyingPlayer;

	public PrintingPlayer(Player underlyingPlayer) {
		this.underlyingPlayer = underlyingPlayer;
	}

	/*
	 * L'utilité des prochaines méthodes sont décrites dans l'interface Player
	 */
	public void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
		System.out.println("Les joueurs sont : ");
		for (Entry<PlayerId, String> entry : playerNames.entrySet()) {
			if (entry.getKey() == ownId)
				System.out.println(entry.getKey() + "  (moi)");
			else
				System.out.println(entry.getKey());
		}
	}

	@Override
	public Card cardToPlay(TurnState state, CardSet hand) {
		System.out.print("C'est à moi de jouer... Je joue : ");
		Card c = underlyingPlayer.cardToPlay(state, hand);
		System.out.println(c);
		return c;
	}

	public void updateHand(CardSet newHand) {
		System.out.print("Ma main est faîte : ");
		System.out.println(newHand.toString());
	}

	public void setTrump(Color trump) {
		System.out.print("Le trump est set :  ");
		System.out.println(trump.toString());
	}

	public void updateTrick(Trick newTrick) {
		System.out.print("Le trick est updaté :  ");
		System.out.println(newTrick.toString());
	}

	public void updateScore(Score score) {
		System.out.print("Le score est updaté :  ");
		System.out.println(score);
	}

	public void setWinningTeam(TeamId winningTeam) {
		System.out.print("La winning Team est  :  ");
		System.out.println(winningTeam.toString());
	}

}
