package ch.epfl.javass.jass;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.SplittableRandom;

import org.junit.jupiter.api.Test;

import ch.epfl.test.TestRandomizer;

public class PackedScoreTest {

    static {
        int totalCardsPoints = 0;
        for (Card.Color color : Card.Color.ALL) {
            for (Card.Rank rank : Card.Rank.ALL) {
                totalCardsPoints += Card.of(color, rank)
                        .points(Card.Color.SPADE);
            }
        }

        TOTAL_POINTS_IF_MATCH = totalCardsPoints + Jass.MATCH_ADDITIONAL_POINTS
                + Jass.LAST_TRICK_ADDITIONAL_POINTS;

    }

    static final int TOTAL_POINTS_IF_MATCH;

    static int[] randomValidScore(SplittableRandom rand) {
        int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN + 1);
        int tricks2 = rand.nextInt(Jass.TRICKS_PER_TURN + 1);
        int points1 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1);
        int points2 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1);
        int game1 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1);
        int game2 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1);
        return new int[] { tricks1, points1, game1, tricks2, points2, game2 };
    }

    static long randomValidPackedScore(SplittableRandom rand) {
        int[] values = randomValidScore(rand);
        return PackedScore.pack(values[0], values[1], values[2], values[3], values[4], values[5]);
    }

        @Test
    public void initialWorks() {
        assertEquals(0, PackedScore.INITIAL);
    }

    @Test
    public void isValidWorksForAllValidScore() {
        SplittableRandom rand = TestRandomizer.newRandom();

        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN + 1);
            int tricks2 = rand.nextInt(Jass.TRICKS_PER_TURN + 1);
            int points1 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1);
            int points2 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1);
            int game1 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1);
            int game2 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1);
            assertTrue(PackedScore.isValid(PackedScore.pack(tricks1, points1,
                    game1, tricks2, points2, game2)));
        }
    }

    @Test
    public void isValidWorksWithInvalidScore() {
        SplittableRandom rand = TestRandomizer.newRandom();

        // Invalid tricks
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidScore(rand);
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN + 1, 16);
            int tricks2 = rand.nextInt(Jass.TRICKS_PER_TURN + 1, 16);
            assertFalse(PackedScore.isValid(PackedScore.pack(tricks1, values[1],
                    values[2], tricks2, values[4], values[5])));
        }

        // Invalid points
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidScore(rand);
            int points1 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1, 512);
            int points2 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1, 512);
            assertFalse(PackedScore.isValid(PackedScore.pack(values[0], points1,
                    values[2], values[3], points2, values[5])));
        }

        // Invalid game
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidScore(rand);
            int game1 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1, 2048);
            int game2 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1, 2048);
            assertFalse(PackedScore.isValid(PackedScore.pack(values[0],
                    values[1], game1, values[3], values[4], game2)));
        }

        // invalid 0
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long score1 = rand.nextLong(0b11111111111111111111111,
                    Integer.MAX_VALUE);
            long score2 = rand.nextLong(0b11111111111111111111111,
                    Integer.MAX_VALUE);
            assertFalse(PackedScore.isValid(score2 << 32 | score1));
        }
    }

    @Test
    public void packAndTrickAndPointsAndGameAndTotalWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();

        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidScore(rand);
            long packed = PackedScore.pack(values[0], values[1], values[2],
                    values[3], values[4], values[5]);
            assertEquals(values[0],
                    PackedScore.turnTricks(packed, TeamId.TEAM_1));
            assertEquals(values[3],
                    PackedScore.turnTricks(packed, TeamId.TEAM_2));

            assertEquals(values[1],
                    PackedScore.turnPoints(packed, TeamId.TEAM_1));
            assertEquals(values[4],
                    PackedScore.turnPoints(packed, TeamId.TEAM_2));

            assertEquals(values[2],
                    PackedScore.gamePoints(packed, TeamId.TEAM_1));
            assertEquals(values[5],
                    PackedScore.gamePoints(packed, TeamId.TEAM_2));

            assertEquals(values[1] + values[2],
                    PackedScore.totalPoints(packed, TeamId.TEAM_1));
            assertEquals(values[4] + values[5],
                    PackedScore.totalPoints(packed, TeamId.TEAM_2));
        }
    }

    @Test
    public void withAdditionalTrickWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();

        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN);
            int tricks2 = rand.nextInt(Jass.TRICKS_PER_TURN);
            int teamToWin = rand.nextInt(2);
            boolean matchTrick = teamToWin == 0
                    ? tricks1 + 1 >= Jass.TRICKS_PER_TURN
                    : tricks2 + 1 >= Jass.TRICKS_PER_TURN;
            int maxPointsBeforeTrick = matchTrick
                    ? (TOTAL_POINTS_IF_MATCH - Jass.MATCH_ADDITIONAL_POINTS)
                    : TOTAL_POINTS_IF_MATCH;
            int points1 = rand.nextInt(maxPointsBeforeTrick + 1);
            int points2 = rand.nextInt(maxPointsBeforeTrick + 1);
            int trickPoints = rand.nextInt(Math.max(0,
                    maxPointsBeforeTrick - (teamToWin == 0 ? points1 : points2))
                    + 1);
            int game1 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1);
            int game2 = rand.nextInt(Jass.WINNING_POINTS * 2 + 1);

            long packed = PackedScore
                    .withAdditionalTrick(
                            PackedScore.pack(tricks1, points1, game1, tricks2,
                                    points2, game2),
                            TeamId.values()[teamToWin], trickPoints);

            // Match trick
            if (matchTrick)
                trickPoints += Jass.MATCH_ADDITIONAL_POINTS;

            assertEquals(tricks1 + (teamToWin == 0 ? 1 : 0),
                    PackedScore.turnTricks(packed, TeamId.TEAM_1));
            assertEquals(tricks2 + (teamToWin == 0 ? 0 : 1),
                    PackedScore.turnTricks(packed, TeamId.TEAM_2));

            assertEquals(points1 + (teamToWin == 0 ? trickPoints : 0),
                    PackedScore.turnPoints(packed, TeamId.TEAM_1));
            assertEquals(points2 + (teamToWin == 0 ? 0 : trickPoints),
                    PackedScore.turnPoints(packed, TeamId.TEAM_2));

            assertEquals(game1, PackedScore.gamePoints(packed, TeamId.TEAM_1));
            assertEquals(game2, PackedScore.gamePoints(packed, TeamId.TEAM_2));
        }
    }

    @Test
    public void nextTurnWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();

        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN + 1);
            // This must be the last trick
            int tricks2 = Jass.TRICKS_PER_TURN;
            int points1 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1);
            int points2 = rand.nextInt(TOTAL_POINTS_IF_MATCH + 1);
            int game1 = rand.nextInt(Jass.WINNING_POINTS * 2 - points1 + 1);
            int game2 = rand.nextInt(Jass.WINNING_POINTS * 2 - points2 + 1);
            long packed = PackedScore.pack(tricks1, points1, game1, tricks2,
                    points2, game2);
            packed = PackedScore.nextTurn(packed);

            assertEquals(0, PackedScore.turnTricks(packed, TeamId.TEAM_1));
            assertEquals(0, PackedScore.turnTricks(packed, TeamId.TEAM_2));

            assertEquals(0, PackedScore.turnPoints(packed, TeamId.TEAM_1));
            assertEquals(0, PackedScore.turnPoints(packed, TeamId.TEAM_2));

            assertEquals(points1 + game1,
                    PackedScore.gamePoints(packed, TeamId.TEAM_1));
            assertEquals(points2 + game2,
                    PackedScore.gamePoints(packed, TeamId.TEAM_2));

        }
    }
}
