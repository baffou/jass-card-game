package ch.epfl.javass.jass;

import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.SplittableRandom;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreTest {

    @Test
    public void initialWorks(){
        assertEquals(PackedScore.INITIAL, Score.INITIAL.packed());
    }

    @Test
    public void ofPackedFailsWithInvalidPacked(){
        SplittableRandom rand = TestRandomizer.newRandom();
        //Invalid tricks
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            int[] values = PackedScoreTest.randomValidScore(rand);
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN + 1, 16);
            int tricks2 = rand.nextInt(Jass.TRICKS_PER_TURN + 1, 16);
            assertFalse(PackedScore.isValid(PackedScore.pack(tricks1, values[1], values[2], tricks2, values[4], values[5])));
        }

        //Invalid points
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            int[] values = PackedScoreTest.randomValidScore(rand);
            int points1 = rand.nextInt(PackedScoreTest.TOTAL_POINTS_IF_MATCH + 1, 512);
            int points2 = rand.nextInt(PackedScoreTest.TOTAL_POINTS_IF_MATCH + 1, 512);
            assertFalse(PackedScore.isValid(PackedScore.pack(values[0], points1, values[2], values[3], points2, values[5])));
        }

        //Invalid game
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            int[] values = PackedScoreTest.randomValidScore(rand);
            int game1 = rand.nextInt(Jass.WINNING_POINTS*2 + 1, 2048);
            int game2 = rand.nextInt(Jass.WINNING_POINTS*2 + 1, 2048);
            assertFalse(PackedScore.isValid(PackedScore.pack(values[0], values[1], game1, values[3], values[4], game2)));
        }
    }

    @Test
    public void packedWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = PackedScoreTest.randomValidScore(rand);
            long packed = PackedScore.pack(values[0], values[1], values[2], values[3], values[4], values[5]);
            Score score = Score.ofPacked(packed);
            assertEquals(packed, score.packed());
        }
    }

    @Test
    public void tricksAnsPointsAndGameAndTotalWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = PackedScoreTest.randomValidScore(rand);
            long packed = PackedScore.pack(values[0], values[1], values[2], values[3], values[4], values[5]);
            Score score = Score.ofPacked(packed);
            assertEquals(values[0], score.turnTricks(TeamId.TEAM_1));
            assertEquals(values[1], score.turnPoints(TeamId.TEAM_1));
            assertEquals(values[2], score.gamePoints(TeamId.TEAM_1));
            assertEquals(values[3], score.turnTricks(TeamId.TEAM_2));
            assertEquals(values[4], score.turnPoints(TeamId.TEAM_2));
            assertEquals(values[5], score.gamePoints(TeamId.TEAM_2));

            assertEquals(values[1] + values[2], score.totalPoints(TeamId.TEAM_1));
            assertEquals(values[4] + values[5], score.totalPoints(TeamId.TEAM_2));
        }
    }

    @Test
    public void withAdditionalTurnWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();

        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN);
            int tricks2 = rand.nextInt(Jass.TRICKS_PER_TURN);
            int teamToWin = rand.nextInt(2);
            boolean matchTrick = teamToWin == 0 ? tricks1 + 1 >= Jass.TRICKS_PER_TURN : tricks2 + 1 >= Jass.TRICKS_PER_TURN;
            int maxPointsBeforeTrick = matchTrick ? (PackedScoreTest.TOTAL_POINTS_IF_MATCH - Jass.MATCH_ADDITIONAL_POINTS) : PackedScoreTest.TOTAL_POINTS_IF_MATCH;
            int points1 = rand.nextInt(maxPointsBeforeTrick + 1);
            int points2 = rand.nextInt(maxPointsBeforeTrick + 1);
            int trickPoints = rand.nextInt(Math.max(0, maxPointsBeforeTrick - (teamToWin == 0 ? points1 : points2)) + 1);
            int game1 = rand.nextInt(Jass.WINNING_POINTS*2 + 1);
            int game2 = rand.nextInt(Jass.WINNING_POINTS*2 + 1);

            long packed = PackedScore.pack(tricks1, points1, game1, tricks2, points2, game2);

            Score s = Score.ofPacked(packed).withAdditionalTrick(TeamId.values()[teamToWin], trickPoints);
            assertEquals(PackedScore.withAdditionalTrick(packed, TeamId.values()[teamToWin], trickPoints), s.packed());
        }
    }

    @Test
    public void nextTurnWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();

        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int tricks1 = rand.nextInt(Jass.TRICKS_PER_TURN + 1);
            //This must be the last trick
            int tricks2 = Jass.TRICKS_PER_TURN;
            int points1 = rand.nextInt(PackedScoreTest.TOTAL_POINTS_IF_MATCH + 1);
            int points2 = rand.nextInt(PackedScoreTest.TOTAL_POINTS_IF_MATCH + 1);
            int game1 = rand.nextInt(Jass.WINNING_POINTS*2 - points1 + 1);
            int game2 = rand.nextInt(Jass.WINNING_POINTS*2 - points2 + 1);
            long packed = PackedScore
                    .pack(tricks1, points1, game1, tricks2, points2, game2);
            Score s = Score.ofPacked(packed).nextTurn();
            assertEquals(PackedScore.nextTurn(packed), s.packed());
        }
    }

    @Test
    public void equalsWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();

        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = PackedScoreTest.randomValidScore(rand);
            int[] values2 = PackedScoreTest.randomValidScore(rand);
            if(!Arrays.equals(values, values2)){
                long packed1 = PackedScore.pack(values[0], values[1], values[2], values[3], values[4], values[5]);
                long packed2 = PackedScore.pack(values2[0], values2[1], values2[2], values2[3], values2[4], values2[5]);
                assertEquals(Score.ofPacked(packed1), Score.ofPacked(packed1));
                assertNotEquals(Score.ofPacked(packed1),
                        Score.ofPacked(packed2));
                assertNotEquals(null, Score.ofPacked(packed1));
            }
        }
    }

    @Test
    public void hashCodeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();

        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = PackedScoreTest.randomValidScore(rand);
            long packed = PackedScore
                    .pack(values[0], values[1], values[2], values[3], values[4],
                            values[5]);
            assertEquals(Long.hashCode(packed), Score.ofPacked(packed).hashCode());
        }
    }

    static Score randomValidScore(SplittableRandom rand){
        return Score.ofPacked(PackedScoreTest.randomValidPackedScore(rand));
    }
}
