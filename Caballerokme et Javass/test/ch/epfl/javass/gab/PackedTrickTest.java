package ch.epfl.javass.jass;

import ch.epfl.javass.bits.Bits32;
import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.SplittableRandom;

import static org.junit.jupiter.api.Assertions.*;

public class PackedTrickTest {

    static int[] randomValidTrick(SplittableRandom rand, boolean full){
        int size = full ? 4 : rand.nextInt(5);
        return new int[]{
                size < 1 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand),
                size < 2 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand),
                size < 3 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand),
                size < 4 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand),
                rand.nextInt(Jass.TRICKS_PER_TURN),
                rand.nextInt(PlayerId.COUNT),
                rand.nextInt(Card.Color.COUNT),
                size
        };
    }

    static int randomValidPackedTrick(SplittableRandom rand){
        int size = rand.nextInt(5);
        int card1 = size < 1 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand);
        int card2 = size < 2 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand);
        int card3 = size < 3 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand);
        int card4 = size < 4 ? PackedCard.INVALID : PackedCardTest.randomValidCard(rand);
        int index = rand.nextInt(Jass.TRICKS_PER_TURN);
        int player = rand.nextInt(PlayerId.COUNT);
        int trump = rand.nextInt(Card.Color.COUNT);
        return Bits32.pack(card1, 6, card2, 6, card3, 6, card4, 6, index, 4, player, 2, trump, 2);
    }

    static int randomInvalidTrick(SplittableRandom rand){
        int[] values = randomValidTrick(rand, false);

        switch (rand.nextInt(5)){
        case 0:
            values[0] = PackedCardTest.randomInvalidCard(rand);
            if(values[0]==PackedCard.INVALID)
                values[0] = PackedCard.INVALID - 1;
            assertFalse(PackedCard.isValid(values[0]));
            break;
        case 1:
            values[1] = PackedCardTest.randomInvalidCard(rand);
            if(values[1]==PackedCard.INVALID)
                values[1] = PackedCard.INVALID - 1;
            assertFalse(PackedCard.isValid(values[1]));
            break;
        case 2:
            values[2] = PackedCardTest.randomInvalidCard(rand);
            if(values[2]==PackedCard.INVALID)
                values[2] = PackedCard.INVALID - 1;
            assertFalse(PackedCard.isValid(values[2]));
            break;
        case 3:
            values[3] = PackedCardTest.randomInvalidCard(rand);
            if(values[3]==PackedCard.INVALID)
                values[3] = PackedCard.INVALID - 1;
            assertFalse(PackedCard.isValid(values[3]));
            break;
        case 4:
            values[4] = rand.nextInt(Jass.TRICKS_PER_TURN, 16);
            break;
        }

        return Bits32.pack(values[0], 6, values[1], 6, values[2], 6, values[3], 6, values[4], 4, values[5], 2, values[6], 2);
    }

    @Test
    public void isValidWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);
            assertTrue(PackedTrick.isValid(trick));
        }
    }

    @Test
    public void isValidWorksWithInvalid(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            assertFalse(PackedTrick.isValid(randomInvalidTrick(rand)));
        }
    }
    
    @Test
    public void firstEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Card.Color trump = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            PlayerId pl = PlayerId.ALL.get(rand.nextInt(PlayerId.COUNT));
            int trick = PackedTrick.firstEmpty(trump, pl);

            for(int j = 0; j < PlayerId.COUNT; ++j){
                assertEquals(PackedCard.INVALID, Bits32.extract(trick, j*6, 6));
            }

            assertEquals(0, Bits32.extract(trick, 24,4));
            assertEquals(pl.ordinal(), Bits32.extract(trick, 28,2));
            assertEquals(trump.ordinal(), Bits32.extract(trick, 30,2));
        }
    }

    @Test
    public void nextEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidTrick(rand, true);

            int trump = values[6];

            int bestPlayer = 0;
            for(int j = 1; j < 4; ++j){
                if(PackedCard.isBetter(Card.Color.ALL.get(trump), values[j], values[bestPlayer]))
                    bestPlayer = j;
            }
            bestPlayer = (bestPlayer + values[5])%4;

            int index = values[4];

            int nextTrick = PackedTrick.nextEmpty(Bits32.pack(values[0], 6, values[1], 6, values[2], 6, values[3], 6, values[4], 4, values[5], 2, values[6], 2));

            if(index + 1 == Jass.TRICKS_PER_TURN)
                assertEquals(Bits32.mask(0, 32), nextTrick);
            else {
                for (int j = 0; j < PlayerId.COUNT; ++j) {
                    assertEquals(PackedCard.INVALID,
                            Bits32.extract(nextTrick, j * 6, 6));
                }

                assertEquals(index + 1, Bits32.extract(nextTrick, 24, 4));
                assertEquals(bestPlayer, Bits32.extract(nextTrick, 28, 2));
                assertEquals(trump, Bits32.extract(nextTrick, 30, 2));
            }
        }
    }

    @Test
    public void isLastWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);
            assertEquals(Bits32.extract(trick, 24, 4) == 8, PackedTrick.isLast(trick));
        }
    }

    @Test
    public void isEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);
            if(rand.nextBoolean())
                trick |= Bits32.mask(0, 6*4);

            assertEquals((trick | Bits32.mask(0, 6*4)) == trick, PackedTrick.isEmpty(trick));
        }
    }

    @Test
    public void isFullWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);

            if(!PackedTrick.isEmpty(trick)) {
                boolean b = true;
                for (int j = 0; j < 4; ++j) {
                    if(!PackedCard.isValid(Bits32.extract(trick, j*6, 6)))
                        b = false;
                }

                assertEquals(b, PackedTrick.isFull(trick));
            }
        }
    }

    @Test
    public void sizeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidTrick(rand, true);
            int trick = Bits32.pack(values[0], 6, values[1], 6, values[2], 6, values[3], 6, values[4], 4, values[5], 2, values[6], 2);
            assertEquals(values[7], PackedTrick.size(trick));
        }
    }

    @Test
    public void trumpAndIndexAndPlayerAndCardWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int[] values = randomValidTrick(rand, true);
            int trick = Bits32.pack(values[0], 6, values[1], 6, values[2], 6, values[3], 6, values[4], 4, values[5], 2, values[6], 2);

            assertEquals(values[0], PackedTrick.card(trick, 0));
            assertEquals(values[1], PackedTrick.card(trick, 1));
            assertEquals(values[2], PackedTrick.card(trick, 2));
            assertEquals(values[3], PackedTrick.card(trick, 3));

            assertEquals(values[4], PackedTrick.index(trick));
            assertEquals(values[5], PackedTrick.player(trick, 0).ordinal());
            assertEquals((values[5] + 1) % 4, PackedTrick.player(trick, 1).ordinal());
            assertEquals((values[5] + 2) % 4, PackedTrick.player(trick, 2).ordinal());
            assertEquals((values[5] + 3) % 4, PackedTrick.player(trick, 3).ordinal());
            assertEquals(values[6], PackedTrick.trump(trick).ordinal());
        }
    }

    @Test
    public void withAddedCardWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);
            int size = PackedTrick.size(trick);
            if(!PackedTrick.isFull(trick)){
                int newCard = PackedCardTest.randomValidCard(rand);
                int trick2 = PackedTrick.withAddedCard(trick, newCard);

                assertEquals(size + 1, PackedTrick.size(trick2));

                for(int j = 0; j < 4; ++j){
                    if(j < size)
                        assertEquals(PackedTrick.card(trick, j), PackedTrick.card(trick2, j));
                    else if(j == size)
                        assertEquals(newCard, PackedTrick.card(trick2, j));
                }

                assertEquals(PackedTrick.index(trick), PackedTrick.index(trick2));
                assertEquals(PackedTrick.player(trick, 0).ordinal(), PackedTrick.player(trick2, 0).ordinal());
                assertEquals(PackedTrick.player(trick, 1).ordinal(), PackedTrick.player(trick2, 1).ordinal());
                assertEquals(PackedTrick.player(trick, 2).ordinal(), PackedTrick.player(trick2, 2).ordinal());
                assertEquals(PackedTrick.player(trick, 3).ordinal(), PackedTrick.player(trick2, 3).ordinal());
                assertEquals(PackedTrick.trump(trick).ordinal(), PackedTrick.trump(trick2).ordinal());
            }
        }
    }

    @Test
    public void baseColorWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);
            if(!PackedTrick.isEmpty(trick)){
                assertEquals(PackedCard.color(PackedTrick.card(trick, 0)), PackedTrick.baseColor(trick));
            }
        }
    }

    @Test
    public void playableCardWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);

            Card.Color trump = PackedTrick.trump(trick);
            long hand = PackedCardSetTest.randomValidCardSet(rand);
            long playablepacked;


            if(PackedTrick.isFull(trick)) {
                final long hand2 = hand;
                assertThrows(AssertionError.class,
                        () -> PackedTrick.playableCards(trick, hand2));
            }else{
                if(PackedTrick.isEmpty(trick)){
                    assertEquals(hand, PackedTrick.playableCards(trick, hand));
                }else {
                    Card winning = getWinningCard(trick, trump);
                    for (int j = 0; j < PackedTrick.size(trick); ++j) {
                        hand = PackedCardSet.remove(hand, PackedTrick.card(trick, j));
                    }

                    if (!PackedCardSet.isEmpty(hand)) {
                        Card.Color baseColor = PackedTrick.baseColor(trick);
                        CardSet playable = CardSet.EMPTY;
                        for (int j = 0; j < PackedCardSet.size(hand); ++j) {
                            Card c = Card.ofPacked(PackedCardSet.get(hand, j));
                            if (c.color() == baseColor) {
                                playable = playable.add(c);
                            }
                        }

                        if (playable.size() == 0) {
                            playable = CardSet.ofPacked(hand);
                        }

                        if (baseColor == trump) {
                            if (playable.size() == 1 && playable.get(0)
                                    .equals(Card.of(trump, Card.Rank.JACK))) {
                                playablepacked = hand;
                            } else {
                                playablepacked = playable.packed();
                            }
                        } else {
                            //Add trump
                            for (int j = 0; j < PackedCardSet.size(hand); ++j) {
                                Card c = Card.ofPacked(PackedCardSet.get(hand, j));
                                if (c.color() == trump) {
                                    playable = playable.add(c);
                                }
                            }

                            CardSet beforeRemovingOfTrumps = playable;
                            CardSet trumps = playable.subsetOfColor(trump);

                            for (int j = 0; j < trumps.size(); ++j) {
                                if (winning.isBetter(trump, trumps.get(j))) {
                                    playable = playable.remove(trumps.get(j));
                                }
                            }

                            if(playable.isEmpty())
                                playablepacked = beforeRemovingOfTrumps.packed();
                            else
                                playablepacked = playable.packed();
                        }
                        assertEquals(playablepacked, PackedTrick.playableCards(trick, hand));
                    }
                }
            }
        }
    }

    private Card getWinningCard(int trick, Card.Color trump) {
        Card c = Card.ofPacked(PackedTrick.card(trick, 0));
        for(int i = 0; i < PackedTrick.size(trick); ++i){
            Card other = Card.ofPacked(PackedTrick.card(trick, i));
            if(other.isBetter(trump, c))
                c = other;
        }
        return c;
    }

    @Test
    public void pointsWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int points = 0;
            int trick = randomValidPackedTrick(rand);
            Card.Color trump = PackedTrick.trump(trick);
            for (int j = 0; j < PackedTrick.size(trick); ++j) {
                points += PackedCard.points(trump, PackedTrick.card(trick, j));
            }

            if(PackedTrick.isLast(trick))
                points += Jass.LAST_TRICK_ADDITIONAL_POINTS;

            assertEquals(points, PackedTrick.points(trick));
        }
    }

    @Test
    public void winningPlayerWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int trick = randomValidPackedTrick(rand);
            if (!PackedTrick.isEmpty(trick)){
                Card.Color trump = PackedTrick.trump(trick);

                int index = 0;
                Card c = Card.ofPacked(PackedTrick.card(trick, 0));
                for (int j = 0; j < PackedTrick.size(trick); ++j) {
                    Card other = Card.ofPacked(PackedTrick.card(trick, j));
                    if (other.isBetter(trump, c)) {
                        c = other;
                        index = j;
                    }
                }

                assertEquals(PackedTrick.player(trick, index), PackedTrick.winningPlayer(trick));
            }
        }
    }
}
