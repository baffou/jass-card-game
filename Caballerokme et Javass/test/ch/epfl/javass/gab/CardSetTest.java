package ch.epfl.javass.gab;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.LinkedList;
import java.util.List;
import java.util.SplittableRandom;

import org.junit.jupiter.api.Test;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.PackedCard;
import ch.epfl.javass.jass.PackedCardSet;
import ch.epfl.test.TestRandomizer;

public class CardSetTest {

    @Test
    public void ofPackedAndPackedWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(packed, CardSet.ofPacked(packed).packed());
        }
    }

    @Test
    public void ofWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = 0;
            List<Card> cards = new LinkedList<>();
            int maxSize = rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1);
            for(int j = 0; j < maxSize; ++j) {
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                cards.add(Card.of(color, rank));
                packed = PackedCardSet.add(packed, PackedCard.pack(color, rank));
            }

            assertEquals(packed, CardSet.of(cards).packed());
        }
    }

    @Test
    public void isEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(PackedCardSet.isEmpty(packed), CardSet.ofPacked(packed).isEmpty());
        }
    }

    @Test
    public void sizeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(PackedCardSet.size(packed), CardSet.ofPacked(packed).size());
        }
    }

    @Test
    public void getWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            int index = rand.nextInt(PackedCardSet.size(packed));
            assertEquals(PackedCardSet.get(packed, index), CardSet.ofPacked(packed).get(index).packed());
        }
    }

    @Test
    public void addWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

            Card card = Card.of(color, rank);
            assertEquals(PackedCardSet.add(packed, card.packed()), CardSet.ofPacked(packed).add(card).packed());
        }
    }

    @Test
    public void removeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

            Card card = Card.of(color, rank);
            assertEquals(PackedCardSet.remove(packed, card.packed()), CardSet.ofPacked(packed).remove(card).packed());
        }
    }

    @Test
    public void containsWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

            Card card = Card.of(color, rank);
            assertEquals(PackedCardSet.contains(packed, card.packed()), CardSet.ofPacked(packed).contains(card));
        }
    }

    @Test
    public void complementWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(PackedCardSet.complement(packed), CardSet.ofPacked(packed).complement().packed());
        }
    }

    @Test
    public void unionWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed1 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            long packed2 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(PackedCardSet.union(packed1, packed2), CardSet.ofPacked(packed1).union(CardSet.ofPacked(packed2)).packed());
        }
    }

    @Test
    public void intersectionWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed1 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            long packed2 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(PackedCardSet.intersection(packed1, packed2), CardSet.ofPacked(packed1).intersection(CardSet.ofPacked(packed2)).packed());
        }
    }

    @Test
    public void differenceWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed1 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            long packed2 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(PackedCardSet.difference(packed1, packed2), CardSet.ofPacked(packed1).difference(CardSet.ofPacked(packed2)).packed());
        }
    }

    @Test
    public void subSetOfColorWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            Card.Color c = Card.Color.values()[rand.nextInt(Card.Color.COUNT)];
            assertEquals(PackedCardSet.subsetOfColor(packed, c), CardSet.ofPacked(packed).subsetOfColor(c).packed());
        }
    }

    @Test
    public void equalsWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed = 0;
            List<Card> cards = new LinkedList<>();
            int maxSize = rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1);
            for(int j = 0; j < maxSize; ++j) {
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                cards.add(Card.of(color, rank));
                packed = PackedCardSet.add(packed, PackedCard.pack(color, rank));
            }

            CardSet set1 = CardSet.ofPacked(packed);
            CardSet set2 = CardSet.of(cards);
            CardSet set3;
            do{
                set3 = CardSet.ofPacked(PackedCardSetTest.randomValidCardSet(rand));
            }while (set3.packed() == set2.packed());

            assertEquals(set1, set1);
            assertEquals(set1, set2);
            assertEquals(set2, set1);
            assertEquals(set2, set2);
            assertNotEquals(set1, set3);
            assertNotEquals(set2, set3);
            assertNotEquals(null, set1);
            assertNotEquals(null, set2);
        }
    }

    @Test
    public void hashcodeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed = rand.nextLong() & PackedCardSet.ALL_CARDS;
            assertEquals(Long.hashCode(packed), CardSet.ofPacked(packed).hashCode());
        }
    }
}
