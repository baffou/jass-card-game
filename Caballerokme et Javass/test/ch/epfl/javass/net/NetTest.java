package ch.epfl.javass.net;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SplittableRandom;

import org.junit.jupiter.api.Test;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.JassGame;
import ch.epfl.javass.jass.Player;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.Score;
import ch.epfl.javass.jass.TeamId;
import ch.epfl.javass.jass.Trick;
import ch.epfl.javass.jass.TurnState;
import ch.epfl.javass.jass.RandomGame.RandomPlayer;
import ch.epfl.test.TestRandomizer;

class NetTest {

    @Test
    void runGame() {
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Map<PlayerId, Player> players = new HashMap<>();
            Map<PlayerId, String> playerNames = new HashMap<>();

            List<String> remotePrinted = new LinkedList<>();
            List<String> currentPrinted = new LinkedList<>();

            long seed = rand.nextLong();
            long gameSeed = rand.nextLong();

            Thread t = new Thread(() -> {
                RemotePlayerServer remote = new RemotePlayerServer(
                        new PrintingListPlayer(remotePrinted, new RandomPlayer(seed)));
                remote.run();
            });

            t.start();

            try (RemotePlayerClient remote = new RemotePlayerClient("localhost", 5108)) {
                for (PlayerId pId : PlayerId.ALL) {
                    Player player = new RandomPlayer(seed);
                    if (pId == PlayerId.PLAYER_2)
                        player = new PrintingListPlayer(currentPrinted, remote);
                    players.put(pId, player);
                    playerNames.put(pId, pId.name());
                }


                JassGame g = new JassGame(gameSeed, players, playerNames);
                while (!g.isGameOver()) {
                    g.advanceToEndOfNextTrick();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

			try {
				Thread.sleep(10);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

            try {
                assertArrayEquals(currentPrinted.toArray(), remotePrinted.toArray());
            }catch (AssertionError e){
                System.out.println("Player seed : " + seed);
                System.out.println("Game seed : " + gameSeed);
                throw e;
            }
        }
    }

    class PrintingListPlayer implements Player {

        private final Player underlyingPlayer;
        private final List<String> printed;

        PrintingListPlayer(List<String> remotePrinted, Player player){
            this.printed = remotePrinted;
            this.underlyingPlayer = player;
        }

        @Override
        public Card cardToPlay(TurnState state, CardSet hand) {
            Card c = underlyingPlayer.cardToPlay(state, hand);
            this.printed.add("C'est à moi de jouer... Je joue : " + c.toString());
            return c;
        }

        @Override
        public void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
            this.printed.add("Les joueurs sont :");
            for (PlayerId pId : PlayerId.ALL) {
                this.printed.add(
                        playerNames.get(pId) + (pId == ownId ? " (moi)" : ""));
            }
            this.underlyingPlayer.setPlayers(ownId, playerNames);
        }

        @Override
        public void updateHand(CardSet newHand) {
            this.printed.add("Ma nouvelle main : " + newHand);
            this.underlyingPlayer.updateHand(newHand);
        }

        @Override
        public void setTrump(Card.Color trump) {
            this.printed.add("Atout : " + trump);
            this.underlyingPlayer.setTrump(trump);
        }

        @Override
        public void updateTrick(Trick newTrick) {
            this.printed.add(newTrick.toString());
            this.underlyingPlayer.updateTrick(newTrick);
        }

        @Override
        public void updateScore(Score score) {
            this.printed.add("Scores : " + score);
            this.underlyingPlayer.updateScore(score);
        }

        @Override
        public void setWinningTeam(TeamId winningTeam) {
            this.printed.add("L'équipe " + winningTeam + " a gagné !");
            this.underlyingPlayer.setWinningTeam(winningTeam);
        }
    }
}
