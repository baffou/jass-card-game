package ch.epfl.javass.jass;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TeamIdTest {

    @Test
    public void teamsAreInRightOrder(){
        assertArrayEquals(new TeamId[]{TeamId.TEAM_1, TeamId.TEAM_2}, TeamId.values());
    }

    @Test
    public void allWorks(){
        assertArrayEquals(TeamId.values(), TeamId.ALL.toArray());
    }

    @Test
    public void countWorks(){
        assertEquals(TeamId.values().length, TeamId.COUNT);
    }

    @Test
    public void teamIdOtherWorks(){
        assertEquals(TeamId.TEAM_2, TeamId.TEAM_1.other());
        assertEquals(TeamId.TEAM_1, TeamId.TEAM_2.other());
    }
}
