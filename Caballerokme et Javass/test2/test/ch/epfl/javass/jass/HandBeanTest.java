package ch.epfl.javass.gui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SplittableRandom;

import org.junit.jupiter.api.Test;

import ch.epfl.javass.gui.HandBean;
import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.CardSetTest;
import ch.epfl.javass.jass.Jass;
import ch.epfl.test.TestRandomizer;

public class HandBeanTest {

    @Test
    public void initWorks(){
        HandBean bean = new HandBean();
        assertEquals(Jass.HAND_SIZE, bean.hand().size());
        for(Card c : bean.hand())
            assertNull(c);
        assertEquals(0, bean.playableCards().size());
    }

    @Test
    public void handWorks(){
        HandBean bean = new HandBean();
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            List<Card> lastHand = bean.hand();
            CardSet newHand = CardSetTest.randomCardSet(rand,9);
            bean.setHand(newHand);
            List<Card> hand = bean.hand();
            List<Card> common = new LinkedList<>(hand);
            common.retainAll(lastHand);

            for(Card c : common)
                if(c != null)
                    assertEquals(lastHand.indexOf(c), hand.indexOf(c));

            for(int j = 0; j < newHand.size(); ++j)
                assertTrue(hand.contains(newHand.get(j)));

            for (Card card : hand)
                if(card != null)
                    assertTrue(newHand.contains(card));
        }
    }

    @Test
    public void playableCardsWorks(){
        HandBean bean = new HandBean();
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            CardSet playables = CardSetTest.randomCardSet(rand, 9 * 4);
            bean.setPlayableCards(playables);
            Set<Card> cards = bean.playableCards();

            for(int j = 0; j < playables.size(); ++j)
                assertTrue(cards.contains(playables.get(j)));

            for (Card card : cards)
                if(card != null)
                    assertTrue(playables.contains(card));
        }
    }
}
