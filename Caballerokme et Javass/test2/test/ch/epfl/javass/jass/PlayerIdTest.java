package ch.epfl.javass.jass;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerIdTest {

    @Test
    public void playersAreInRightOrder(){
        assertArrayEquals(new PlayerId[]{PlayerId.PLAYER_1, PlayerId.PLAYER_2, PlayerId.PLAYER_3, PlayerId.PLAYER_4}, PlayerId.values());
    }

    @Test
    public void allWorks(){
        assertArrayEquals(PlayerId.values(), PlayerId.ALL.toArray());
    }

    @Test
    public void countWorks(){
        assertEquals(PlayerId.values().length, PlayerId.COUNT);
    }

    @Test
    public void playerIdTeamWorks(){
        assertEquals(TeamId.TEAM_1, PlayerId.PLAYER_1.team());
        assertEquals(TeamId.TEAM_2, PlayerId.PLAYER_2.team());
        assertEquals(TeamId.TEAM_1, PlayerId.PLAYER_3.team());
        assertEquals(TeamId.TEAM_2, PlayerId.PLAYER_4.team());
    }
}
