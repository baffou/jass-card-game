package ch.epfl.javass.jass;

import ch.epfl.javass.bits.Bits64;
import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class PackedCardSetTest {

    @Test
    public void isValidWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            int maxCardValue = (int) Math.pow(2, Card.Rank.COUNT);
            int c1 = rand.nextInt(maxCardValue);
            int c2 = rand.nextInt(maxCardValue);
            int c3 = rand.nextInt(maxCardValue);
            int c4 = rand.nextInt(maxCardValue);
            long packed = Bits64.pack(c1, 16, c2, 16, c3, 16, c4, 16);
            assertTrue(PackedCardSet.isValid(packed));
        }
    }

    @Test
    public void isValidWorksWhenInvalid(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for(int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i){
            int maxCardValue = (int) Math.pow(2, Card.Rank.COUNT);
            int maxPartValue =  (int) Math.pow(2, 16);
            int c1 = rand.nextInt(maxCardValue, maxPartValue);
            int c2 = rand.nextInt(maxCardValue, maxPartValue);
            int c3 = rand.nextInt(maxCardValue, maxPartValue);
            int c4 = rand.nextInt(maxCardValue, maxPartValue);
            long packed = Bits64.pack(c1, 16, c2, 16, c3, 16, c4, 16);
            assertFalse(PackedCardSet.isValid(packed));
        }
    }

    private int getCardFromIndex(int index){
        int color = Math.floorDiv(index, 16);
        int rank = index - 16 * color;
        return PackedCard.pack(Card.Color.ALL.get(color), Card.Rank.ALL.get(rank));
    }

    @Test
    public void trumpAboveWorks(){
        for(Card.Color c : Card.Color.ALL){
            for(Card.Rank r : Card.Rank.ALL){
                int packed = PackedCard.pack(c, r);
                long aboveIndexes = PackedCardSet.trumpAbove(packed);
                String binary = Long.toBinaryString(aboveIndexes);

                for(int i = 0; i < Long.SIZE; ++i){
                    if(i % 16 >= Card.Rank.COUNT)
                        continue;

                    boolean above = false;
                    if(i < binary.length()){
                        above = binary.charAt(binary.length() - i - 1) == '1';
                    }

                    if(above){
                        assertTrue(PackedCard.isBetter(c, getCardFromIndex(i), packed));
                    }else{
                        assertFalse(PackedCard.isBetter(c, getCardFromIndex(i), packed));
                    }
                }
            }
        }
    }

    @Test
    public void singletonWorks() {
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

            int index = color.ordinal() * 16 + rank.ordinal();
            long singleton = PackedCardSet.singleton(PackedCard.pack(color, rank));
            String binary = Long.toBinaryString(singleton);

            for(int j = 0; j < Long.SIZE; ++j){
                char charAt = j < binary.length() ? binary.charAt(binary.length() - j - 1) : '0';
                assertTrue((index == j) == (charAt == '1'));
            }
        }
    }

    @Test
    public void emptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

            int index = color.ordinal() * 16 + rank.ordinal();
            assertFalse(PackedCardSet.isEmpty(PackedCardSet.singleton(index)));
        }

        assertTrue(PackedCardSet.isEmpty(0L));
    }

    @Test
    public void sizeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed = 0;
            int currentSize = 0;
            int maxSize = rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1);
            Set<Integer> added = new HashSet<>();
            for(int j = 0; j < maxSize; ++j){
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                int index = color.ordinal() * 16 + rank.ordinal();

                assertEquals(currentSize, PackedCardSet.size(packed));

                if(!added.contains(index)) {
                    packed |= (1L << index);
                    added.add(index);
                    currentSize++;
                }

                assertEquals(currentSize, PackedCardSet.size(packed));
            }
        }
    }

    @Test
    public void getWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed = 0;
            int maxSize = rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1);
            List<Integer> added = new ArrayList<>();
            for(int j = 0; j < maxSize; ++j){
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                int index = color.ordinal() * 16 + rank.ordinal();
                if(!added.contains(index))added.add(index);
                packed |= (1L << index);
            }

            Collections.sort(added);

            for(int j = 0; j < added.size(); ++j){
                assertEquals(getCardFromIndex(added.get(j)), PackedCardSet.get(packed, j));
            }
        }
    }

    @Test
    public void containsWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed1 = 0;
            long packed2 = 0;
            int maxSize = rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1);
            for(int j = 0; j < maxSize; ++j){
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                int index = color.ordinal() * 16 + rank.ordinal();
                packed1 |= (1L << index);
                packed2 = PackedCardSet.add(packed2, PackedCard.pack(color, rank));
                assertEquals(packed1, packed2);
            }
        }
    }

    @Test
    public void addAndRemoveWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed1 = 0;
            long packed2 = 0;
            int maxSize = rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1);
            for(int j = 0; j < maxSize; ++j){
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                int index = color.ordinal() * 16 + rank.ordinal();
                packed1 |= (1L << index);
                packed2 = PackedCardSet.add(packed2, PackedCard.pack(color, rank));
                assertEquals(packed1, packed2);
            }

            for(int j = 0; j < Long.SIZE; ++j){
                if(j % 16 < Card.Rank.COUNT){
                    int card = getCardFromIndex(j);
                    long packed3 = PackedCardSet.remove(packed2, card);
                    if(PackedCardSet.contains(packed2, card)){
                        assertEquals(packed2 & ~(1L << j), packed3);
                    }else{
                        assertEquals(packed2, packed3);
                    }
                }
            }
        }
    }

    @Test
    public void complementWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long packed1 = 0;
            long packed2 = 0;
            for(Card.Color c : Card.Color.ALL){
                for(Card.Rank r : Card.Rank.ALL){
                    if(rand.nextBoolean())
                        packed1 = PackedCardSet.add(packed1, PackedCard.pack(c, r));
                    else
                        packed2 = PackedCardSet.add(packed2, PackedCard.pack(c, r));
                }
            }

            assertEquals(packed1, PackedCardSet.complement(packed2));
            assertEquals(packed2, PackedCardSet.complement(packed1));
        }
    }

    @Test
    public void unionWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long l1 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            long l2 = rand.nextLong() & PackedCardSet.ALL_CARDS;

            assertEquals(l1 | l2, PackedCardSet.union(l1, l2));
        }
    }

    @Test
    public void intersectionWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long l1 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            long l2 = rand.nextLong() & PackedCardSet.ALL_CARDS;

            assertEquals(l1 & l2, PackedCardSet.intersection(l1, l2));
        }
    }

    @Test
    public void differenceWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            long l1 = rand.nextLong() & PackedCardSet.ALL_CARDS;
            long l2 = rand.nextLong() & PackedCardSet.ALL_CARDS;

            assertEquals(l1 & (~l2 & PackedCardSet.ALL_CARDS), PackedCardSet.difference(l1, l2));
        }
    }

    @Test
    public void subsetOfColorWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Set<Integer> cards = new HashSet<>();
            long packedSet = 0;
            for(int j = 0; j < rand.nextInt(Card.Rank.COUNT * Card.Color.COUNT + 1); ++j){
                Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
                Card.Rank rank = Card.Rank.ALL.get(rand.nextInt(Card.Rank.COUNT));

                int packedCard = PackedCard.pack(color, rank);
                packedSet = PackedCardSet.add(packedSet, packedCard);
                cards.add(packedCard);
            }

            for(Card.Color c : Card.Color.ALL){
                long subsetColor = PackedCardSet.subsetOfColor(packedSet, c);
                for(int j = 0; j < Long.SIZE; ++j){
                    if(Math.floorDiv(j, 16) == c.ordinal() && j % 16 < Card.Rank.COUNT){
                        assertEquals(cards.contains(getCardFromIndex(j)), (subsetColor & PackedCardSet.singleton(j)) >> j == 1);
                    }else{
                        assertEquals(0, (subsetColor & (1L << j)));
                    }
                }
            }
        }
    }

    static long randomValidCardSet(SplittableRandom rand){
        return rand.nextLong() & PackedCardSet.ALL_CARDS;
    }
}
