package ch.epfl.javass.jass;

import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;

import java.util.SplittableRandom;

import static org.junit.jupiter.api.Assertions.*;

public class TrickTest {

    private Trick randomValidTrick(SplittableRandom rand) {
        return Trick.ofPacked(PackedTrickTest.randomValidPackedTrick(rand));
    }

    @Test
    public void firstEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            PlayerId player = PlayerId.ALL.get(rand.nextInt(PlayerId.COUNT));
            assertEquals(PackedTrick.firstEmpty(color, player), Trick.firstEmpty(color, player).packed());
        }
    }

    @Test
    public void ofPackedWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int validTrick = PackedTrickTest.randomValidPackedTrick(rand);
            assertEquals(validTrick, Trick.ofPacked(validTrick).packed());
            int invalidTrick = PackedTrickTest.randomInvalidTrick(rand);
            assertThrows(IllegalArgumentException.class, () -> Trick.ofPacked(invalidTrick));
        }
    }

    @Test
    public void packedWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            int validTrick = PackedTrickTest.randomValidPackedTrick(rand);
            assertEquals(validTrick, Trick.ofPacked(validTrick).packed());
        }
    }

    @Test
    public void nextEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            if(trick.isFull())
                assertEquals(PackedTrick.nextEmpty(trick.packed()), trick.nextEmpty().packed());
            else
                assertThrows(IllegalStateException.class, () -> trick.nextEmpty());
        }
    }

    @Test
    public void isEmptyWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.isEmpty(trick.packed()), trick.isEmpty());
        }
    }

    @Test
    public void isFullWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.isFull(trick.packed()), trick.isFull());
        }
    }

    @Test
    public void isLastWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.isLast(trick.packed()), trick.isLast());
        }
    }

    @Test
    public void sizeWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.size(trick.packed()), trick.size());
        }
    }

    @Test
    public void trumpWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.trump(trick.packed()), trick.trump());
        }
    }

    @Test
    public void indexWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.index(trick.packed()), trick.index());
        }
    }

    @Test
    public void playerWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(PackedTrick.player(trick.packed(), 0), trick.player(0));
            assertEquals(PackedTrick.player(trick.packed(), 1), trick.player(1));
            assertEquals(PackedTrick.player(trick.packed(), 2), trick.player(2));
            assertEquals(PackedTrick.player(trick.packed(), 3), trick.player(3));
            assertThrows(IndexOutOfBoundsException.class, () -> trick.player(4));
        }
    }

    @Test
    public void cardWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            int size = trick.size();
            if(size >= 1)
                assertEquals(PackedTrick.card(trick.packed(), 0), trick.card(0).packed());
            else
                assertThrows(IndexOutOfBoundsException.class, () -> trick.card(0));
            if(size >= 2)
                assertEquals(PackedTrick.card(trick.packed(), 1), trick.card(1).packed());
            else
                assertThrows(IndexOutOfBoundsException.class, () -> trick.card(1));
            if(size >= 3)
                assertEquals(PackedTrick.card(trick.packed(), 2), trick.card(2).packed());
            else
                assertThrows(IndexOutOfBoundsException.class, () -> trick.card(2));
            if(size >= 4)
                assertEquals(PackedTrick.card(trick.packed(), 3), trick.card(3).packed());
            else
                assertThrows(IndexOutOfBoundsException.class, () -> trick.card(3));
        }
    }

    @Test
    public void withAddedCardWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            Card c = CardTest.randomValidCard(rand);
            if(!trick.isFull())
                assertEquals(PackedTrick.withAddedCard(trick.packed(), c.packed()), trick.withAddedCard(c).packed());
            else
                assertThrows(IllegalStateException.class, () -> trick.withAddedCard(c));
        }
    }

    @Test
    public void baseColorWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            if(!trick.isEmpty())
                assertEquals(PackedTrick.baseColor(trick.packed()), trick.baseColor());
            else
                assertThrows(IllegalStateException.class, () -> trick.baseColor());
        }
    }

    @Test
    public void winningPlayerWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            if(!trick.isEmpty())
                assertEquals(PackedTrick.winningPlayer(trick.packed()), trick.winningPlayer());
            else
                assertThrows(IllegalStateException.class, () -> trick.winningPlayer());
        }
    }

    @Test
    public void playableCardsWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            CardSet set = CardSet.ofPacked(PackedCardSetTest.randomValidCardSet(rand));
            if(!trick.isFull())
                assertEquals(PackedTrick.playableCards(trick.packed(), set.packed()), trick.playableCards(set).packed());
            else
                assertThrows(IllegalStateException.class, () -> trick.playableCards(set));
        }
    }


    @Test
    public void equalsPlayerWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick1 = randomValidTrick(rand);

            Trick trick2;
            do{
               trick2 = randomValidTrick(rand);
            }while (trick2.packed() == trick1.packed());

            assertEquals(trick1, trick1);
            assertEquals(trick2, trick2);
            assertNotEquals(trick1, trick2);
            assertNotEquals(null, trick1);
            assertNotEquals(null, trick2);

            Card.Color color = Card.Color.ALL.get(rand.nextInt(Card.Color.COUNT));
            PlayerId player = PlayerId.ALL.get(rand.nextInt(PlayerId.COUNT));
            assertEquals(Trick.firstEmpty(color, player), Trick.firstEmpty(color, player));
        }
    }

    @Test
    public void hashCodePlayerWorks(){
        SplittableRandom rand = TestRandomizer.newRandom();
        for (int i = 0; i < TestRandomizer.RANDOM_ITERATIONS; ++i) {
            Trick trick = randomValidTrick(rand);
            assertEquals(trick.packed(), trick.hashCode());
        }
    }
}
