package ch.epfl.javass;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ch.epfl.javass.gui.GraphicalPlayerAdapter;
import ch.epfl.javass.jass.JassGame;
import ch.epfl.javass.jass.MctsPlayer;
import ch.epfl.javass.jass.PacedPlayer;
import ch.epfl.javass.jass.Player;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.net.RemotePlayerClient;
import ch.epfl.javass.net.StringSerializer;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Classe servant à lancer une partie depuis cet ordinateur
 * 
 * @author Jérémy
 *
 */
public class LocalMain extends Application {

    private final static int ANORMAL_END_CTE = 1;
    private final static String DEFAULT_IP = "localhost";
    private final static String DEFAULT_ITERATION_NUMBER = "10000";
    private final static int MIN_ITERATIONS = 10;
    private final static char INSTRUCTION_FOR_SIMULATED_PLAYER = 's';
    private final static char INSTRUCTION_FOR_DISTANT_PLAYER = 'r';
    private final static char INSTRUCTION_FOR_HUMAN_PLAYER = 'h';
    private final static int MIN_WAITING_TIME = 2;
    private final static int MIN_ARGUMENT_SIZE = 4;
    private final static int MAX_ARGUMENT_SIZE = 5;
    private final static int THREAD_SLEEPING_TIME = 1000;

    /**
     * Cette énumération sert à atribuer à chaque joueur un nom. (Nous aurions
     * pu le faire grâce à une map mais nous trouvions l'enum plus élégante)
     */
    private enum StandardNames {
        Player1("Steve"), Player2("Javier"), Player3("PainPerdu"), Player4(
                "Samosa");

        private String name;

        private StandardNames(String name) {
            this.name = name;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Méthode qui s'occupe de créer chaque joueur (tout en vérifiant la
     * validité de leurs arguments) et qui crée la partie de jass et la lance
     * 
     * @param arg0
     *            : Stage de base
     * @throws Exception
     */
    @Override
    public void start(Stage arg0) throws Exception {

        List<String> parameters = getParameters().getRaw();
        Map<PlayerId, Player> players = new EnumMap<>(PlayerId.class);
        Map<PlayerId, String> names = new EnumMap<>(PlayerId.class);

        // on vérifie qu on a le bon nombre de paramètres
        if (parameters.size() != MIN_ARGUMENT_SIZE
                && parameters.size() != MAX_ARGUMENT_SIZE) {
            printErrorInNumberOfParametersErrorMessage(parameters.size());
            System.exit(ANORMAL_END_CTE);
        }

        // si on a un 5ème argument on essaye de créer une seed à partir de là
        Random r = new Random();
        if (parameters.size() == MAX_ARGUMENT_SIZE) {
            try {
                r = new Random(
                        Long.parseLong(parameters.get(MAX_ARGUMENT_SIZE - 1)));
            } catch (NumberFormatException e) {
                printInvalidSeedErrorMessage();
                System.exit(ANORMAL_END_CTE);
            }
        }

        long rng_long = r.nextLong();

        // On crée le thread sur lequel se déroulera la partie
        Thread gameThread = new Thread(() -> {
            JassGame g = new JassGame(rng_long, players, names);
            while (!g.isGameOver()) {
                g.advanceToEndOfNextTrick();
                try {
                    Thread.sleep(THREAD_SLEEPING_TIME);
                } catch (Exception e) {
                }
            }
        });

        // on vérifie pour chaqu un des 4 premiers paramètres si il est valide
        // et si oui on crée le joueur demandé
        for (int i = 0; i < PlayerId.COUNT; i++) {
            String[] actualStringTab = StringSerializer.split(":",
                    parameters.get(i));
            checkAndApplyParameters2(actualStringTab, players, i, names, r);
        }

        gameThread.setDaemon(true);
        gameThread.start();
    }

    /**
     * Méthode s'occupant principalement de créer le joueur demandé (par le
     * charactère c) (elle vérifie quelques conditions par la même occasion)
     * 
     * @param c
     *            : Charactère qui va nous indiquer quel type de joueur il va
     *            falloir créer
     * @param name
     *            : Nom du joueur
     * @param ipAdress
     *            : Adresse Ip du joueur distant (pas forcément utilisé si ce
     *            n'est pas un joueur distant)
     * @param iterations
     *            : Nombres d'itérations réalisées par le joueur simulé (par
     *            MCTS ducoup) (pas forcément utilisé si ce n'est pas un joueur
     *            simulé)
     * @param r
     *            : Seed pour l'aléatoire
     * @param mapPlayers
     *            : Table associative qui associe à chaque PlayerId un
     *            joueur(Player)
     * @param names
     *            : Table associative qui associe à chaque PlayerId un nom
     * @param id
     *            : Id du joueur actuel
     */
    private void ApplyParameters(char c, String name, String ipAdress,
            String iterations, Random r, Map<PlayerId, Player> mapPlayers,
            Map<PlayerId, String> names, PlayerId id) {
        names.put(id, name);
        switch (c) {
        case INSTRUCTION_FOR_DISTANT_PLAYER:
            try {
                mapPlayers.put(id, new RemotePlayerClient(ipAdress));
            } catch (IOException e) {
                printErrorInInstanciationOfDistantPlayer();
                System.exit(ANORMAL_END_CTE);
            }
            ;
            break;
        case INSTRUCTION_FOR_SIMULATED_PLAYER:
            checkThirdArgument(iterations);
            mapPlayers.put(id,
                    new PacedPlayer(
                            new MctsPlayer(id, r.nextLong(),
                                    Integer.parseInt(iterations)),
                            MIN_WAITING_TIME));
            break;
        case INSTRUCTION_FOR_HUMAN_PLAYER:
            mapPlayers.put(id, new GraphicalPlayerAdapter());
            break;
        default:
            printInvalidFirstArgumentErrorMessage();
            System.exit(ANORMAL_END_CTE);
            break;
        }
    }

    /**
     * Méthode s'occupant de vérifier un certain nombre de conditions et ensuite
     * appelle ApplyParameters pour créer le joueur demandé
     * 
     * @param stringTab
     *            : Ensembles des paramètres attribués à la création du joueur
     * @param mapPlayers
     *            : Table associative qui associe à chaque PlayerId un
     *            joueur(Player)
     * @param i
     *            : Index du joueur dans l'énumération PlayerId
     * @param names
     *            : Table associative qui associe à chaque PlayerId un nom
     * @param r
     *            : Seed pour l'aléatoire
     */
    private void checkAndApplyParameters2(String[] stringTab,
            Map<PlayerId, Player> mapPlayers, int i,
            Map<PlayerId, String> names, Random r) {
        PlayerId player = PlayerId.values()[i];
        if (stringTab[0].length() != 1) {
            printInvalidFirstArgumentErrorMessage();
            System.exit(ANORMAL_END_CTE);
        }
        if (stringTab.length == 1) {
            ApplyParameters(stringTab[0].charAt(0),
                    StandardNames.values()[player.ordinal()].name, DEFAULT_IP,
                    DEFAULT_ITERATION_NUMBER, r, mapPlayers, names, player);
        }
        if (stringTab.length == 2) {
            ApplyParameters(stringTab[0].charAt(0),
                    !stringTab[1].isEmpty() ? stringTab[1]
                            : StandardNames.values()[player.ordinal()].name,
                    DEFAULT_IP, DEFAULT_ITERATION_NUMBER, r, mapPlayers, names,
                    player);
        }
        if (stringTab.length == 3) {
            if (stringTab[0].charAt(0) == INSTRUCTION_FOR_HUMAN_PLAYER) {
                printErrorTooMuchArgumentForHumanErrorMessage();
                System.exit(ANORMAL_END_CTE);
            }
            ApplyParameters(stringTab[0].charAt(0),
                    !stringTab[1].isEmpty() ? stringTab[1]
                            : StandardNames.values()[player.ordinal()].name,
                    stringTab[0].charAt(0) == INSTRUCTION_FOR_DISTANT_PLAYER
                            ? stringTab[2]
                            : DEFAULT_IP,
                    stringTab[0].charAt(0) == INSTRUCTION_FOR_SIMULATED_PLAYER
                            ? stringTab[2]
                            : DEFAULT_ITERATION_NUMBER,
                    r, mapPlayers, names, player);
        }
        if (stringTab.length > 3) {
            printTooMuchArgumentErrorMessage();
            System.exit(ANORMAL_END_CTE);
        }
    }

    /**
     * Méthode qui s'occupe de vérifier que le nombre d'itération est un entier
     * valide
     * 
     * @param value
     *            : Nombre d'itérations passées en paramètre à la méthode main
     */
    private void checkThirdArgument(String value) {
        try {
            if (Integer.parseInt(value) < MIN_ITERATIONS) {
                printNotEnoughIterationErrorMessage();
                System.exit(ANORMAL_END_CTE);
            }
        } catch (NumberFormatException e) {
            printErrorInIterationValueErrorMessage();
            System.exit(ANORMAL_END_CTE);
        }
    }

    /**
     * Message d'erreur qu'utilise tout les autres
     */
    private void printBaseErrorMessage() {
        System.err.println("Attention erreur !");
        System.err.println(
                "-Localisation : paramètres passés à la méthode main de la classe LocalMain située dans le paquetage src.ch.epfl.javass");
    }

    /**
     * Message d'erreur indiquant si il y a eu trop ou pas assez d'arguments
     * pour l'instanciation de joueur
     * 
     * @param nbOfParameters
     */
    private void printErrorInNumberOfParametersErrorMessage(
            int nbOfParameters) {
        printBaseErrorMessage();
        System.err.println("-Raison : Nombres de paramètres "
                + (nbOfParameters < 4 ? "trop petit" : "trop grand"));
        System.err.println(
                "-Solution : Les paramètres doivent être formulés de cette manière : ");
        System.err.println(
                "    -Il faut qu'il y ait 4 paramètres (un pour chaque joueur) qui doivent être d'un de ces trois types :");
        System.err.println("		- s pour un joueurs simulé ");
        System.err.println("		- h pour un joueurs humain ");
        System.err.println("		- r pour un joueurs distant ");
        System.err.println(
                "    -A chaque joueur peut être assigné (de manière optionnelle) un nom de cette manière --> s/h/r:<le_nom>, sinon le joueur aura un nom par default.");
        System.err.println(
                "     De plus pour un joueur simulé on peut peut attribuer à l'algorithme le nombre d'itération (un entier >= 10) souhaité de la manière suivante --> s:(<le_nom>):<nb_iteration> ;");
        System.err.println(
                "     et pour le joueur distant on peut préciser son ip (numéro ou nom) de cette manière --> r:(<le_nom>):<ip>, sinon le joueur aura l'ip par default : localhost.");
        System.err.println(
                "    -Il peut y avoir un 5ème paramètre (un entier) optionnel qui est la graine qui servira à générer toutes les autres graines qui s'occupent de la partie aléatoire du jeux (distribution des cartes, ...).");
    }

    /**
     * Message d'erreur pour indiquerr qu il y a eu une erreur dans le nombre d
     * itérations (pas un entier)
     */
    private void printErrorInIterationValueErrorMessage() {
        printBaseErrorMessage();
        System.err.println(
                "Raison : La valeur de la spécification du nombre d'itérations ne contient pas que un entier");
        System.err.println(
                "-Solution : Remplacer la valeur du nombre d'itérations par une valeur valide, c'est à dire un entier (de type int)");
    }

    /**
     * Mesage d'erreur pour indiquer que le nombre d'itérations est trop faible
     * (<10)
     */
    private void printNotEnoughIterationErrorMessage() {
        printBaseErrorMessage();
        System.err.println(
                "-Raison : Le nombre d'itérations passées en paramètres est inférieur à 10.");
        System.err.println(
                "-Solution : Remplacer la valeur mise dans h:...:<value> par quelque chose supérieur ou égale à 10.");
    }

    /**
     * Message d'erreur si problème lors de la création du joueur distant
     * (problème de oonnections, ip invalide, ...)
     */
    private void printErrorInInstanciationOfDistantPlayer() {
        printBaseErrorMessage();
        System.err.println(
                "-Raison : Une erreur à été lancée à la création du joueur distant");
        System.err.println(
                "-Solution : C'est probablement lié à l'ip, vérifier que celui-ci est le bon");
    }

    /**
     * Message d'erreur pour indiquer qu un joueur humain ne peut prendre que
     * deux arguments et pas trois comme les deux autres
     */
    private void printErrorTooMuchArgumentForHumanErrorMessage() {
        printBaseErrorMessage();
        System.err.println(
                "-Raison : Le nombre de spécification liées à l'argument de création d'un joueur humain est trop important");
        System.err.println(
                "-Solution : Le paramètre h peut prendre une spécification: un nom, sous cette forme --> s:<nom>. ");
    }

    /**
     * Message d'erreur pour dire qu il y a trop d arguments pour le joueur
     * donné
     */
    private void printTooMuchArgumentErrorMessage() {
        printBaseErrorMessage();
        System.err.println(
                "-Raison : Le nombre de spécification liées à l'argument est trop important");
        System.err.println(
                "-Solution : Pour chaque type d'argument se réferer à sa description :");
        System.err.println(
                "		- s peut prendre deux spécifications: un nom et le nombre d'itérations (un entier de type int), sous cette forme --> s:<nom>:<nb_iterations>. ");
        System.err.println(
                "		- h peut prendre une spécification: un nom, sous cette forme --> s:<nom>. ");
        System.err.println(
                "		- r peut prendre deux spécifications: un nom et le port (un entier de type int) pour la connection, sous cette forme --> s:<nom>:<port>. ");
    }

    /**
     * Message d'erreur pour mauvaise commande de création de joueur
     */
    private void printInvalidFirstArgumentErrorMessage() {
        printBaseErrorMessage();
        System.err.println(
                "-Raison : L'instruction pour la création d'un joueur n'est pas reconnu (n'est pas r, h ou s).");
        System.err.println(
                "-Solution : Remplacer l'argument erroné par une des valeurs selon votre souhait (doit être composé d'un seul charactère):");
        System.err.println(
                "                 -h si vous souhaitez qu'un joueur humain joue sur votre machine");
        System.err.println(
                "                 -s si vous souhaitez qu'un joueur simuler joue avec vous");
        System.err.println(
                "                 -r si vous souhaitez qu'un joueur distant se connecte à votre partie");
    }

    /**
     * Message d'erreur pour mauvaise graine pour l'aléatoire
     */
    private void printInvalidSeedErrorMessage() {
        printBaseErrorMessage();
        System.err.println(
                "-Raison : La valeur de la graine passée en paramètre est invalide car elle n'est pas composée uniquement d'un entier");
        System.err.println(
                "-Solution : Remplacer la valeur de la graine par simplement un entier (de type long)");
    }
}

// package ch.epfl.javass;
//
// import java.io.IOException;
// import java.net.UnknownHostException;
// import java.util.EnumMap;
// import java.util.List;
// import java.util.Map;
// import java.util.Random;
//
// import ch.epfl.javass.gui.GraphicalPlayerAdapter;
// import ch.epfl.javass.jass.JassGame;
// import ch.epfl.javass.jass.MctsPlayer;
// import ch.epfl.javass.jass.PacedPlayer;
// import ch.epfl.javass.jass.Player;
// import ch.epfl.javass.jass.PlayerId;
// import ch.epfl.javass.net.RemotePlayerClient;
// import ch.epfl.javass.net.StringSerializer;
// import javafx.application.Application;
// import javafx.stage.Stage;
//
// public class LocalMain extends Application {
//
// private final static int ANORMAL_END_CTE = 1;
//
// private enum StandardNames{
// Player1("Steve"), Player2("Javier"), Player3("PainPerdu"), Player4("Samosa");
//
// private String name;
//
// private StandardNames(String name) {
// this.name= name;
// }
// }
//
// public static void main(String[] args) {
// launch(args);
// }
//
// @Override
// public void start(Stage arg0) throws Exception {
//
// List<String> parameters = getParameters().getRaw();
// Map<PlayerId, Player> players = new EnumMap<>(PlayerId.class);
// Map<PlayerId, String> names = new EnumMap<>(PlayerId.class);
//
// if(parameters.size() != 4 && parameters.size() != 5) {
// printErrorInNumberOfParametersErrorMessage(parameters.size());
// System.exit(ANORMAL_END_CTE);
// }
//
// Random r = new Random();
// if(parameters.size() == 5) {
// try {
// r = new Random(Long.parseLong(parameters.get(4)));
// }catch (NumberFormatException e) {
// printInvalidSeedErrorMessage();
// System.exit(ANORMAL_END_CTE);
// }
// }
//
// Thread gameThread = new Thread(() -> {
// JassGame g = new JassGame(0, players, names);
// while (!g.isGameOver()) {
// g.advanceToEndOfNextTrick();
// try {
// Thread.sleep(1000);
// } catch (Exception e) {
// }
// }
// });
//
// int i = 0;
// for(String s : parameters) {
// String [] actualStringTab = StringSerializer.split(":", s);
// checkAndApplyParameters(actualStringTab, players, i, names, r);
// i++;
// }
//
// gameThread.setDaemon(true);
// gameThread.start();
// }
//
//
//
// private void checkAndApplyParameters(String [] stringTab, Map<PlayerId,
// Player> mapPlayers, int i, Map<PlayerId, String> names, Random r) {
// PlayerId player = PlayerId.values()[i];
// if(stringTab[0].length() != 1) {
// printInvalidFirstArgumentErrorMessage();
// System.exit(ANORMAL_END_CTE);
// }
// if(stringTab.length == 1) {
// names.put(player, StandardNames.values()[player.ordinal()].name);
// switch (stringTab[0].charAt(0)) {
// case 'r':
// try {
// mapPlayers.put(player, new RemotePlayerClient("localhost"));
// } catch (IOException e) {
// printErrorInInstanciationOfDistantPlayer();
// System.exit(ANORMAL_END_CTE);
// };
// break;
// case 's':
// mapPlayers.put(player, new PacedPlayer(new MctsPlayer(player, r.nextLong(),
// 10_000), 1));
// break;
// case 'h':
// mapPlayers.put(player, new GraphicalPlayerAdapter());
// break;
// default:
// printInvalidFirstArgumentErrorMessage();
// System.exit(ANORMAL_END_CTE);
// break;
// }
////
//// if (stringTab[0].charAt(0) == 's') {
//// mapPlayers.put(player, new PacedPlayer(new MctsPlayer(player, 0, 10_000),
// 2000));
//// }
//// if (stringTab[0] == "h") {
//// mapPlayers.put(player, new GraphicalPlayerAdapter());
//// }
//// if (stringTab[0] == "r") {
//// try {
//// mapPlayers.put(player, new RemotePlayerClient("Jamie", 0));
//// } catch (IOException e) {
//// printErrorInInstanciationOfDistantPlayer();
//// System.exit(ANORMAL_END_CTE);
//// }
//// } else {
//// printInvalidFirstArgumentErrorMessage();
//// System.exit(ANORMAL_END_CTE);
//// }
// }
// //---------------------------------------------------------------------------
// if(stringTab.length == 2) {
// if(stringTab[1] != "")
// names.put(player, stringTab[1]);
// else
// names.put(player, StandardNames.values()[player.ordinal()].name);
// switch (stringTab[0].charAt(0)) {
// case 'r':
// try {
// mapPlayers.put(player, new RemotePlayerClient("localhost"));
// } catch (IOException e) {
// printErrorInInstanciationOfDistantPlayer();
// System.exit(ANORMAL_END_CTE);
// };
// break;
// case 's':
// mapPlayers.put(player, new PacedPlayer(new MctsPlayer(player, r.nextLong(),
// 10_000), 1));
// break;
// case 'h':
// mapPlayers.put(player, new GraphicalPlayerAdapter());
// break;
// default:
// printInvalidFirstArgumentErrorMessage();
// System.exit(ANORMAL_END_CTE);
// break;
// }
////
//// if(stringTab[0] == "s") {
//// mapPlayers.put(player, new PacedPlayer(new MctsPlayer(player, 0, 10_000),
// 2));
//// }
//// if(stringTab[0] == "h") {
//// mapPlayers.put(player, new GraphicalPlayerAdapter());
//// }
//// if(stringTab[0] == "r") {
//// try {
//// mapPlayers.put(player, new RemotePlayerClient(stringTab[1] != "" ?
// stringTab[1] : "" , 0 ));
//// } catch (IOException e) {
//// printErrorInInstanciationOfDistantPlayer();
//// System.exit(ANORMAL_END_CTE);
//// }
//// }
//// else {
//// printInvalidFirstArgumentErrorMessage();
//// System.exit(ANORMAL_END_CTE);
//// }
// }
//
// //---------------------------------------------------------------------------------
// if(stringTab.length == 3) {
//// if(stringTab[1] != "")
//// names.put(player, stringTab[1]);
//// else
// names.put(player, StandardNames.values()[player.ordinal()].name);
// switch (stringTab[0].charAt(0)) {
// case 'r':
// String s = stringTab[2]/*.replace(".", "")*/;
// // checkThirdArgument("r", s);
// try {
// mapPlayers.put(player, new RemotePlayerClient(s));
// }
// catch (IOException e) {
// printErrorInInstanciationOfDistantPlayer();
// System.exit(ANORMAL_END_CTE);
// };
// break;
// case 's':
// checkThirdArgument("s", stringTab[2]);
// mapPlayers.put(player, new PacedPlayer(new MctsPlayer(player, r.nextLong(),
// Integer.parseInt(stringTab[2])), 1));
// break;
// case 'h':
// printErrorTooMuchArgumentForHumanErrorMessage();
// System.exit(ANORMAL_END_CTE);
// break;
// default:
// printInvalidFirstArgumentErrorMessage();
// System.exit(ANORMAL_END_CTE);
// break;
// }
//
// ///--------------------
//
//// if(stringTab[0] == "s") {
//// checkThirdArgument("s", stringTab[2]);
//// mapPlayers.put(player, new PacedPlayer(new MctsPlayer(player, 0,
// Integer.parseInt(stringTab[2])), 2000));
//// }
//// if(stringTab[0] == "h") {
//// printErrorTooMuchArgumentForHumanErrorMessage();
//// System.exit(ANORMAL_END_CTE);
//// }
//// if(stringTab[0] == "r") {
//// checkThirdArgument("r", stringTab[2]);
//// String s = stringTab[2].replace(".", "");
//// try {
//// mapPlayers.put(player, new RemotePlayerClient(stringTab[1] != "" ?
// stringTab[1] : "" , Integer.parseInt(s) ));
//// } catch (IOException e) {
//// printErrorInInstanciationOfDistantPlayer();
//// System.exit(ANORMAL_END_CTE);
//// }
//// }
//// else {
//// printInvalidFirstArgumentErrorMessage();
//// System.exit(ANORMAL_END_CTE);
//// }
// }
//
// //---------------------------------------------------------------------------------------------
//
// if(stringTab.length > 3) {
// printTooMuchArgumentErrorMessage();
// System.exit(ANORMAL_END_CTE);
// }
// }
//
//
// private void checkThirdArgument(String type, String value) {
// if (type == "s") {
// try {
// if (Integer.parseInt(value) < 10) {
// printNotEnoughIterationErrorMessage();
// System.exit(ANORMAL_END_CTE);
// }
// } catch (NumberFormatException e) {
// printErrorInIterationValueErrorMessage();
// System.exit(ANORMAL_END_CTE);
// }
// }
// if (type == "r") {
// System.out.println(value);
// try {
// Integer.parseInt(value);
// } catch (NumberFormatException e) {
// printErrorInPortValueErrorMessage();
// System.exit(ANORMAL_END_CTE);
// }
// }
// }
//
//
// private void printBaseErrorMessage() {
// System.err.println("Attention erreur !");
// System.err.println("-Localisation : paramètres passés à la méthode main de la
// classe LocalMain située dans le paquetage src.ch.epfl.javass");
// }
//
//
// private void printErrorInNumberOfParametersErrorMessage(int nbOfParameters) {
// printBaseErrorMessage();
// System.err.println("-Raison : Nombres de paramètres " + (nbOfParameters<4 ?
// "trop petit" : "trop grand") );
// System.err.println("-Solution : Les paramètres doivent être formulés de cette
// manière : ");
// System.err.println(" -Il faut qu'il y ait 4 paramètres (un pour chaque
// joueur) qui doivent être d'un de ces trois types :");
// System.err.println(" - s pour un joueurs simulé ");
// System.err.println(" - h pour un joueurs humain ");
// System.err.println(" - r pour un joueurs distant ");
// System.err.println(" -A chaque joueur peut être assigné (de manière
// optionnelle) un nom de cette manière --> s/h/r:<le_nom>, sinon le joueur aura
// un nom par default.");
// System.err.println(" De plus pour un joueur simulé on peut peut attribuer à
// l'algorithme le nombre d'itération (un entier >= 10) souhaité de la manière
// suivante --> s:(<le_nom>):<nb_iteration> ;");
// System.err.println(" et pour le joueur distant on peut préciser son ip
// (numéro ou nom) de cette manière --> r:(<le_nom>):<ip>, sinon le joueur aura
// l'ip par default : localhost.");
// System.err.println(" -Il peut y avoir un 5ème paramètre (un entier) optionnel
// qui est la graine qui servira à générer toutes les autres graines qui
// s'occupent de la partie aléatoire du jeux (distribution des cartes, ...).");
// }
//
// private void printErrorInIterationValueErrorMessage() {
// printBaseErrorMessage();
// System.err.println("Raison : La valeur de la spécification du nombre
// d'itérations ne contient pas que un entier");
// System.err.println("-Solution : Remplacer la valeur du nombre d'itérations
// par une valeur valide, c'est à dire un entier (de type int)");
// }
//
// private void printErrorInPortValueErrorMessage() {
// printBaseErrorMessage();
// System.err.println("Raison : La valeur de la spécification de l'ip ne
// contient pas que un entier");
// System.err.println("-Solution : Remplacer la valeur du port par une valeur
// valide, c'est à dire un entier (de type int)");
// }
//
// private void printNotEnoughIterationErrorMessage() {
// printBaseErrorMessage();
// System.err.println("-Raison : Le nombre d'itérations passées en paramètres
// est inférieur à 10.");
// System.err.println("-Solution : Remplacer la valeur mise dans h:...:<value>
// par quelque chose supérieur ou égale à 10.");
// }
//
// private void printErrorInInstanciationOfDistantPlayer() {
// printBaseErrorMessage();
// System.err.println("-Raison : Une erreur à été lancée à la création du joueur
// distant");
// System.err.println("-Solution : C'est probablement lié à l'ip, vérifier que
// celui-ci est le bon");
// }
//
// private void printErrorTooMuchArgumentForHumanErrorMessage() {
// printBaseErrorMessage();
// System.err.println("-Raison : Le nombre de spécification liées à l'argument
// de création d'un joueur humain est trop important");
// System.err.println("-Solution : Le paramètre h peut prendre une
// spécification: un nom, sous cette forme --> s:<nom>. ");
// }
//
// private void printTooMuchArgumentErrorMessage() {
// printBaseErrorMessage();
// System.err.println("-Raison : Le nombre de spécification liées à l'argument
// est trop important");
// System.err.println("-Solution : Pour chaque type d'argument se réferer à sa
// description :");
// System.err.println(" - s peut prendre deux spécifications: un nom et le
// nombre d'itérations (un entier de type int), sous cette forme -->
// s:<nom>:<nb_iterations>. ");
// System.err.println(" - h peut prendre une spécification: un nom, sous cette
// forme --> s:<nom>. ");
// System.err.println(" - r peut prendre deux spécifications: un nom et le port
// (un entier de type int) pour la connection, sous cette forme -->
// s:<nom>:<port>. ");
// }
//
// private void printInvalidFirstArgumentErrorMessage() {
// printBaseErrorMessage();
// System.err.println("-Raison : L'instruction pour la création d'un joueur
// n'est pas reconnu (n'est pas r, h ou s).");
// System.err.println("-Solution : Remplacer l'argument erroné par une des
// valeurs selon votre souhait (doit être composé d'un seul charactère):");
// System.err.println(" -h si vous souhaitez qu'un joueur humain joue sur votre
// machine");
// System.err.println(" -s si vous souhaitez qu'un joueur simuler joue avec
// vous");
// System.err.println(" -r si vous souhaitez qu'un joueur distant se connecte à
// votre partie");
// }
//
// private void printInvalidSeedErrorMessage() {
// printBaseErrorMessage();
// System.err.println("-Raison : La valeur de la graine passée en paramètre est
// invalide car elle n'est pas composée uniquement d'un entier");
// System.err.println("-Solution : Remplacer la valeur de la graine par
// simplement un entier (de type long)");
// }
// }
