package ch.epfl.javass.jass;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ch.epfl.javass.Preconditions;

/**
 * Squelette d'une carte
 */
public final class Card {
    public final static int SIX_TRUMP_ORDINAL = 0;
    public final static int SEVEN_TRUMP_ORDINAL = 1;
    public final static int EIGHT_TRUMP_ORDINAL = 2;
    public final static int NINE_TRUMP_ORDINAL = 7;
    public final static int TEN_TRUMP_ORDINAL = 3;
    public final static int JACK_TRUMP_ORDINAL = 8;
    public final static int QUEEN_TRUMP_ORDINAL = 4;
    public final static int KING_TRUMP_ORDINAL = 5;
    public final static int ACE_TRUMP_ORDINAL = 6;

    private final int packedCard;

    /**
     * Crée une carte de couleur et rang donnée
     * 
     * @param c
     *            : la couleur
     * @param r
     *            : le rang
     */
    private Card(Color c, Rank r) {
        packedCard = PackedCard.pack(c, r);
    }

    /**
     * Méthode qui permet de créer des cartes sans être un constructeur à partir
     * d'une couleur et d'un rang donné
     * 
     * @param c
     *            : la couleur
     * @param r
     *            : le rang
     * @return la carte
     */
    public static Card of(Color c, Rank r) {
        Card carte = new Card(c, r);
        return carte;
    }

    /**
     * Méthode qui permet de créer des cartes sans être un constructeur à partir
     * d'une carte empaquettée
     * 
     * @param packed
     *            : la carte empaquettée
     * @return la carte
     */
    public static Card ofPacked(int packed) {
        Preconditions.checkArgument(PackedCard.isValid(packed));
        Card carte = new Card(PackedCard.color(packed),
                PackedCard.rank(packed));
        return carte;
    }

    /**
     * @return la carte empaquettée
     */
    public int packed() {
        return packedCard;
    }

    /**
     * @return la couleur de la carte
     */
    public Color color() {
        return PackedCard.color(packedCard);
    }

    /**
     * @return le rang de la carte
     */
    public Rank rank() {
        return PackedCard.rank(packedCard);
    }

    /**
     * Comme dans la classe PackedCard
     */
    public boolean isBetter(Color trump, Card that) {
        return PackedCard.isBetter(trump, packedCard, that.packedCard);
    }

    /**
     * Comme dans la classe PackedCard
     */
    public int points(Color trump) {
        return PackedCard.points(trump, packedCard);
    }

    /**
     * Vérifie si 2 cartes sont les mêmes
     */
    public boolean equals(Object thatO) {
        if (thatO == null) {
            return false;
        } else
            return packedCard == ((Card) thatO).packedCard;
    }

    /**
     * @return la carte empaquettée
     */
    public int hashCode() {
        return packed();
    }


    public String toString() {
        return PackedCard.toString(packedCard);
    }

    /**
     * La classe qui s'occuppe des couleurs
     */
    public enum Color {
        // Énumération des 4 couleurs
        SPADE("\u2660"), HEART("\u2665"), DIAMOND("\u2666"), CLUB("\u2663");

        private String couleur;

        /**
         * Attribue la couleur donnée
         * 
         * @param couleur
         */
        private Color(String couleur) {
            this.couleur = couleur;
        }

        public String toString() {
            return couleur;
        }

        public final static List<Color> ALL = Collections
                .unmodifiableList(Arrays.asList(values()));
        public final static int COUNT = 4;
    }

    /**
     * Classe qui s'occupe des rangs
     */
    public enum Rank {
        // Énumération des rangs
        SIX("6"), SEVEN("7"), EIGHT("8"), NINE("9"), TEN("10"), JACK(
                "J"), QUEEN("Q"), KING("K"), ACE("A");
        private String rang;

        /**
         * Attribue le rang donné
         * 
         * @param rang
         */
        private Rank(String rang) {
            this.rang = rang;
        }

        public String toString() {
            return rang;
        }

        public final static List<Rank> ALL = Collections
                .unmodifiableList(Arrays.asList(values()));
        public final static int COUNT = 9;

        /**
         * @return la position de la carte si c'est un atout
         */
        public int trumpOrdinal() {
            switch (this) {
            case SIX:
                return SIX_TRUMP_ORDINAL;
            case SEVEN:
                return SEVEN_TRUMP_ORDINAL;
            case EIGHT:
                return EIGHT_TRUMP_ORDINAL;
            case TEN:
                return TEN_TRUMP_ORDINAL;
            case QUEEN:
                return QUEEN_TRUMP_ORDINAL;
            case KING:
                return KING_TRUMP_ORDINAL;
            case ACE:
                return ACE_TRUMP_ORDINAL;
            case NINE:
                return NINE_TRUMP_ORDINAL;
            case JACK:
                return JACK_TRUMP_ORDINAL;
            }
            return 0;
        }

    }

}
