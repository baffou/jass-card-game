package ch.epfl.javass.jass;

/**
 * @author Shayan
 * On déclare ici les constantes qui nous serons utiles tout le long du projet
 */
public interface Jass {
    
    final static int HAND_SIZE = 9;
    final static int TRICKS_PER_TURN = 9;
    final static int WINNING_POINTS = 1000;
    final static int MATCH_ADDITIONAL_POINTS = 100;
    final static int LAST_TRICK_ADDITIONAL_POINTS = 5;
}
