package ch.epfl.javass.jass;

import java.util.StringJoiner;

import ch.epfl.javass.bits.Bits64;
import ch.epfl.javass.jass.Card.Color;
import ch.epfl.javass.jass.Card.Rank;

public final class PackedCardSet {

	public static final int CARDSET_HEART_START = 16;
	public static final int CARDSET_CLUB_START = 32;
	public static final int CARDSET_DIAMOND_START = 48;
	public static final int CARDSET_COLOR_EMPTY_SIZE = 7;
	public static final int CARDSET_COLOR_SIZE = 16;
	public final static int NUMBER_OF_CARDS_PER_COLOR = 9;
	public final static long ALL_CARDS_OF_A_COLOR = 0b111111111L;

	public static final long EMPTY = 0L;
	public static final long ALL_CARDS = Bits64.pack(ALL_CARDS_OF_A_COLOR, NUMBER_OF_CARDS_PER_COLOR, EMPTY,
			CARDSET_COLOR_EMPTY_SIZE)
			| Bits64.pack(ALL_CARDS_OF_A_COLOR, NUMBER_OF_CARDS_PER_COLOR, EMPTY,
					CARDSET_COLOR_EMPTY_SIZE) << CARDSET_HEART_START
			| Bits64.pack(ALL_CARDS_OF_A_COLOR, NUMBER_OF_CARDS_PER_COLOR, EMPTY,
					CARDSET_COLOR_EMPTY_SIZE) << CARDSET_CLUB_START
			| Bits64.pack(ALL_CARDS_OF_A_COLOR, NUMBER_OF_CARDS_PER_COLOR, EMPTY,
					CARDSET_COLOR_EMPTY_SIZE) << CARDSET_DIAMOND_START;

	private final static long[][] trumpTableau = initTrumpTab();
	private final static long[] colorTableau = initColorTab();

	public static boolean isValid(long pkCardSet) {
		long cardSet = Bits64.extract(pkCardSet, NUMBER_OF_CARDS_PER_COLOR, CARDSET_COLOR_EMPTY_SIZE)
				| Bits64.extract(pkCardSet, NUMBER_OF_CARDS_PER_COLOR + CARDSET_HEART_START, CARDSET_COLOR_EMPTY_SIZE)
				| Bits64.extract(pkCardSet, NUMBER_OF_CARDS_PER_COLOR + CARDSET_CLUB_START, CARDSET_COLOR_EMPTY_SIZE)
				| Bits64.extract(pkCardSet, NUMBER_OF_CARDS_PER_COLOR + CARDSET_DIAMOND_START,
						CARDSET_COLOR_EMPTY_SIZE);
		return cardSet == EMPTY;
	}

	/**
	 * @param pkCard
	 * @return les cartes atout meilleures que la carte donnée
	 */
	public static long trumpAbove(int pkCard) {
		return trumpTableau[PackedCard.color(pkCard).ordinal()][PackedCard.rank(pkCard).trumpOrdinal()];
	}

	/**
	 * Crée un PackedCardSet avec seulement la carte en paramètres
	 * 
	 * @param pkCard
	 * @return le PackedCardSet
	 */
	public static long singleton(int pkCard) {
		return 1L << Bits64.extract(pkCard, PackedCard.RANK_START, PackedCard.RANK_SIZE)
				+ Bits64.extract(pkCard, PackedCard.COLOR_START, PackedCard.COLOR_SIZE) * CARDSET_COLOR_SIZE;
	}

	/**
	 * Méthode qui vérifie si un PackedCardSet est vide
	 * 
	 * @param pkCardSet
	 * @return true ou false
	 */
	public static boolean isEmpty(long pkCardSet) {
		return pkCardSet == EMPTY;
	}

	/**
	 * Méthode qui renvoie la taille d'un PackedCardSet
	 * 
	 * @param pkCardSet
	 * @return la taille
	 */
	public static int size(long pkCardSet) {
		return Long.bitCount(pkCardSet);
	}

	/**
	 * Méthode qui récupère une carte à un index donné d'un PackedCardSet
	 * 
	 * @param pkCardSet
	 * @param index
	 * @return la carte
	 */
	public static int get(long pkCardSet, int index) {
		long l = pkCardSet;
		for (int i = 0; i < index; i++) {
			l = l & ~Long.lowestOneBit(l);
		}
		return PackedCard.pack(Color.values()[(Long.numberOfTrailingZeros(l)) / CARDSET_COLOR_SIZE],
				Rank.values()[Long.numberOfTrailingZeros(l)
						- CARDSET_COLOR_SIZE * ((Long.numberOfTrailingZeros(l)) / CARDSET_COLOR_SIZE)]);
	}

	/**
	 * Méthode qui ajoute une carte à un PackedCardSet
	 * 
	 * @param pkCardSet
	 * @param pkCard
	 * @return le nouveau PackedCardSet
	 */
	public static long add(long pkCardSet, int pkCard) {
		return pkCardSet | singleton(pkCard);
	}

	/**
	 * Méthode qui enlève une carte d'un PackedCardSet
	 * 
	 * @param pkCardSet
	 * @param pkCard
	 * @return le nouveau PackedCardSet
	 */
	public static long remove(long pkCardSet, int pkCard) {
		return pkCardSet & ~(singleton(pkCard));
	}

	/**
	 * Méthode qui vérifie si un PackedCardSet contient une carte donnée
	 * 
	 * @param pkCardSet
	 * @param pkCard
	 * @return true ou false
	 */
	public static boolean contains(long pkCardSet, int pkCard) {
		return pkCardSet == (pkCardSet | singleton(pkCard));
	}

	/**
	 * Méthode qui retourne un PackedCardSet avec toutes les autres cartes que les
	 * cartes PackedCardSet donnés
	 * 
	 * @param pkCardSet
	 * @return le PackedCardSet complément
	 */
	public static long complement(long pkCardSet) {
		return (~pkCardSet) & ALL_CARDS;
	}

	/**
	 * Méthode qui unit deux PackedCardSet
	 * 
	 * @param pkCardSet1
	 * @param pkCardSet2
	 * @return le nouveau PackedCardSet
	 */
	public static long union(long pkCardSet1, long pkCardSet2) {
		return pkCardSet1 | pkCardSet2;
	}

	/**
	 * Méthode qui renvoie le CardSet des cartes que les deux PackedCardSet donnés
	 * ont en commun
	 * 
	 * @param pkCardSet1
	 * @param pkCardSet2
	 * @return le PackedCardSet
	 */
	public static long intersection(long pkCardSet1, long pkCardSet2) {
		return pkCardSet1 & pkCardSet2;
	}

	/**
	 * Méthode qui enlève à un PackedCardSet a, les cartes cartes qu'il a en commun
	 * avec un PackedCardSet b
	 * 
	 * @param pkCardSet1
	 * @param pkCardSet2
	 * @return le nouveau PackedCardSet a
	 */
	public static long difference(long pkCardSet1, long pkCardSet2) {
		return pkCardSet1 & ~(intersection(pkCardSet1, pkCardSet2));
	}

	/**
	 * Méthode qui modifie un PackedCardSet en ne gardant que les cartes de la
	 * couleur donnée
	 * 
	 * @param pkCardSet
	 * @param color
	 * @return le nouveau PackedCardSet
	 */
	public static long subsetOfColor(long pkCardSet, Card.Color color) {
		return colorTableau[color.ordinal()] & pkCardSet;
	}

	/**
	 * Méthode qui affiche le PackedCardSet donné
	 * 
	 * @param pkCardSet
	 * @return l'affichage
	 */
	public static String toString(long pkCardSet) {
		StringJoiner j = new StringJoiner(",", "{", "}");
		String[] tab = new String[PackedCardSet.size(pkCardSet)];
		for (int i = 0; i < PackedCardSet.size(pkCardSet); i++) {
			tab[i] = PackedCard.color(PackedCardSet.get(pkCardSet, i)).toString()
					+ PackedCard.rank(PackedCardSet.get(pkCardSet, i)).toString();
		}
		for (String s : tab) {
			j.add(s);
		}
		return j.toString();
	}

	/**
	 * Crée un tableau de length 4, qui a dans chaque case toutes les cartes d0une
	 * couleur
	 * 
	 * @return le tableau
	 */
	private final static long[] initColorTab() {
		long[] tab = new long[Card.Color.COUNT];
		for (int i = 0; i < Card.Color.COUNT; i++) {
			tab[i] = ALL_CARDS_OF_A_COLOR << CARDSET_COLOR_SIZE * i;
		}
		return tab;
	}

	/**
	 * Crée un tableau de dimension, qui pour chaque carte donne la liste des cartes
	 * atout meilleures qu'elle
	 * 
	 * @return le tableau
	 */
	private final static long[][] initTrumpTab() {
		long[][] tab = new long[Card.Color.COUNT][Card.Rank.COUNT];
		for (int i = 0; i < Card.Color.COUNT; i++) { // pr les couleurs
			for (int j = 0; j < Card.Rank.COUNT; j++) { // pr les rangs
				switch (j) {
				case 0:
					tab[i][j] = 0b111111110L << i * CARDSET_COLOR_SIZE;
					break;
				case 1:
					tab[i][j] = 0b111111100L << i * CARDSET_COLOR_SIZE;
					break;
				case 2:
					tab[i][j] = 0b111111000L << i * CARDSET_COLOR_SIZE;
					break;
				case 3:
					tab[i][j] = 0b111101000L << i * CARDSET_COLOR_SIZE;
					break;
				case 4:
					tab[i][j] = 0b110101000L << i * CARDSET_COLOR_SIZE;
					break;
				case 5:
					tab[i][j] = 0b100101000L << i * CARDSET_COLOR_SIZE;
					break;
				case 6:
					tab[i][j] = 0b000101000L << i * CARDSET_COLOR_SIZE;
					break;
				case 7:
					tab[i][j] = 0b000100000L << i * CARDSET_COLOR_SIZE;
					break;
				case 8:
					tab[i][j] = 0b000000000L << i * CARDSET_COLOR_SIZE;
					break;
				}
			}
		}
		return tab;
	}
}
