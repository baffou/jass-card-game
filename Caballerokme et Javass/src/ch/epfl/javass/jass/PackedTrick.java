package ch.epfl.javass.jass;

import ch.epfl.javass.bits.Bits32;
import ch.epfl.javass.jass.Card.Color;
import ch.epfl.javass.jass.Card.Rank;

public final class PackedTrick {

	private final static int CARD_SIZE = 6;
	private final static int CARD0_INDEX = 0;
	private final static int CARD1_INDEX = 6;
	private final static int CARD2_INDEX = 12;
	private final static int CARD3_INDEX = 18;

	public final static int INVALID = ~0;
	private final static int INDEX_MAX = 1 << 3;
	private final static int INDEX_START = 24;
	private final static int INDEX_SIZE = 4;

	private final static int NUMBER_OF_CARD = 4;
	private final static int TRUMP_INDEX = 30;
	private final static int TRUMP_SIZE = 2;
	private final static int PLAYER_INDEX = 28;
	private final static int PLAYER_SIZE = 2;
	private final static int INDEX_0 = 0;

	private PackedTrick() {
	}

	/**
	 * Méthode qui vérifie si un trick donné est valide
	 * 
	 * @param pkTrick
	 * @return true ou false
	 */
	public static boolean isValid(int pkTrick) {
		boolean condition1 = PackedCard.isValid(Bits32.extract(pkTrick, CARD0_INDEX, CARD_SIZE));
		boolean condition2 = PackedCard.isValid(Bits32.extract(pkTrick, CARD1_INDEX, CARD_SIZE));
		boolean condition3 = PackedCard.isValid(Bits32.extract(pkTrick, CARD2_INDEX, CARD_SIZE));
		boolean condition4 = PackedCard.isValid(Bits32.extract(pkTrick, CARD3_INDEX, CARD_SIZE));

		boolean condition1b = Bits32.extract(pkTrick, CARD0_INDEX, CARD_SIZE) == PackedCard.INVALID;
		boolean condition2b = Bits32.extract(pkTrick, CARD1_INDEX, CARD_SIZE) == PackedCard.INVALID;
		boolean condition3b = Bits32.extract(pkTrick, CARD2_INDEX, CARD_SIZE) == PackedCard.INVALID;
		boolean condition4b = Bits32.extract(pkTrick, CARD3_INDEX, CARD_SIZE) == PackedCard.INVALID;

		if (Bits32.extract(pkTrick, INDEX_START, INDEX_SIZE) <= INDEX_MAX) {
			if (condition1 && condition2 && condition3 && condition4) {
				return true;
			}
			if (condition1 && condition2 && condition3 && condition4b) {
				return true;
			}
			if (condition1 && condition2 && condition3b && condition4b) {
				return true;
			}
			if (condition1 && condition2b && condition3b && condition4b) {
				return true;
			}
			if (condition1b && condition2b && condition3b && condition4b) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Méthode qui retourne le pli empaqueté vide — c-à-d sans aucune
	 * carte — d'index 0 avec l'atout et le premier joueur donnés
	 * 
	 * @param trump
	 * @param firstPlayer
	 * @return le pli
	 */
	public static int firstEmpty(Color trump, PlayerId firstPlayer) {
		return Bits32.pack(PackedCard.INVALID, CARD_SIZE, PackedCard.INVALID, CARD_SIZE, PackedCard.INVALID, CARD_SIZE,
				PackedCard.INVALID, CARD_SIZE, INDEX_0, INDEX_SIZE, firstPlayer.ordinal(), PLAYER_SIZE, trump.ordinal(),
				TRUMP_SIZE);
	}

	/**
	 * Méthode qui retourne le pli empaqueté vide suivant celui donné (supposé
	 * plein), c-à-d le pli vide dont l'atout est identique à celui du pli donné,
	 * l'index est le successeur de celui du pli donné et le premier joueur est le
	 * vainqueur du pli donné ; si le pli donné est le dernier du tour, alors le pli
	 * invalide (INVALID ci-dessus) est retourné
	 * 
	 * @param pkTrick
	 * @return le pli
	 */
	public static int nextEmpty(int pkTrick) {
		if (index(pkTrick) == INDEX_MAX) {
			return INVALID;
		}
		return Bits32.pack(PackedCard.INVALID, CARD_SIZE, PackedCard.INVALID, CARD_SIZE, PackedCard.INVALID, CARD_SIZE,
				PackedCard.INVALID, CARD_SIZE, index(pkTrick) + 1, INDEX_SIZE, winningPlayer(pkTrick).ordinal(),
				PLAYER_SIZE, trump(pkTrick).ordinal(), TRUMP_SIZE);
	}

	/**
	 * Méthode qui retourne vrai ssi le pli est le dernier du tour, c-à-d si son
	 * index vaut 8
	 * 
	 * @param pkTrick
	 * @return true ou false
	 */
	public static boolean isLast(int pkTrick) {
		if (Bits32.extract(pkTrick, INDEX_START, INDEX_SIZE) == INDEX_MAX) {
			return true;
		}
		return false;
	}

	/**
	 * Méthode qui retourne vrai ssi le pli est vide, c-à-d s'il ne contient aucune
	 * carte
	 * 
	 * @param pkTrick
	 * @return true ou false
	 */
	public static boolean isEmpty(int pkTrick) {
		boolean condition1 = Bits32.extract(pkTrick, CARD0_INDEX, CARD_SIZE) == PackedCard.INVALID;
		boolean condition2 = Bits32.extract(pkTrick, CARD1_INDEX, CARD_SIZE) == PackedCard.INVALID;
		boolean condition3 = Bits32.extract(pkTrick, CARD2_INDEX, CARD_SIZE) == PackedCard.INVALID;
		boolean condition4 = Bits32.extract(pkTrick, CARD3_INDEX, CARD_SIZE) == PackedCard.INVALID;

		return condition1 & condition2 & condition3 & condition4;
	}

	/**
	 * Méthode qui retourne vrai ssi le pli est plein, c-à-d s'il contient 4 cartes
	 * 
	 * @param pkTrick
	 * @return true ou false
	 */
	public static boolean isFull(int pkTrick) {
		boolean condition1 = Bits32.extract(pkTrick, CARD0_INDEX, CARD_SIZE) != PackedCard.INVALID;
		boolean condition2 = Bits32.extract(pkTrick, CARD1_INDEX, CARD_SIZE) != PackedCard.INVALID;
		boolean condition3 = Bits32.extract(pkTrick, CARD2_INDEX, CARD_SIZE) != PackedCard.INVALID;
		boolean condition4 = Bits32.extract(pkTrick, CARD3_INDEX, CARD_SIZE) != PackedCard.INVALID;

		return condition1 & condition2 & condition3 & condition4;
	}

	/**
	 * Méthode qui retourne la taille du pli, c-à-d le nombre de cartes qu'il
	 * contient
	 * 
	 * @param pkTrick
	 * @return la taille
	 */
	public static int size(int pkTrick) {
		int nmb = 0;
		for (int i = 0; i < NUMBER_OF_CARD; i++) {
			if (Bits32.extract(pkTrick, CARD0_INDEX + i * CARD_SIZE, CARD_SIZE) != PackedCard.INVALID) {
				nmb++;
			} else {
				break;
			}
		}
		return nmb;
	}

	/**
	 * Méthode qui retourne l'atout du pli
	 * 
	 * @param pkTrick
	 * @return
	 */
	public static Color trump(int pkTrick) {
		return Card.Color.values()[Bits32.extract(pkTrick, TRUMP_INDEX, TRUMP_SIZE)];
	}

	/**
	 * Méthode qui retourne le joueur d'index donné dans le pli, le joueur d'index 0
	 * étant le premier du pli
	 * 
	 * @param pkTrick
	 * @param index
	 * @return
	 */
	public static PlayerId player(int pkTrick, int index) {
		return PlayerId.values()[(Bits32.extract(pkTrick, PLAYER_INDEX, PLAYER_SIZE) + index) % PlayerId.COUNT];
	}

	/**
	 * Méthode qui retourne l'index du pli
	 * 
	 * @param pkTrick
	 * @return
	 */
	public static int index(int pkTrick) {
		return Bits32.extract(pkTrick, INDEX_START, INDEX_SIZE);
	}

	/**
	 * Méthode qui retourne la version empaquetée de la carte du pli à l'index donné
	 * (supposée avoir été posée)
	 * 
	 * @param pkTrick
	 * @param index
	 * @return
	 */
	public static int card(int pkTrick, int index) {
		return Bits32.extract(pkTrick, index * CARD_SIZE, CARD_SIZE);
	}

	/**
	 * Méthode qui retourne un pli identique à celui donné (supposé non plein), mais
	 * à laquelle la carte donnée a été ajoutée
	 * 
	 * @param pkTrick
	 * @param pkCard
	 * @return
	 */
	public static int withAddedCard(int pkTrick, int pkCard) {
		return (pkTrick & ~(PackedCard.INVALID << PackedTrick.size(pkTrick) * CARD_SIZE))
				| pkCard << PackedTrick.size(pkTrick) * CARD_SIZE;
	}

	/**
	 * Méthode qui retourne la couleur de base du pli, c-à-d la couleur de sa
	 * première carte (supposée avoir été jouée)
	 * 
	 * @param pkTrick
	 * @return
	 */
	public static Color baseColor(int pkTrick) {
		return PackedCard.color(Bits32.extract(pkTrick, CARD0_INDEX, CARD_SIZE));
	}

	/**
	 * Méthode qui retourne le sous-ensemble (empaqueté) des cartes de la main
	 * pkHand qui peuvent être jouées comme prochaine carte du pli pkTrick (supposé
	 * non plein)
	 * 
	 * @param pkTrick
	 * @param pkHand
	 * @return
	 */
	public static long playableCards(int pkTrick, long pkHand) {
        long playableCards = PackedCardSet.EMPTY;
        int actualCard = 0;
        Card.Color trump = trump(pkTrick);
        Card.Color baseColor = baseColor(pkTrick);
        
        if (isEmpty(pkTrick)) {
            return pkHand;
        } 
        
        else {
        	//on ajoute toutes les cartes de la couleur de base de notre main dans les playableCards;
            for (int i = 0; i < PackedCardSet.size(pkHand); i++) {
                actualCard = PackedCardSet.get(pkHand, i);

                if (PackedCard.color(actualCard).equals(baseColor)) {
                    playableCards = PackedCardSet.add(playableCards,
                            actualCard);
                }
                
            }
            
            //si notre main n'est composée que de cartes de la couleur de base alors les playablesCards sont notre main
            if ((playableCards & ~PackedCardSet.subsetOfColor(pkHand, baseColor)) == playableCards ) {
                playableCards = pkHand;
            }

            //on reparcours toute notre main
            for (int i = 0; i < PackedCardSet.size(pkHand); i++) {
                actualCard = PackedCardSet.get(pkHand, i);
                
                /*si la carte actuelle est de couleur trump mais que trump n'est pas la couleur de base alors on va 
                vérifier que la carte actuelle est meilleur que les autres cartes de type trump pour éviter de sous couper */
                if (PackedCard.color(actualCard) == trump
                        && baseColor != trump) {
                    int bestCard = actualCard;
                    for (int j = 0; j < size(pkTrick); j++) {
                        int actualTestCard = card(pkTrick, j);
                        
                        if (PackedCard.color(actualTestCard) == trump) {
                            if (PackedCard.isBetter(trump,
                                    actualTestCard, bestCard)) {
                                bestCard = actualTestCard;
                            }
                        }
                    }
                    
                    if (bestCard == actualCard && (PackedCardSet.subsetOfColor(playableCards, baseColor) != PackedCardSet.EMPTY)) {
                        playableCards = PackedCardSet.add(playableCards,
                                actualCard);
                    }
                    
                    /* toutes les conditions ci-dessous sont pour vérifier que si on a que le valet d'atout comme carte de couleur atout alors on est pas obliger
                    de le jouer */
                    boolean isTheBourgTheOnlyTrumpWeHave;
                    if(PackedCardSet.contains((PackedCardSet.subsetOfColor(playableCards, trump)), PackedCard.pack(trump, Rank.JACK))) {
                        if(PackedCardSet.subsetOfColor(playableCards, trump) == PackedCardSet.singleton(PackedCard.pack(trump, Rank.JACK))) {
                            isTheBourgTheOnlyTrumpWeHave = false;
                        }
                        else {
                            isTheBourgTheOnlyTrumpWeHave = true;
                        }
                    }
                    else {
                        isTheBourgTheOnlyTrumpWeHave = false;
                    }
                    
                    if (bestCard != actualCard && (PackedCardSet.subsetOfColor(playableCards, baseColor) == PackedCardSet.EMPTY)
                          && isTheBourgTheOnlyTrumpWeHave) {
                      playableCards = PackedCardSet.remove(playableCards,
                              actualCard);
                  }
             
                    
                }

            }

        }
        
        if (PackedCardSet.isEmpty(playableCards)
                || (PackedCardSet.size(playableCards) == 1
                        &&  PackedCardSet.contains((PackedCardSet.subsetOfColor(playableCards, trump)), PackedCard.pack(trump, Rank.JACK)))) {
            playableCards = pkHand;
        }

        return playableCards;
    }

	/**
	 * Méthode qui retourne la valeur du pli, en tenant compte des « 5 de der »
	 * 
	 * @param pkTrick
	 * @return
	 */
	public static int points(int pkTrick) {
		int additionnalPoints = 0;
		if (index(pkTrick) == INDEX_MAX) {
			additionnalPoints += Jass.LAST_TRICK_ADDITIONAL_POINTS;
		}
		for (int i = 0; i < NUMBER_OF_CARD; i++) {
			if (PackedCard.isValid(card(pkTrick, i))) {
				additionnalPoints += PackedCard.points(trump(pkTrick), card(pkTrick, i));
			}
		}

		return additionnalPoints;
	}

	/**
	 * Méthode qui retourne l'identité du joueur menant le pli (supposé non vide)
	 * 
	 * @param pkTrick
	 * @return
	 */
	public static PlayerId winningPlayer(int pkTrick) {
		PlayerId player = player(pkTrick, 0);
		int bestCard = card(pkTrick, 0);

		for (int i = 0; i < size(pkTrick); i++) {
			if (PackedCard.isBetter(trump(pkTrick), card(pkTrick, i), bestCard)) {
				bestCard = card(pkTrick, i);
				player = player(pkTrick, i);
			}
		}

		return player;
	}

	/**
	 * Méthode qui retourne l'affichage de la trick
	 * 
	 * @param pkTrick
	 * @return l'affichage
	 */
	public static String toString(int pkTrick) {
		String s = "";
		for (int i = 0; i < size(pkTrick); i++) {
			s += (PackedCard.toString(card(pkTrick, i)) + ", ");
		}
		return s;
	}

}