package ch.epfl.javass.jass;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Classe énumérée des joueurs de la partie
 */
public enum TeamId {
	TEAM_1, TEAM_2;

	public final static List<TeamId> ALL = Collections.unmodifiableList(Arrays.asList(values()));
	public final static int COUNT = 2;
	
	public TeamId other() {
		return values()[(this.ordinal() + 1) % 2];
	}
}
