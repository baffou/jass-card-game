package ch.epfl.javass.jass;

import java.util.Map;

import ch.epfl.javass.jass.Card.Color;

public final class PacedPlayer implements Player {

	private Player player;
	private double time;

	/**
	 * Constructeur de PacedPlayer
	 * 
	 * @param underlyingPlayer
	 * @param minTime
	 */
	public PacedPlayer(Player underlyingPlayer, double minTime) {
		player = underlyingPlayer;
		time = minTime;
	}

	/*
	 * L'utilité des prochaines méthodes sont décrites dans l'interface Player
	 */
	@Override
	public Card cardToPlay(TurnState state, CardSet hand) {
		long t1 = System.currentTimeMillis();
		Card card = player.cardToPlay(state, hand);
		long t2 = System.currentTimeMillis();
		long diffTime = t2 - t1;
		long newtime = ((long) time) * 1000;
		try {
		    if(diffTime < newtime)
		        Thread.sleep(newtime - diffTime);
		} catch (InterruptedException e) {
			/* ignore */ }
		return card;
	}

	public void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
		player.setPlayers(ownId, playerNames);
	}

	public void updateHand(CardSet newHand) {
		player.updateHand(newHand);
	}

	public void setTrump(Color trump) {
		player.setTrump(trump);
	}

	public void updateTrick(Trick newTrick) {
		player.updateTrick(newTrick);
	}

	public void updateScore(Score score) {
		player.updateScore(score);
	}

	public void setWinningTeam(TeamId winningTeam) {
		player.setWinningTeam(winningTeam);
	}

}
