package ch.epfl.javass.jass;

/**
 * Classe pour manipuler et visualiser les scores de la partie
 *
 */
public final class Score {
	final static public Score INITIAL = new Score(0L);

	// Attribut représentant le score actuel dans sa version empaqueté
	private final long packedScore;

	private Score(long packed) {
		packedScore = packed;
	}

	/**
	 * Méthode permettant de créer des instance en évitant d'appeller directement le
	 * constructeur
	 * 
	 * @param packed
	 * @return
	 */
	public static Score ofPacked(long packed) {
		if (!PackedScore.isValid(packed)) {
			throw new IllegalArgumentException();
		}
		Score score = new Score(packed);
		return score;
	}

	public long packed() {
		return packedScore;
	}

	/**
	 * Appelle la méthode de meme nom dans packedScore
	 * 
	 * @param t
	 * @return
	 */
	public int turnTricks(TeamId t) {
		return PackedScore.turnTricks(packedScore, t);
	}

	/**
	 * Appelle la méthode de meme nom dans packedScore
	 * 
	 * @param t
	 * @return
	 */
	public int turnPoints(TeamId t) {
		return PackedScore.turnPoints(packedScore, t);
	}

	/**
	 * Appelle la méthode de meme nom dans packedScore
	 * 
	 * @param t
	 * @return
	 */
	public int gamePoints(TeamId t) {
		return PackedScore.gamePoints(packedScore, t);
	}

	/**
	 * Appelle la méthode de meme nom dans packedScore
	 * 
	 * @param t
	 * @return
	 */
	public int totalPoints(TeamId t) {
		return PackedScore.totalPoints(packedScore, t);
	}

	/**
	 * Méthode qui se comporte comme la méthode de même nom de PackedScore ou lève
	 * l'exception IllegalArgumentException si le nombre de points donné est
	 * inférieur à 0
	 * 
	 * @param winningTeam
	 * @param trickPoints
	 * @return le Score
	 */
	public Score withAdditionalTrick(TeamId winningTeam, int trickPoints) {
		Score newScore = new Score(PackedScore.withAdditionalTrick(packedScore, winningTeam, trickPoints));
		return newScore;
	}

	/**
	 * Appelle la méthode de meme nom dans packedScore et retourne un Score mis a
	 * jour
	 * 
	 * @return
	 */
	public Score nextTurn() {
		Score newScore = new Score(PackedScore.nextTurn(packedScore));
		return newScore;
	}

	/**
	 * Vérifie si l'objet donné en parametre est bien un score et si oui vérifie si
	 * il est égal au score actuel
	 */
	public boolean equals(Object score) {
		if (score == null) {
			return false;
		} else
			return packedScore == ((Score) score).packedScore;
	}

	public int hashCode() {
		return Long.hashCode(packedScore);
	}

	/**
	 * Comme dans PackedScore
	 */
	public String toString() {
		return PackedScore.toString(packedScore);
	}

}