package ch.epfl.javass.jass;

import java.util.Map;

import ch.epfl.javass.jass.Card.Color;

/**
 * Interface qui représente un joueur
 */
public interface Player {
	/**
	 * Méthode qui retourne la carte que le joueur désire jouer, sachant que l'état
	 * actuel du tour est celui décrit par state et que le joueur a les cartes hand
	 * en main.
	 * 
	 * @param state
	 * @param hand
	 * @return
	 */
	Card cardToPlay(TurnState state, CardSet hand);

	/**
	 * Méthode qui est appelée une seule fois en début de partie pour informer le
	 * joueur qu'il a l'identité ownId et que les différents joueurs (lui inclus)
	 * sont nommés selon le contenu de la table associative playerNames
	 * 
	 * @param ownId
	 * @param playerNames
	 */
	default void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
	}

	/**
	 * Méthode qui est appelée chaque fois que la main du joueur change — soit en
	 * début de tour lorsque les cartes sont distribuées, soit après qu'il ait joué
	 * une carte — pour l'informer de sa nouvelle main
	 * 
	 * @param newHand
	 */
	default void updateHand(CardSet newHand) {
	}

	/**
	 * Méthode qui est appelée chaque fois que l'atout change — c-à-d au début de
	 * chaque tour — pour informer le joueur de l'atout
	 * 
	 * @param trump
	 */
	default void setTrump(Color trump) {
	}

	/**
	 * Méthode qui est appelée chaque fois que le pli change, c-à-d chaque fois
	 * qu'une carte est posée ou lorsqu'un pli terminé est ramassé et que le
	 * prochain pli (vide) le remplace
	 * 
	 * @param newTrick
	 */
	default void updateTrick(Trick newTrick) {
	}

	/**
	 * Méthode qui est appelée chaque fois que le score change, c-à-d chaque fois
	 * qu'un pli est ramassé
	 * 
	 * @param score
	 */
	default void updateScore(Score score) {
	}

	/**
	 * Méthode qui est appelée une seule fois dès qu'une équipe à gagné en obtenant
	 * 1000 points ou plus.
	 * 
	 * @param winningTeam
	 */
	default void setWinningTeam(TeamId winningTeam) {
	}

}
