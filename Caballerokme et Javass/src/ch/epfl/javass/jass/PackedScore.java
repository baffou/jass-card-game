package ch.epfl.javass.jass;

import ch.epfl.javass.bits.Bits64;

//Classe ayant les méthodes pour visualiser et manipuler
//Score représenté dans sa version empaquetée
public final class PackedScore {
	public final static int TRICK_MAX = 9;
	public final static int TRICK_BITS_SIZE = 4;
	public final static int TURN_POINTS_MAX = 257;
	public final static int TURN_POINTS_BITS_SIZE = 9;
	public final static int GAME_POINTS_MAX = 2000;
	public final static int GAME_POINTS_BITS_SIZE = 11;
	public final static int NB_FREE_BITS = 8;
	public final static int NB_BITS_TEAM = 32;

	// constante long valant 0
	final static public long INITIAL = 0L;

	/**
	 * @author Jérémy
	 * @param pkScore
	 *            : score courant empaqueté
	 * @return un boolean qui est vrai si les six valeurs clés sont bien comprises
	 *         entre les bornes prédéfinis et si les bits inutilisés vallent bien 0
	 */
	public static boolean isValid(long pkScore) {
		long extracted1 = Bits64.extract(pkScore, 0, Integer.SIZE);
		long extracted2 = Bits64.extract(pkScore, Integer.SIZE, Integer.SIZE);
		long decale1 = extracted1 >>> TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE + GAME_POINTS_BITS_SIZE;
		long decale2 = extracted2 >>> TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE + GAME_POINTS_BITS_SIZE;

		if (decale1 != INITIAL || decale2 != INITIAL || turnTricks(pkScore, TeamId.TEAM_1) > TRICK_MAX
				|| turnTricks(pkScore, TeamId.TEAM_2) > TRICK_MAX
				|| turnPoints(pkScore, TeamId.TEAM_1) > TURN_POINTS_MAX
				|| turnPoints(pkScore, TeamId.TEAM_2) > TURN_POINTS_MAX
				|| gamePoints(pkScore, TeamId.TEAM_1) > GAME_POINTS_MAX
				|| gamePoints(pkScore, TeamId.TEAM_2) > GAME_POINTS_MAX) {

			return false;
		}
		return true;
	}

	/**
	 * @author Jérémy
	 * @param turnTricks1
	 *            : points du plis pr l'équipe 1
	 * @param turnPoints1
	 *            : points des tours pr l'équipe 1
	 * @param gamePoints1
	 *            : points de la partie pr l'équipe 1
	 * @param turnTricks2
	 *            : points du plis pr l'équipe 2
	 * @param turnPoints2
	 *            : points des tours pr l'équipe 2
	 * @param gamePoints2
	 *            : points de la partie pr l'équipe 2
	 * @return une version empaquetée du score représenté par tte les valeurs misent
	 *         en paramètre
	 */
	public static long pack(int turnTricks1, int turnPoints1, int gamePoints1, int turnTricks2, int turnPoints2,
			int gamePoints2) {
		long packed = Bits64.pack(turnTricks1, TRICK_BITS_SIZE, turnPoints1, TURN_POINTS_BITS_SIZE)
				| (Bits64.pack(gamePoints1, GAME_POINTS_BITS_SIZE, INITIAL, NB_FREE_BITS) << TRICK_BITS_SIZE
						+ TURN_POINTS_BITS_SIZE)
				| (Bits64.pack(turnTricks2, TRICK_BITS_SIZE, turnPoints2, TURN_POINTS_BITS_SIZE) << Integer.SIZE)
				| (Bits64.pack(gamePoints2, GAME_POINTS_BITS_SIZE, INITIAL, NB_FREE_BITS) << NB_BITS_TEAM
						+ TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE);

		return packed;
	}

	/**
	 * @author Jérémy
	 * @param pkScore
	 *            : Score empaqueté
	 * @param t
	 *            : "Numéro" de l'équipe
	 * @return extracted1/2_plis : nb de plis emportés pr l'équipe donnée dans le
	 *         tour courant
	 */
	public static int turnTricks(long pkScore, TeamId t) {
		if (t.ordinal() == 0) {
			return (int) Bits64.extract(pkScore, 0, TRICK_BITS_SIZE);
		} else {
			return (int) Bits64.extract(pkScore, NB_BITS_TEAM, TRICK_BITS_SIZE);
		}
	}

	/**
	 * @author Jérémy
	 * @param pkScore
	 *            : Score empaqueté
	 * @param t
	 *            : "Numéro" de l'équipe
	 * @return extracted1/2_PtTour : nb de points emportés pr l'équipe donnée dans
	 *         le tour courant
	 */
	public static int turnPoints(long pkScore, TeamId t) {
		if (t.ordinal() == 0) {
			return (int) Bits64.extract(pkScore, TRICK_BITS_SIZE, TURN_POINTS_BITS_SIZE);
		} else {
			return (int) Bits64.extract(pkScore, NB_BITS_TEAM + TRICK_BITS_SIZE, TURN_POINTS_BITS_SIZE);
		}
	}

	/**
	 * @author Jérémy
	 * @param pkScore
	 *            : Score empaqueté
	 * @param t
	 *            : "Numéro" de l'équipe
	 * @return extracted1/2_PtPartie : nb de points emportés par l'équipe t dans les
	 *         tours précédants (sans compter l actuel)
	 */
	public static int gamePoints(long pkScore, TeamId t) {
		if (t.ordinal() == 0) {
			return (int) Bits64.extract(pkScore, TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE, GAME_POINTS_BITS_SIZE);
		} else {
			return (int) Bits64.extract(pkScore, NB_BITS_TEAM + TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE,
					GAME_POINTS_BITS_SIZE);
		}
	}

	/**
	 * 
	 * @param pkScore
	 *            : score empaqueté
	 * @param t
	 *            : numéro de l'équipe
	 * @return le nb de pt total de l'équipe donné
	 */
	public static int totalPoints(long pkScore, TeamId t) {
		return turnPoints(pkScore, t) + gamePoints(pkScore, t);
	}

	/**
	 * 
	 * @param pkScore
	 *            : score empaqueté
	 * @param winningTeam
	 *            : équipe gagnante
	 * @param trickPoints
	 *            : nb de points remportés dans le plis
	 * @return newPkScore : nouveau score empaqueté mis a jour en tenant compte du
	 *         fait que winningTeam a remporté le plis
	 */
	public static long withAdditionalTrick(long pkScore, TeamId winningTeam, int trickPoints) {
		long newPkScore = pkScore;
		int newTrickPoints = trickPoints;
		if (winningTeam == TeamId.TEAM_1) {
			newPkScore += 1L;
			if (turnTricks(newPkScore, winningTeam) >= TRICK_MAX) {
				newPkScore += ((long) Jass.MATCH_ADDITIONAL_POINTS << TRICK_BITS_SIZE);
			}
			return newPkScore += ((long) newTrickPoints << TRICK_BITS_SIZE);

		} else {
			newPkScore += (1L << Integer.SIZE);
			if (turnTricks(newPkScore, winningTeam) >= TRICK_MAX) {
				newPkScore += ((long) Jass.MATCH_ADDITIONAL_POINTS << (NB_BITS_TEAM + TRICK_BITS_SIZE));
			}
			return newPkScore += ((long) newTrickPoints << (Integer.SIZE + TRICK_BITS_SIZE));

		}
	}

	/**
	 * 
	 * @param pkScore
	 *            : score empaqueté
	 * @return newPkScore : score empaqueté mis à jour pr le tour suivant
	 */
	public static long nextTurn(long pkScore) {
		return pack(0, 0, totalPoints(pkScore, TeamId.TEAM_1), 0, 0, totalPoints(pkScore, TeamId.TEAM_2));
	}

	/**
	 * Méthode qui renvoie l'affichage du score
	 * 
	 * @param pkScore
	 * @return l'affichage
	 */
	public static String toString(long pkScore) {
		String score1 = "Équipe 1 : "
				+ Bits64.extract(pkScore, TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE, GAME_POINTS_BITS_SIZE) + " + " + Bits64.extract(pkScore, TRICK_BITS_SIZE, TURN_POINTS_BITS_SIZE);
		String score2 = " / Équipe 2 : " + Bits64.extract(pkScore,
				NB_BITS_TEAM + TRICK_BITS_SIZE + TURN_POINTS_BITS_SIZE, GAME_POINTS_BITS_SIZE) + " + " + Bits64.extract(pkScore, NB_BITS_TEAM + TRICK_BITS_SIZE, TURN_POINTS_BITS_SIZE);
		return score1 + score2;
	}
}