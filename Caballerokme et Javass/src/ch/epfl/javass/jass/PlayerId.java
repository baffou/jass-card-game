package ch.epfl.javass.jass;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Shayan Classe énumérée des joueurs de la partie
 */
public enum PlayerId {
	PLAYER_1, PLAYER_2, PLAYER_3, PLAYER_4;

	public final static List<PlayerId> ALL = Collections.unmodifiableList(Arrays.asList(values()));
	public final static int COUNT = 4;

	public TeamId team() {
		return TeamId.values()[this.ordinal() % 2];
	}
}
