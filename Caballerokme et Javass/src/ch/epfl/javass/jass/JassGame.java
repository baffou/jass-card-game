package ch.epfl.javass.jass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import ch.epfl.javass.jass.Card.Color;
import ch.epfl.javass.jass.Card.Rank;

public final class JassGame {

    private final static int NUMBER_OF_COLOR = Color.COUNT;
    private final static int NUMBER_OF_RANK = Rank.COUNT;
    private final static int NUMBER_OF_PLAYER = PlayerId.COUNT;
    private boolean isGameOver = false;
    private boolean FirstTurn = true;

    private Map<PlayerId, Player> players;
    private Map<PlayerId, String> playersNames;
    private final Random shuffleRng;
    private final Random trumpRng;
    private TurnState tState;
    private Map<PlayerId, CardSet> deck;
    private PlayerId firstPlayer;
    private PlayerId winningPlayer;

    public JassGame(long rngSeed, Map<PlayerId, Player> players,
            Map<PlayerId, String> playerNames) {
        this.players = Collections.unmodifiableMap(players);
        this.playersNames = Collections.unmodifiableMap(playerNames);
        Random rng = new Random(rngSeed);
        shuffleRng = new Random(rng.nextLong());
        trumpRng = new Random(rng.nextLong());
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    /**
     * méthode permettant de faire avancer la partie de jass d'un trick, c'est
     * elle qui se charge de faire les updates pour les joueurs ainsi que de
     * tester le fait qu'une équipe a gagné
     */
    public void advanceToEndOfNextTrick() {

        /*
         * cette condition vérifie si c'est le tout premier tour de la game et
         * si oui elle s'occupe d'initialiser le premier tour et les updates des
         * mains et du plis pour chaque joueurs
         */
        if (FirstTurn == true) {
            deck = distriboute();
            tState = initFirstTurn();
            for (Entry<PlayerId, Player> entry : players.entrySet()) {
                Player player = entry.getValue();
                players.get(entry.getKey()).setPlayers(entry.getKey(),
                        playersNames);
                player.updateHand(deck.get(entry.getKey()));
                player.updateTrick(tState.trick());
            }
            FirstTurn = false;
        }

        // Si ce n'est pas le premier tour :
        else {
            // on collecte le plis

            tState = tState.withTrickCollected();

            // une fois que tout le monde a joué on update le score pour tous
            for (Entry<PlayerId, Player> entry : players.entrySet()) {
                Player player = entry.getValue();
                player.updateScore(tState.score());
                ;
            }

            checkWin();
            if (isGameOver)
                return;

            /*
             * si le plis est terminal on crée un nouveau tour en updatant les
             * mains et tricks pr chaque joueurs
             */

            if (tState.isTerminal()) {
                deck = distriboute();
                tState = initTurn();
            }
            for (Entry<PlayerId, Player> entry : players.entrySet()) {
                Player player = entry.getValue();
                player.updateTrick(tState.trick());
                player.updateScore(tState.score());
                player.updateHand(deck.get(entry.getKey()));
            }

            checkWin();
            if (isGameOver)
                return;
        }

        /*
         * boucle s'occupant de faire joueur les 4 joueurs (elle update la main
         * du joueur qui joue et le le plis pour tout les joueurs à chaque fois
         * qu une carte est jouée
         */
        for (int i = 0; i < NUMBER_OF_PLAYER; i++) {
            PlayerId playerId = PlayerId.values()[(winningPlayer.ordinal() + i)
                    % PlayerId.COUNT];
            Player player = players.get(playerId);
            CardSet hand = deck.get(playerId);
            Card cardPlayed = player.cardToPlay(tState, hand);
            tState = tState.withNewCardPlayed(cardPlayed);
            CardSet newHand = hand.remove(cardPlayed);
            deck.replace(playerId, hand, newHand);
            CardSet hand2 = deck.get(playerId);
            player.updateHand(hand2);
            for (Entry<PlayerId, Player> entry : players.entrySet()) {
                Player player2 = entry.getValue();
                player2.updateTrick(tState.trick());
            }
        }

        // on set le winningPlayer pr savoir qui commence le prochain plis
        winningPlayer = PackedTrick.winningPlayer(tState.packedTrick());

    }

    /*
     * on vérifie les scores pour voir si la game est finie si oui on set notre
     * condition isGameOver à true et on set la winningTeam pour tous, et sinon
     */
    private void checkWin() {
        if (PackedScore.totalPoints(tState.packedScore(),
                TeamId.TEAM_1) >= Jass.WINNING_POINTS
                || PackedScore.totalPoints(tState.packedScore(),
                        TeamId.TEAM_2) >= Jass.WINNING_POINTS) {
            TeamId winningTeam = TeamId.TEAM_2;
            if (PackedScore.totalPoints(tState.packedScore(),
                    TeamId.TEAM_1) >= Jass.WINNING_POINTS) {
                winningTeam = TeamId.TEAM_1;
            }
            isGameOver = true;
            for (Entry<PlayerId, Player> entry : players.entrySet()) {
                Player player = entry.getValue();
                player.setWinningTeam(winningTeam);
            }
        }
    }

    /**
     * méthode s'occupant de créer un deck et de le distribuer aléatoirement à
     * tout les joueurs
     * 
     * @return Map<PlayerId, CardSet> deck : une map qui fait correspondre une
     *         main à son joueur
     */
    private Map<PlayerId, CardSet> distriboute() {

        // création d'une liste contenant toutes les cartes de la partie
        List<Card> deck = new ArrayList<Card>();
        for (int i = 0; i < NUMBER_OF_COLOR; i++) {
            for (int j = 0; j < NUMBER_OF_RANK; j++) {
                deck.add(Card.of(Color.values()[i], Rank.values()[j]));
            }
        }
        // on mélange la liste
        Collections.shuffle(deck, shuffleRng);

        /*
         * On crée une map et une liste de cardSets représentant les mains des
         * joueurs on ajoute à notre liste nos 9 premières cartes et on attribue
         * ses cartes aux premier joueur et on repète l' opération 3 fois (pour
         * tout les joeueurs en gros)
         */
        List<CardSet> hands = new ArrayList<>();
        Map<PlayerId, CardSet> realDeck = new HashMap<>();
        for (int i = 0; i < NUMBER_OF_PLAYER; i++) {
            CardSet actualCardSet = CardSet.EMPTY;
            for (int j = 0; j < NUMBER_OF_RANK; j++) {
                actualCardSet = actualCardSet.add(deck.get((i * 9) + j));
            }
            hands.add(actualCardSet);
        }
        for (int i = 0; i < hands.size(); i++) {
            realDeck.put(PlayerId.values()[i], hands.get(i));
        }

        return realDeck;
    }

    /**
     * Méthode s'occupant d'initialiser le tout premier tour
     * 
     * @return TurnState tState: return le tout premier tour (où le premier
     *         joueur est celui qui a le 7 de carreau)
     */
    private TurnState initFirstTurn() {
        firstPlayer = chooseFirstPlayer(deck);
        // on set le FirstPlayer au winningPlayer car c'est lui qui commencer pr
        // le
        // premier plis d'un tour
        winningPlayer = firstPlayer;
        Color trump = Color.values()[trumpRng.nextInt(NUMBER_OF_PLAYER)];
        // on update le trump pr tous
        for (Entry<PlayerId, Player> entry : players.entrySet()) {
            Player player = entry.getValue();
            player.setTrump(trump);
        }
        return TurnState.initial(trump, Score.INITIAL, firstPlayer);
    }

    /**
     * Méthode s'occupant d'initialiser un tour lambda
     * 
     * @return TurnState tState: return un tour (où le premier joueur du tour
     *         est celui qui se trouve après le joueur ayant commencer le tour
     *         précédant)
     */
    private TurnState initTurn() {
        // on prend le joueur suivant
        firstPlayer = PlayerId.values()[(firstPlayer.ordinal() + 1)
                % PlayerId.COUNT];
        // on set le FirstPlayer au winningPlayer car c'est lui qui commencer pr
        // le
        // premier plis d'un tour
        winningPlayer = firstPlayer;
        Color trump = Color.values()[trumpRng.nextInt(NUMBER_OF_PLAYER)];
        // on update le trump pr tous
        for (Entry<PlayerId, Player> entry : players.entrySet()) {
            Player player = entry.getValue();
            player.setTrump(trump);
        }
        return TurnState.initial(trump, tState.score().nextTurn(), firstPlayer);
    }

    /**
     * Méthode s'occupant de trouver l'Id du joueur devant commencer la partie
     * 
     * @param Map<PlayerId,
     *            CardSet> deck (les mains de chaque joueur)
     * @return PlayerId FirstPlayer : renvoie l'Id du player qui doit commencer
     *         (à savoir celui qui a le 7 de carreau)
     */
    private PlayerId chooseFirstPlayer(Map<PlayerId, CardSet> deck) {
        for (Entry<PlayerId, CardSet> entry : deck.entrySet()) {
            if (entry.getValue().contains(Card.of(Color.DIAMOND, Rank.SEVEN))) {
                return entry.getKey();
            }
        }
        return null;
    }

}
