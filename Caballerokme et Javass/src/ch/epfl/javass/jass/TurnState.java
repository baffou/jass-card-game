package ch.epfl.javass.jass;

import ch.epfl.javass.Preconditions;
import ch.epfl.javass.jass.Card.Color;

public final class TurnState {
    private final long pkScore;
    private final long pkUnplayedCards;
    private final int pkTrick;

    /**
     * Constructeur de TurnState
     * 
     * @param pkScore
     * @param pkUnplayedCards
     * @param pkTrick
     */
    private TurnState(long pkScore, long pkUnplayedCards, int pkTrick) {
        this.pkScore = pkScore;
        this.pkUnplayedCards = pkUnplayedCards;
        this.pkTrick = pkTrick;
    }

    /**
     * Méthode qui retourne l'état initial correspondant à un tour de jeu dont
     * l'atout, le score initial et le joueur initial sont ceux donnés
     * 
     * @param trump
     * @param score
     * @param firstPlayer
     * @return
     */
    public static TurnState initial(Color trump, Score score,
            PlayerId firstPlayer) {
        return new TurnState(score.packed(), PackedCardSet.ALL_CARDS,
                PackedTrick.firstEmpty(trump, firstPlayer));
    }

    /**
     * Méthode qui retourne l'état dont les composantes (empaquetées) sont
     * celles données, ou lève IllegalArgumentException si l'une d'entre elles
     * est invalide selon la méthode isValid correspondante
     * 
     * @param pkScore
     * @param pkUnplayedCards
     * @param pkTrick
     * @return
     */
    public static TurnState ofPackedComponents(long pkScore,
            long pkUnplayedCards, int pkTrick) {
        Preconditions.checkArgument(!(!PackedScore.isValid(pkScore)
                || !PackedCardSet.isValid(pkUnplayedCards)
                || !PackedTrick.isValid(pkTrick)));
        return new TurnState(pkScore, pkUnplayedCards, pkTrick);
    }

    /**
     * Méthode qui retourne la version empaquetée du score courant
     * 
     * @return
     */
    public long packedScore() {
        return pkScore;
    }

    /**
     * Méthode qui retourne la version empaquetée de l'ensemble des cartes pas
     * encore jouées
     * 
     * @return
     */
    public long packedUnplayedCards() {
        return pkUnplayedCards;
    }

    /**
     * Méthode qui retourne la version empaquetée du pli courant
     * 
     * @return
     */
    public int packedTrick() {
        return pkTrick;
    }

    /**
     * Méthode qui retourne le score courant
     * 
     * @return
     */
    public Score score() {
        return Score.ofPacked(pkScore);
    }

    /**
     * Méthode qui qui retourne l'ensemble des cartes pas encore jouées
     * 
     * @return
     */
    public CardSet unplayedCards() {
        return CardSet.ofPacked(pkUnplayedCards);
    }

    /**
     * Méthode qui retourne le pli courant
     * 
     * @return
     */
    public Trick trick() {
        return Trick.ofPacked(pkTrick);
    }

    /**
     * Méthode qui retourne vrai ssi l'état est terminal, c-à-d si le dernier
     * pli du tour a été joué
     * 
     * @return
     */
    public boolean isTerminal() {
        return pkTrick == PackedTrick.INVALID;
    }

    /**
     * Méthode qui retourne l'identité du joueur devant jouer la prochaine
     * carte, ou lève l'exception IllegalStateException si le pli courant est
     * plein
     * 
     * @return
     */
    public PlayerId nextPlayer() {
        if (PackedTrick.isFull(pkTrick)) {
            throw new IllegalStateException();
        }
        return PackedTrick.player(pkTrick, PackedTrick.size(pkTrick));
    }

    /**
     * Méthode qui retourne l'état correspondant à celui auquel on l'applique
     * après que le prochain joueur ait joué la carte donnée, ou lève
     * IllegalStateException si le pli courant est plein
     * 
     * @param card
     * @return
     */
    public TurnState withNewCardPlayed(Card card) {
        if (PackedTrick.isFull(pkTrick)) {
            throw new IllegalStateException();
        }
        return ofPackedComponents(pkScore,
                pkUnplayedCards & ~PackedCardSet.singleton(card.packed()),
                PackedTrick.withAddedCard(pkTrick, card.packed()));
    }

    /**
     * Méthode qui retourne l'état correspondant à celui auquel on l'applique
     * après que le pli courant ait été ramassé, ou lève IllegalStateException
     * si le pli courant n'est pas terminé (c-à-d plein)
     * 
     * @return
     */
    public TurnState withTrickCollected() {
        if (!PackedTrick.isFull(pkTrick)) {
            throw new IllegalStateException();
        }
        return new TurnState(
                PackedScore.withAdditionalTrick(pkScore,
                        PackedTrick.winningPlayer(pkTrick).team(),
                        PackedTrick.points(pkTrick)),
                pkUnplayedCards, PackedTrick.nextEmpty(pkTrick));
    }

    /**
     * Méthode qui retourne l'état correspondant à celui auquel on l'applique
     * après que le prochain joueur ait joué la carte donnée, et que le pli
     * courant ait été ramassé s'il est alors plein ; lève IllegalStateException
     * si le pli courant est plein
     * 
     * @param card
     * @return
     */
    public TurnState withNewCardPlayedAndTrickCollected(Card card) {
        TurnState t = new TurnState(pkScore, pkUnplayedCards, pkTrick);
        if (PackedTrick.isFull(pkTrick)) {
            throw new IllegalStateException();
        }
        t = withNewCardPlayed(card);
        if (PackedTrick.isFull(t.pkTrick)) {
            t = t.withTrickCollected();
        }
        return t;
    }

}
