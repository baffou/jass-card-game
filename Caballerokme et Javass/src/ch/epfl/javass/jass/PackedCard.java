package ch.epfl.javass.jass;

import ch.epfl.javass.bits.Bits32;
import ch.epfl.javass.jass.Card.Rank;

/**
 * @author Shayan
 * 
 *         Version empaquetée dans un bits à 32 digits d'une carte
 *
 */
public final class PackedCard {
    public final static int RANK_START = 0;
    public final static int RANK_SIZE = 4;
    public final static int RANK_MAX_VALUE = 8;
    public final static int COLOR_START = 4;
    public final static int COLOR_SIZE = 2;
    public final static int PACKED_VALUE_WIHTOUT_RANK_AND_COLOR = 0;

    public static int INVALID = 0b111111;

    public final static int TEN_ORDINAL = 4;
    public final static int JACK_ORDINAL = 5;
    public final static int QUEEN_ORDINAL = 6;
    public final static int KING_ORDINAL = 7;
    public final static int ACE_ORDINAL = 8;

    public final static int TEN_POINTS = 10;
    public final static int JACK_POINTS = 2;
    public final static int QUEEN_POINTS = 3;
    public final static int KING_POINTS = 4;
    public final static int ACE_POINTS = 11;

    public final static int NINE_TRUMP_ORDINAL = 7;
    public final static int TEN_TRUMP_ORDINAL = 3;
    public final static int JACK_TRUMP_ORDINAL = 8;
    public final static int QUEEN_TRUMP_ORDINAL = 4;
    public final static int KING_TRUMP_ORDINAL = 5;
    public final static int ACE_TRUMP_ORDINAL = 6;

    public final static int NINE_TRUMP_POINTS = 14;
    public final static int TEN_TRUMP_POINTS = 10;
    public final static int JACK_TRUMP_POINTS = 20;
    public final static int QUEEN_TRUMP_POINTS = 3;
    public final static int KING_TRUMP_POINTS = 4;
    public final static int ACE_TRUMP_POINTS = 11;

    public final static int DEFAULT_POINTS = 0;

    private PackedCard() {
    }

    /**
     * Vérifie qu'une carte est valide
     * 
     * @param pkCard
     *            : bits qu'on vérifie
     * @return si oui ou non la carte est valide
     */
    public static boolean isValid(int pkCard) {
        int tested = Bits32.extract(pkCard, RANK_START, RANK_SIZE);
        int decale = pkCard >>> (RANK_SIZE + COLOR_SIZE);
        if (tested <= RANK_MAX_VALUE
                && decale == PACKED_VALUE_WIHTOUT_RANK_AND_COLOR) {
            return true;
        }
        return false;
    }

    /**
     * Empaquette les informations de couleur et de rang pour créer une carte en
     * digits (empaquettée)
     * 
     * @param c
     *            : la couleur de la carte
     * @param r
     *            : le rang de la carte
     * @return la carte empaquettée
     */
    public static int pack(Card.Color c, Card.Rank r) {
        int color = c.ordinal();
        int rank = r.ordinal();
        return Bits32.pack(rank, RANK_SIZE, color, COLOR_SIZE);
    }

    /**
     * Récupère la couleur d'une carte empaquetée donnée
     * 
     * @param pkCard
     *            : la carte
     * @return la couleur
     */
    public static Card.Color color(int pkCard) {
        int color = Bits32.extract(pkCard, COLOR_START, COLOR_SIZE);
        return Card.Color.values()[color];
    }

    /**
     * Récupère le rang d'une carte empaquetée donnée
     * 
     * @param pkCard
     *            : la carte
     * @return le rang
     */
    public static Card.Rank rank(int pkCard) {
        int rank = Bits32.extract(pkCard, RANK_START, RANK_SIZE);
        return Card.Rank.values()[rank];
    }

    /**
     * Vérifie si une première carte est plus forte ou non qu'une deuxième carte
     * 
     * @param trump
     *            : la couleur de l'atout
     * @param pkCardL
     *            : la première carte
     * @param pkCardR
     *            : la deuxième carte
     * @return true si plus forte, sinon false
     */
    public static boolean isBetter(Card.Color trump, int pkCardL, int pkCardR) {
        if (color(pkCardL) != color(pkCardR) && color(pkCardL) != trump
                && color(pkCardR) != trump) {
            return false;
        } else if (color(pkCardL) == trump) {
            if (color(pkCardR) == trump
                    && rank(pkCardL).trumpOrdinal() <= rank(pkCardR)
                            .trumpOrdinal()
                    && rank(pkCardL).trumpOrdinal() <= rank(pkCardR)
                            .trumpOrdinal()) {
                return false;
            }
            return true;
        } else {
            if (color(pkCardR) == trump) {
                return false;
            }
            Rank rL = rank(pkCardL);
            Rank rR = rank(pkCardR);
            if (rL.ordinal() <= rR.ordinal()) {
                return false;
            }
            return true;
        }
    }


    /**
     * Donne le nombre de points que vaut une carte
     * 
     * @param trump
     *            : la couleur de l'atout
     * @param pkCard
     *            : la carte donnée
     * @return le nombre de points
     */
    public static int points(Card.Color trump, int pkCard) {
        Rank r = rank(pkCard);
        if (color(pkCard) == trump) {
            switch (r.trumpOrdinal()) {
            case NINE_TRUMP_ORDINAL:
                return NINE_TRUMP_POINTS;
            case TEN_TRUMP_ORDINAL:
                return TEN_TRUMP_POINTS;
            case JACK_TRUMP_ORDINAL:
                return JACK_TRUMP_POINTS;
            case QUEEN_TRUMP_ORDINAL:
                return QUEEN_TRUMP_POINTS;
            case KING_TRUMP_ORDINAL:
                return KING_TRUMP_POINTS;
            case ACE_TRUMP_ORDINAL:
                return ACE_TRUMP_POINTS;
            default:
                return DEFAULT_POINTS;
            }
        }
        switch (r.ordinal()) {
        case TEN_ORDINAL:
            return TEN_POINTS;
        case JACK_ORDINAL:
            return JACK_POINTS;
        case QUEEN_ORDINAL:
            return QUEEN_POINTS;
        case KING_ORDINAL:
            return KING_POINTS;
        case ACE_ORDINAL:
            return ACE_POINTS;
        default:
            return DEFAULT_POINTS;
        }

    }

    /**
     * Affiche une carte donnée en texte
     * 
     * @param pkCard
     *            : la carte
     * @return le texte
     */
    public static String toString(int pkCard) {
        String rank = rank(pkCard).toString();
        String color = color(pkCard).toString();
        return color + " " + rank;
    }
}