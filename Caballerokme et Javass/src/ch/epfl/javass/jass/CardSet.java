package ch.epfl.javass.jass;

import java.util.List;

public final class CardSet {
	public final static CardSet EMPTY = new CardSet(PackedCardSet.EMPTY);
	public final static CardSet ALL_CARDS = new CardSet(PackedCardSet.ALL_CARDS);

	private final long packedCardSet;

	/**
	 * Constructeur qui crée un CardSet à partir d'un CardSet empaqueté
	 * 
	 * @param packed
	 */
	private CardSet(long packed) {
		packedCardSet = packed;
	}

	/**
	 * Méthode qui crée un CardSet à partir d'un CardSet empaqueté
	 * 
	 * @param packed
	 * @return le CardSet
	 */
	public static CardSet ofPacked(long packed) {
		if (!PackedCardSet.isValid(packed))
			throw new IllegalArgumentException();
		return new CardSet(packed);
	}

	/**
	 * Crée un CardSet à partir d'une liste de card
	 * 
	 * @param cards
	 * @return le CardSet
	 */
	public static CardSet of(List<Card> cards) {
		CardSet cardSet = EMPTY;
		for (Card c : cards) {
			cardSet = cardSet.add(c);
		}
		return cardSet;
	}

	/**
	 * @return la version empaquetée du CardSet
	 */
	public long packed() {
		return packedCardSet;
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return PackedCardSet.isEmpty(packedCardSet);
	}

	/**
	 * Comme dans PackedCard
	 * 
	 * @return
	 */
	public int size() {
		return PackedCardSet.size(packedCardSet);
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param index
	 * @return
	 */
	public Card get(int index) {
		return Card.ofPacked(PackedCardSet.get(packedCardSet, index));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param card
	 * @return
	 */
	public CardSet add(Card card) {
		return ofPacked(PackedCardSet.add(packedCardSet, card.packed()));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param card
	 * @return
	 */
	public CardSet remove(Card card) {
		return ofPacked(PackedCardSet.remove(packedCardSet, card.packed()));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param card
	 * @return
	 */
	public boolean contains(Card card) {
		return PackedCardSet.contains(packedCardSet, card.packed());
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @return
	 */
	public CardSet complement() {
		return ofPacked(PackedCardSet.complement(packedCardSet));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param that
	 * @return
	 */
	public CardSet union(CardSet that) {
		return ofPacked(PackedCardSet.union(packedCardSet, that.packed()));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param that
	 * @return
	 */
	public CardSet intersection(CardSet that) {
		return ofPacked(PackedCardSet.intersection(packedCardSet, that.packed()));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param that
	 * @return
	 */
	public CardSet difference(CardSet that) {
		return ofPacked(PackedCardSet.difference(packedCardSet, that.packed()));
	}

	/**
	 * Comme dans PackedCardSet
	 * 
	 * @param color
	 * @return
	 */
	public CardSet subsetOfColor(Card.Color color) {
		return ofPacked(PackedCardSet.subsetOfColor(packedCardSet, color));
	}

	/**
	 * Méthode qui compart la CardSet et l'objet o
	 * 
	 * @param o
	 * @return true si le même CardSet, false sinon
	 */
	public boolean equals(Object o) {
		if (o == null)
			return false;
		return packedCardSet == ((CardSet) o).packed();
	}

	/**
	 * @return la carte empaquettée
	 */
	public int hashCode() {
		return Long.hashCode(packedCardSet);
	}

	/**
	 * Comme dans la classe PackedCardSet
	 */
	public String toString() {
		return PackedCardSet.toString(packedCardSet);
	}
}
