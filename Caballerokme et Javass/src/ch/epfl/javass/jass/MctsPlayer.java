package ch.epfl.javass.jass;

import java.util.LinkedList;
import java.util.List;
import java.util.SplittableRandom;

public class MctsPlayer implements Player {

	private final static int NUMBER_MAX_NODE = 9;

	private PlayerId ownId;
	private SplittableRandom rngSeed;
	private int maxIterations;

	public MctsPlayer(PlayerId ownId, long rngSeed, int iterations) {
		if (iterations < NUMBER_MAX_NODE)
			throw new IllegalArgumentException();
		this.ownId = ownId;
		this.rngSeed = new SplittableRandom(rngSeed);
		maxIterations = iterations;
	}

	@Override
	public Card cardToPlay(TurnState state, CardSet hand) {
		if (hand.size() == 1)
			return hand.get(0);
		Node firstNode = new Node(state, state.trick().playableCards(hand), ownId.team());
		int i = 0;
		while (i < maxIterations) {
			firstNode.addNode(ownId, hand, rngSeed);
			i++;
		}
		return firstNode.BestCard();
	}

	private static class Node {

		private TurnState turnState;
		private List<Node> pathFromTop; // chemin du noeud originel jusqu'à ce node
		private Node[] enfantTab; // tableau regroupant tout les enfants d'un node
		private TeamId team;
		private int score;
		private int nbNoeud;
		private CardSet unplayed; // carte jouable de la main du joueur mais qui n'ont pas été jouées

		private final static int c = 40;

		private Node(TurnState turnState, CardSet unplayed, TeamId teamId) {
			this.turnState = turnState;
			pathFromTop = new LinkedList<>();
			enfantTab = new Node[unplayed.size()];
			this.unplayed = unplayed;
			this.team = teamId;
		}

		/**
		 * méthode récursive permetant d'ajouter un noeud à l'arbre au bon endroit (la
		 * méthode vérifie si parmis les enfants du nod originel lequel a le meilleur V,
		 * puis va regarder à l'intérieur pour voir si il lui manque des noeuds enfants,
		 * si oui alors elle lui ajoute un noeud sinon elle s'appelle (récursivement sur
		 * le node enfant qui a le meilleur V)
		 * 
		 * @param ownId
		 *            : Id du player MCTS
		 * @param hand
		 *            : main initial du joueur
		 * @param rng
		 *            : Rng Seed
		 */
		private void addNode(PlayerId ownId, CardSet hand, SplittableRandom rng) {
			int index = bestV();
			if (enfantTab[index] == null) {
				Card playedCard = unplayed.get(index);
				TurnState nextState = turnState.withNewCardPlayedAndTrickCollected(playedCard);
				Node newNode = new Node(nextState, playableCards(nextState, hand, ownId),
						turnState.nextPlayer().team());
				newNode.pathFromTop = new LinkedList<>(pathFromTop); // attribution du nouveau chemin (on ajoute la node
																		// après)
				newNode.pathFromTop.add(this);
				Score simulatedScore = simulateTurn(newNode.turnState, hand, ownId, rng);
				for (int i = 0; i < newNode.pathFromTop.size(); i++) {
					newNode.pathFromTop.get(i).nbNoeud++;
					newNode.pathFromTop.get(i).score += simulatedScore.turnPoints(newNode.pathFromTop.get(i).team);
				}
				enfantTab[index] = newNode;
			} else {
				if (!enfantTab[index].turnState.isTerminal())
					enfantTab[index].addNode(ownId, hand, rng);
				else {
					Score simulatedScore = simulateTurn(this.turnState, hand, ownId, rng);
					for (int i = 0; i < this.pathFromTop.size(); i++) {
						this.pathFromTop.get(i).nbNoeud++;
						this.pathFromTop.get(i).score += simulatedScore.turnPoints(this.pathFromTop.get(i).team);
					}
				}
			}
		}

		/**
		 * Méthode permettant de calculer la valeur de la fonction d' exploration du
		 * noeud
		 * 
		 * @return double V : valeur de la fonction d'exploration V
		 */
		private double V() {
			if (nbNoeud > 0) {
				return score / (double) nbNoeud
						+ c * Math.sqrt((2 * Math.log(pathFromTop.get(pathFromTop.size() - 1).nbNoeud)) / nbNoeud);
			}
			return Double.POSITIVE_INFINITY;
		}

		/**
		 * Méthode permettant de prendre l'index du meilleur enfant
		 * 
		 * @return int Index : l'index dans enfantTab du meilleur childNode
		 */
		private int bestV() {
			int bestIndex = 0;
			for (int i = 0; i < enfantTab.length; i++) {
				if (enfantTab[i] == null) {
					return i;
				}
				if (enfantTab[bestIndex].V() < enfantTab[i].V())
					bestIndex = i;
			}
			return bestIndex;
		}

		/**
		 * Méthode permettant de simuler un tour à partir d'un state donné, de la main
		 * initital du joueur, de l'Id du joueur actuel MCTS et de la seed rng, elle
		 * retourne le score final du tour simulé
		 * 
		 * @param tStateInitial
		 *            : état du tour initial a simuler CardSet hand : main initial du
		 *            joueur PlayerId ownId : Id du joueur actuelement simulé par MCTS
		 *            SplittableRandom rng : rng seed
		 * @return Score score : score final du tour simulé
		 */
		private Score simulateTurn(TurnState tStateInitital, CardSet hand, PlayerId ownId, SplittableRandom rng) {
			TurnState tState = tStateInitital;
			while (!tState.isTerminal()) {
				CardSet playableCards = playableCards(tState, hand, ownId);
				if (playableCards.size() == 0)
					System.out.println(tState.trick() + " " + tState.score() + " " + tState.unplayedCards());
				tState = tState
						.withNewCardPlayedAndTrickCollected(playableCards.get(rng.nextInt(playableCards.size())));
			}
			return tState.score();
		}

		/**
		 * Méthode permettant de dire quelle est le nb d'enfant que contient enfantTab
		 * 
		 * @return size : le nombre de noeud que contient enfantTab
		 */
		private int realSize() {
			int size = 0;
			for (int i = 0; i < enfantTab.length; i++) {
				if (!(enfantTab[i] == null))
					size++;
			}
			return size;
		}

		/**
		 * Méthode permettant de dire quelle est la meilleure carte en fonction du score
		 * et du nombre de noeud créés.
		 * 
		 * @return Card carte : la meilleur carte
		 */
		private Card BestCard() {
			double bestNodeScore = enfantTab[0].score / (double) enfantTab[0].nbNoeud;
			int bestIndex = 0;
			for (int i = 1; i < realSize(); i++) {
				double realScore = enfantTab[i].score / (double) enfantTab[i].nbNoeud;
				if (bestNodeScore < realScore) {
					bestNodeScore = realScore;
					bestIndex = i;
				}
			}
			return unplayed.get(bestIndex);
		}

		/**
		 * Méthode permettant de dire quels cartes est jouable en fonction de l'état
		 * actuel du tour, de la main initial du joueur et de l'Id de notre joueur MCTS
		 * 
		 * @param tstate
		 *            : état actuel hand : main initial ownId : Id de notre joueur MCTS
		 */
		private static CardSet playableCards(TurnState tState, CardSet hand, PlayerId ownId) {
			if (tState.isTerminal()) {
				return CardSet.EMPTY;
			}
			if (tState.nextPlayer() == ownId) {
				return tState.trick().playableCards(tState.unplayedCards().intersection(hand));
			}
			return tState.trick().playableCards(tState.unplayedCards().difference(hand));
		}

	}

}
