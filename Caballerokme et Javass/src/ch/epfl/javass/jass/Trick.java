package ch.epfl.javass.jass;

import ch.epfl.javass.Preconditions;
import ch.epfl.javass.jass.Card.Color;

public final class Trick {
	public final static Trick INVALID = new Trick(PackedTrick.INVALID);

	private final int packedTrick;

	/**
	 * Constructeur de Trick à partir d'un Trick packed
	 * 
	 * @param packed
	 */
	private Trick(int packed) {
		packedTrick = packed;
	}

	/**
	 * Méthode qui retourne le pli dont la version empaquetée est celle donnée, ou
	 * lève IllegalArgumentException si celui-ci n'est pas valide (selon
	 * PackedTrick.isValid)
	 * 
	 * @param packed
	 * @return
	 */
	public static Trick ofPacked(int packed) {
		Preconditions.checkArgument(PackedTrick.isValid(packed));
		Trick trick = new Trick(packed);
		return trick;
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @param trump
	 * @param firstPlayer
	 * @return
	 */
	public static Trick firstEmpty(Color trump, PlayerId firstPlayer) {
		return ofPacked(PackedTrick.firstEmpty(trump, firstPlayer));
	}

	/**
	 * @return le Trick packed
	 */
	public int packed() {
		return packedTrick;
	}

	/**
	 * Méthode qui se comporte comme la méthode correspondante de PackedTrick ou
	 * lève l'exception IllegalStateException si le pli n'est pas plein
	 * 
	 * @return
	 */
	public Trick nextEmpty() {
		if (!isFull()) {
			throw new IllegalStateException();
		}
		return new Trick(PackedTrick.nextEmpty(packedTrick));
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return PackedTrick.isEmpty(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public boolean isFull() {
		return PackedTrick.isFull(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public boolean isLast() {
		return PackedTrick.isLast(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public int size() {
		return PackedTrick.size(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public Color trump() {
		return PackedTrick.trump(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public int index() {
		return PackedTrick.index(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @param index
	 * @return
	 */
	public PlayerId player(int index) {
		if (index < 0 || index >= 4) {
			throw new IndexOutOfBoundsException();
		}
		return PackedTrick.player(packedTrick, index);
	}

	/**
	 * Méthode qui se comporte comme la méthode correspondante de PackedTrick ou
	 * lève IndexOutOfBoundsException si l'index n'est pas compris entre 0 (inclus)
	 * et la taille du pli (exclus)
	 * 
	 * @param index
	 * @return
	 */
	public Card card(int index) {
		if (index < 0 || index >= PackedTrick.size(packedTrick)) {
			throw new IndexOutOfBoundsException();
		}
		return Card.ofPacked(PackedTrick.card(packedTrick, index));
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @param c
	 * @return
	 */
	public Trick withAddedCard(Card c) {
		if (isFull()) {
			throw new IllegalStateException();
		}
		return ofPacked(PackedTrick.withAddedCard(packedTrick, c.packed()));
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public Color baseColor() {
		if (isEmpty()) {
			throw new IllegalStateException();
		}
		return PackedTrick.baseColor(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @param hand
	 * @return
	 */
	public CardSet playableCards(CardSet hand) {
		if (isFull()) {
			throw new IllegalStateException();
		}
		return CardSet.ofPacked(PackedTrick.playableCards(packedTrick, hand.packed()));
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public int points() {
		return PackedTrick.points(packedTrick);
	}

	/**
	 * Comme dans PackedTrick
	 * 
	 * @return
	 */
	public PlayerId winningPlayer() {
		if (isEmpty()) {
			throw new IllegalStateException();
		}
		return PackedTrick.winningPlayer(packedTrick);
	}

	/**
	 * Méthode qui compart la Trick et l'objet o
	 * 
	 * @param o
	 * @return true si le même Trick, false sinon
	 */
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else
			return packedTrick == ((Trick) o).packed();
	}

	public int hashCode() {
		return Integer.hashCode(packedTrick);
	}

	/**
	 * Comme dans PackedCardTrick
	 */
	public String toString() {
		return PackedTrick.toString(packedTrick);
	}
}