package ch.epfl.javass.net;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import ch.epfl.javass.jass.TurnState;

/**
 * 
 * @author Shayan Classe qui contient des méthodes servant à sérialiser et
 *         désérialiser des informations afin de simplifier la communication
 */
public final class StringSerializer {
    private static final int BASE_16 = 16;
    public final static int SCORE_POSITION = 0;
    public final static int UNPLAYED_CARDS_POSITION = 1;
    public final static int TRICK_POSITION = 2;

    /**
     * Serialise un integer i
     * 
     * @param i
     * @return
     */
    public static String serializeInt(int i) {
        return Integer.toUnsignedString(i, BASE_16);
    }

    /**
     * Déserialise un string s en un integer
     * 
     * @param s
     * @return
     */
    public static int deserializeInt(String s) {
        return Integer.parseUnsignedInt(s, BASE_16);
    }

    /**
     * Serialize un long l
     * 
     * @param l
     * @return
     */
    public static String serializeLong(long l) {
        return Long.toUnsignedString(l, BASE_16);
    }

    /**
     * Deserialise un string s en un long
     * 
     * @param s
     * @return
     */
    public static long deserializeLong(String s) {
        return Long.parseUnsignedLong(s, BASE_16);
    }

    /**
     * Serialise un string s
     * 
     * @param s
     * @return
     */
    public static String serializeString(String s) {
        byte[] b = s.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(b);
    }

    /**
     * Deserialise un string s
     * 
     * @param s
     * @return
     */
    public static String deserializeString(String s) {
        return new String(Base64.getDecoder().decode(s),
                StandardCharsets.UTF_8);
    }

    /**
     * Serialise un turnstate
     * 
     * @param state
     * @return
     */
    public static String[] serializeTurnState(TurnState state) {
        String[] s = new String[3];
        s[SCORE_POSITION] = serializeLong(state.packedScore());
        s[TRICK_POSITION] = serializeInt(state.packedTrick());
        s[UNPLAYED_CARDS_POSITION] = serializeLong(state.packedUnplayedCards());
        return s;
    }

    /**
     * Return un string composé par les éléments de a, séparé par un
     * CharSequence c
     * 
     * @param c
     * @param a
     * @return
     */
    public static String combine(CharSequence c, String[] a) {
        String s = String.join(c, a);
        return s;
    }

    /**
     * Return un tableau contenant chaque élément séparé par c
     * 
     * @param c
     * @param s
     * @return
     */
    public static String[] split(String c, String s) {
        String[] string = s.split(c);
        return string;
    }
}