package ch.epfl.javass.net;

/**
 * Type énuméré des 7 types de messages échangés par le client et le serveur
 * 
 * @author Shayan
 *
 */
public enum JassCommand {
    PLRS("setPlayers"), TRMP("setTrump"), HAND("updateHand"), TRCK(
            "updateTrick"), CARD(
                    "cardToPlay"), SCOR("updateScore"), WINR("setWinningTeam");

    String methodName;

    /**
     * Constructeur
     * 
     * @param methodName : Nom de la méthode
     */
    private JassCommand(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Return le type de message en string
     * 
     * @param j : Commande 
     * @return Name : Le nom (en string)
     */
    public String name(JassCommand j) {
        return j.methodName;
    }

}
