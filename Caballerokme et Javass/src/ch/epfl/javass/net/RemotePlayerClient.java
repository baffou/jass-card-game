//package ch.epfl.javass.net;
//
//import java.io.BufferedReader;
//import static java.nio.charset.StandardCharsets.US_ASCII;
//
//import java.io.BufferedWriter;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.OutputStreamWriter;
//import java.io.UncheckedIOException;
//import java.net.Socket;
//import java.util.Collection;
//import java.util.Map;
//
//import ch.epfl.javass.jass.Card;
//import ch.epfl.javass.jass.CardSet;
//import ch.epfl.javass.jass.Player;
//import ch.epfl.javass.jass.PlayerId;
//import ch.epfl.javass.jass.Score;
//import ch.epfl.javass.jass.TeamId;
//import ch.epfl.javass.jass.Trick;
//import ch.epfl.javass.jass.TurnState;
//import ch.epfl.javass.jass.Card.Color;
//
//public final class RemotePlayerClient implements Player, AutoCloseable {
//    private Socket s;
//    private BufferedReader r;
//    private BufferedWriter w;
//
//    public RemotePlayerClient(String hostName) throws IOException {
//        s = new Socket(hostName, 5108);
//        r = new BufferedReader(
//                new InputStreamReader(s.getInputStream(), US_ASCII));
//        w = new BufferedWriter(
//                new OutputStreamWriter(s.getOutputStream(), US_ASCII));
//    }
//    
//    
//    @Override
//    public Card cardToPlay(TurnState state, CardSet hand) {
//        String[] string = StringSerializer.serializeTurnState(state);
//        String sHand = StringSerializer.serializeLong(hand.packed());
//        write(JassCommand.CARD.name() + " " + StringSerializer.combine(",", string) + " "
//                + sHand);
//        Card card;
//        try {
//            card = Card.ofPacked(StringSerializer.deserializeInt((r.readLine())));
//            return card;
//        } catch (IOException e) {
//            throw new UncheckedIOException(e);
//        }
//    }
//
//    public void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
////        Collection<String> values = playerNames.values();
//        String[] names = new String[4];
//        for(int i = 0; i < PlayerId.COUNT; i++) {
//            names[i] = StringSerializer.serializeString(playerNames.get(PlayerId.values()[i]));
//        }
//        String namesJoined = StringSerializer.combine(",", names);
//        write(JassCommand.PLRS.name() + " " + Integer.toString(ownId.ordinal()) + " "
//                + namesJoined);
//    }
//
//    public void updateHand(CardSet newHand) {
//        write(JassCommand.HAND.name() + " " + StringSerializer.serializeLong(newHand.packed()));
//    }
//
//    public void setTrump(Color trump) {
//        write(JassCommand.TRMP.name() + " " + Integer.toString(trump.ordinal()));
//    }
//
//    public void updateTrick(Trick newTrick) {
//        write(JassCommand.TRCK.name() + " " + StringSerializer.serializeInt(newTrick.packed()));
//    }
//
//    public void updateScore(Score score) {
//        write(JassCommand.SCOR.name() + " " + StringSerializer.serializeLong(score.packed()));
//    }
//
//    public void setWinningTeam(TeamId winningTeam) {
//        write(JassCommand.WINR.name() + " " + Integer.toString(winningTeam.ordinal()));
//    }
//
//    private void write(String s) {
//        try {
//            w.write(s);
//            w.write('\n');
//            w.flush();
//        } catch (IOException e) {
//            throw new UncheckedIOException(e);
//        }
//    }
//
//    @Override
//    public void close() throws Exception {
//        r.close();
//        w.close();
//        s.close();
//    }
//    
//}

package ch.epfl.javass.net;

import static java.nio.charset.StandardCharsets.US_ASCII;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.net.Socket;
import java.util.Map;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.Player;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.Score;
import ch.epfl.javass.jass.TeamId;
import ch.epfl.javass.jass.Trick;
import ch.epfl.javass.jass.TurnState;
import ch.epfl.javass.jass.Card.Color;

/**
 * Représente le client d'un joueur
 * 
 * @author Shayan
 *
 */
public final class RemotePlayerClient implements Player, AutoCloseable {
    private final Socket s;
    private final BufferedReader r;
    private final BufferedWriter w;

    /**
     * Constructeur de la classe
     * 
     * @param hostName
     * @throws IOException
     */
    public RemotePlayerClient(String hostName) throws IOException {
        s = new Socket(hostName, 5108);
        r = new BufferedReader(
                new InputStreamReader(s.getInputStream(), US_ASCII));
        w = new BufferedWriter(
                new OutputStreamWriter(s.getOutputStream(), US_ASCII));
    }

    /*
     * Les méthodes qui suivent sont des implémentations de l'interface Player,
     * elles ne font qu'envoyer les messages correspondants et dans le cas de
     * cardToPlay, attend une réponse
     */

    /**
     * Méthode qui va envoyer son turnstate et sa hand et qui va recevoir sa carte (à jouer) qu'elle renvoie
     * @param state : Turnstate actuel (on en a besoin pour cardToPlay)
     * @param hand : Main du joueur
     * @return Card : La carte à jouer
     */
    @Override
    public Card cardToPlay(TurnState state, CardSet hand) {
        String[] string = StringSerializer.serializeTurnState(state);
        String sHand = StringSerializer.serializeLong(hand.packed());
        write(JassCommand.CARD.name() + " "
                + StringSerializer.combine(",", string) + " " + sHand);
        Card card;
        try {
            card = Card
                    .ofPacked(StringSerializer.deserializeInt((r.readLine())));
            return card;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Envoie l'id du player controlé par le client ainsi que la liste de tout les noms des players
     * @param ownId : Id du player contrôlé par le client
     * @param playerNames : Table associative associant à chaque joueur un nom
     */
    public void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
        String[] names = new String[4];
        for (int i = 0; i < PlayerId.COUNT; i++) {
            names[i] = StringSerializer
                    .serializeString(playerNames.get(PlayerId.values()[i]));
        }
        String namesJoined = StringSerializer.combine(",", names);
        write(JassCommand.PLRS.name() + " " + Integer.toString(ownId.ordinal())
                + " " + namesJoined);
    }

    /**
     * Envoie la main updatée
     * @param newHand : Nouvelle main
     */
    public void updateHand(CardSet newHand) {
        write(JassCommand.HAND.name() + " "
                + StringSerializer.serializeLong(newHand.packed()));
    }

    
    /**
     * Envoie l'atout
     * @param trump : Couleur de l'atout
     */
    public void setTrump(Color trump) {
        write(JassCommand.TRMP.name() + " "
                + Integer.toString(trump.ordinal()));
    }

    /**
     * Envoie le trick updaté
     * @param newTrick : nouveau plis
     */
    public void updateTrick(Trick newTrick) {
        write(JassCommand.TRCK.name() + " "
                + StringSerializer.serializeInt(newTrick.packed()));
    }

    /**
     * Envoie le score updaté 
     * @param score : Nouveau score
     */
    public void updateScore(Score score) {
        write(JassCommand.SCOR.name() + " "
                + StringSerializer.serializeLong(score.packed()));
    }

    /**
     * Envoie la winning team
     * @param winningTeam : Id de la team qui a gagné
     */
    public void setWinningTeam(TeamId winningTeam) {
        write(JassCommand.WINR.name() + " "
                + Integer.toString(winningTeam.ordinal()));
    }

    /**
     * Méthode simplifiant l'écriture des commandes write
     * @param s
     */
    private void write(String s) {
        try {
            w.write(s);
            w.write('\n');
            w.flush();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * s'occupe de fermer tout les flots/sockets
     * @throws Exception
     */
    @Override
    public void close() throws Exception {
        r.close();
        w.close();
        s.close();
    }

}