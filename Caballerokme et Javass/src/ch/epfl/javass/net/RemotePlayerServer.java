package ch.epfl.javass.net;

import static java.nio.charset.StandardCharsets.US_ASCII;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.Card.Color;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.Player;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.Score;
import ch.epfl.javass.jass.TeamId;
import ch.epfl.javass.jass.Trick;
import ch.epfl.javass.jass.TurnState;

/**
 * Classe qui représente le serveur d'un joueur
 * 
 * @author Shayan
 *
 */
public final class RemotePlayerServer {
    private final static int PORT = 5108;
    
    private final Player player;

    /**
     * Constructeur de la classe
     * 
     * @param player
     * @throws IOException
     */
    public RemotePlayerServer(Player player) throws IOException {
        this.player = player;
    }

    /**
     * Méthode qui tourne tout le temps. Elle attend les messages du client qu'elle va ensuite traduire en commande pour le jeu (comme update la hand ou le score),
     * afin de tenir informé le joueur distant de l'avancée du jeu
     */
    public void run() {
        try (ServerSocket s0 = new ServerSocket(PORT);
                Socket s = s0.accept();
                BufferedReader r = new BufferedReader(
                        new InputStreamReader(s.getInputStream(), US_ASCII));
                BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
                        s.getOutputStream(), US_ASCII))) {
            while (true) {
                // récupére le message
                String string = r.readLine();
                String[] command = StringSerializer.split(" ", string);

                JassCommand commandName = JassCommand.valueOf(command[0]);
                String commandInfo = command[1];
                // appelle la méthode correspondante du joueur local
                switch (commandName) {

                case CARD :
                    String commandInfo2 = command[2];

                    String[] turnState = StringSerializer.split(",", commandInfo);

                    Card c = player.cardToPlay(
                            TurnState.ofPackedComponents(
                                    StringSerializer.deserializeLong(turnState[StringSerializer.SCORE_POSITION]),
                                    StringSerializer.deserializeLong(turnState[StringSerializer.UNPLAYED_CARDS_POSITION]),
                                    StringSerializer.deserializeInt(turnState[StringSerializer.TRICK_POSITION])),
                            CardSet.ofPacked(
                                    StringSerializer.deserializeLong(commandInfo2)));
                    // renvoie la carte jouée
                    w.write(StringSerializer.serializeInt(c.packed()));
                    w.write('\n');
                    w.flush();
                    break;
                case PLRS:
                    int playerOrdinal = Integer
                            .parseInt(commandInfo);
                    Map<PlayerId, String> map = new HashMap<>();
                    String[] names = StringSerializer.split(",", string);
                    names[0] = StringSerializer.split(" ", names[0])[2];
                    for (int i = 0; i < PlayerId.COUNT; i++)
                        map.put(PlayerId.values()[i],
                                StringSerializer.deserializeString(names[i]));
                    player.setPlayers(PlayerId.values()[playerOrdinal], map);
                    break;
                case TRMP:
                    int colorOrdinal = Integer.parseInt(commandInfo);
                    player.setTrump(Color.values()[colorOrdinal]);
                    break;
                case HAND:
                    long pkHand = StringSerializer
                            .deserializeLong(commandInfo);
                    player.updateHand(CardSet.ofPacked(pkHand));
                    break;
                case TRCK:
                    int pkTrick = StringSerializer
                            .deserializeInt(commandInfo);
                    player.updateTrick(Trick.ofPacked(pkTrick));
                    break;
                case SCOR:
                    long pkScore = StringSerializer
                            .deserializeLong(commandInfo);
                    player.updateScore(Score.ofPacked(pkScore));
                    break;
                case WINR:
                    int teamOrdinal = Integer.parseInt(commandInfo);
                    player.setWinningTeam(TeamId.values()[teamOrdinal]);
                    break;
                default:
                    break;
                }
            }

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
