package ch.epfl.javass.gui;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.PackedTrick;
import ch.epfl.javass.jass.Player;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.Score;
import ch.epfl.javass.jass.TeamId;
import ch.epfl.javass.jass.Trick;
import ch.epfl.javass.jass.TurnState;
import javafx.application.Platform;
import ch.epfl.javass.jass.Card.Color;

/**
 * Cette classe est un adaptateur permettant d'adapter l'interface graphique
 * pour en faire un joueur
 * 
 * @author Shayan
 *
 */
public class GraphicalPlayerAdapter implements Player {
    /*
     * Beans utilisés dans les méthodes suivantes
     */
    private final ScoreBean scoreBean;
    private final TrickBean trickBean;
    private final HandBean handBean;
    private GraphicalPlayer graphicalPlayer;
    private final ArrayBlockingQueue<Card> queue;

    /**
     * Constructeur de la classe
     */
    public GraphicalPlayerAdapter() {
        this.scoreBean = new ScoreBean();
        this.trickBean = new TrickBean();
        this.handBean = new HandBean();
        queue = new ArrayBlockingQueue<Card>(1);
    }

    /*
     * Les méthodes suivantes sont des redéfinitions des méthodes de l'interface
     * Player. Elles servent à communiquer avec l'interface graphique
     */

    /**
     * Méthode qui assure la communication de la carte à jouer (nottament via la
     * queue bloquante)
     * 
     * @param state
     *            : Turnstate actuel
     * @param hand
     *            : Main actuel du joueur
     * @return Card : La carte à jouer
     */
    @Override
    public Card cardToPlay(TurnState state, CardSet hand) {
        try {
            Platform.runLater(() -> {
                handBean.setPlayableCards(state.trick().playableCards(hand));
            });
            Card card = queue.take();
            Platform.runLater(() -> {
                handBean.setPlayableCards(CardSet.EMPTY);
            });
            return card;
        } catch (InterruptedException e) {
            throw new Error(e);
        }
    }

    @Override
    public void setPlayers(PlayerId ownId, Map<PlayerId, String> playerNames) {
        graphicalPlayer = new GraphicalPlayer(ownId, playerNames, scoreBean,
                trickBean, handBean, queue);
        Platform.runLater(() -> {
            graphicalPlayer.createStage().show();
        });
    }

    /**
     * Méthode pour assurer la communication des updates de la main
     * 
     * @param newHand
     *            : Nouvelle main (updatée)
     */
    @Override
    public void updateHand(CardSet newHand) {
        Platform.runLater(() -> {
            handBean.setHand(newHand);
        });
    }

    /**
     * Méthode pour assurer la communication de l'atout
     * 
     * @param trump
     *            : Couleur de l'atout
     */
    @Override
    public void setTrump(Color trump) {
        Platform.runLater(() -> {
            trickBean.setTrump(trump);
        });
    }

    /**
     * Méthode pour assurer la communication des updates du trick
     * 
     * @param newTrick
     *            : Nouveau Trick (updaté)
     */
    @Override
    public void updateTrick(Trick newTrick) {
        Platform.runLater(() -> {
            trickBean.setTrick(newTrick);
        });
    }

    /**
     * Méthode pour assurer la communication des updates du score
     * 
     * @param score
     *            : Nouveau score (updaté)
     */
    @Override
    public void updateScore(Score score) {
        Platform.runLater(() -> {
            scoreBean.setGamePoints(TeamId.TEAM_1,
                    score.gamePoints(TeamId.TEAM_1));
            scoreBean.setGamePoints(TeamId.TEAM_2,
                    score.gamePoints(TeamId.TEAM_2));
            scoreBean.setTurnPoints(TeamId.TEAM_1,
                    score.turnPoints(TeamId.TEAM_1));
            scoreBean.setTurnPoints(TeamId.TEAM_2,
                    score.turnPoints(TeamId.TEAM_2));
            scoreBean.setTotalPoints(TeamId.TEAM_1,
                    score.totalPoints(TeamId.TEAM_1));
            scoreBean.setTotalPoints(TeamId.TEAM_2,
                    score.totalPoints(TeamId.TEAM_2));
        });
    }

    /**
     * Méthode pour assurer la communication de la team qui a gagné
     * 
     * @param winningTeam
     *            : Id de la team qui a gagné
     */
    @Override
    public void setWinningTeam(TeamId winningTeam) {
        Platform.runLater(() -> {
            scoreBean.setWinningTeam(winningTeam);
        });
    }

}