package ch.epfl.javass.gui;

import ch.epfl.javass.jass.TeamId;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * La classe est un bean JavaFX contenant les scores et plusieurs autres
 * propriétés
 * 
 * @author Shayan
 *
 */
public final class ScoreBean {

    // Les différentes propriétés(le chiffre indique le numéro de l'équipe)
    private final SimpleIntegerProperty turnPoints1;
    private final SimpleIntegerProperty gamePoints1;
    private final SimpleIntegerProperty totalPoints1;
    private final SimpleIntegerProperty turnPoints2;
    private final SimpleIntegerProperty gamePoints2;
    private final SimpleIntegerProperty totalPoints2;
    private final SimpleObjectProperty<TeamId> winningTeam;

    /**
     * Constructeur de la classe
     */
    public ScoreBean() {
        turnPoints1 = new SimpleIntegerProperty();
        gamePoints1 = new SimpleIntegerProperty();
        totalPoints1 = new SimpleIntegerProperty();
        turnPoints2 = new SimpleIntegerProperty();
        gamePoints2 = new SimpleIntegerProperty();
        totalPoints2 = new SimpleIntegerProperty();
        winningTeam = new SimpleObjectProperty<TeamId>();
    }
/**
 * Méthode qui return la propriété de turnPoints de l'équipe voulue
 * @param team
 * @return
 */
    public ReadOnlyIntegerProperty turnPointsProperty(TeamId team) {
        if (team.ordinal() == 0)
            return turnPoints1;
        return turnPoints2;
    }
/**
 * Méthode qui permet de modifier la valeur de turnPoints de l'équipe voulue
 * @param team
 * @param newTurnPoints
 */
    public void setTurnPoints(TeamId team, int newTurnPoints) {
        if (team.ordinal() == 0)
            turnPoints1.set(newTurnPoints);
        else
            turnPoints2.set(newTurnPoints);
        ;
    }
    /**
     * Méthode qui return la propriété de gamePoints de l'équipe voulue
     * @param team
     * @return
     */
    public ReadOnlyIntegerProperty gamePointsProperty(TeamId team) {
        if (team.ordinal() == 0)
            return gamePoints1;
        return gamePoints2;
    }
    /**
     * Méthode qui permet de modifier la valeur de gamePoints de l'équipe voulue
     * @param team
     * @param newTurnPoints
     */
    public void setGamePoints(TeamId team, int newGamePoints) {
        if (team.ordinal() == 0)
            gamePoints1.set(newGamePoints);
        else
            gamePoints2.set(newGamePoints);
    }
    /**
     * Méthode qui return la propriété de totalPoints de l'équipe voulue
     * @param team
     * @return
     */
    public ReadOnlyIntegerProperty totalPointsProperty(TeamId team) {
        if (team.ordinal() == 0)
            return totalPoints1;
        return totalPoints2;
    }
    /**
     * Méthode qui permet de modifier la valeur de totalPoints de l'équipe voulue
     * @param team
     * @param newTurnPoints
     */
    public void setTotalPoints(TeamId team, int newTotalPoints) {
        if (team.ordinal() == 0)
            totalPoints1.set(newTotalPoints);
        else
            totalPoints2.set(newTotalPoints);
    }
    /**
     * Méthode qui return la propriété de winningTeam
     * @param team
     * @return
     */
    ReadOnlyObjectProperty<TeamId> winningTeamProperty() {
        return winningTeam;
    }
    /**
     * Méthode qui permet de modifier la valeur de winningTeam
     * @param team
     * @param newTurnPoints
     */
    public void setWinningTeam(TeamId winningTeam) {
        this.winningTeam.set(winningTeam);
    }

}
