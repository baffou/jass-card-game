package ch.epfl.javass.gui;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.Trick;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

/**
 * La classe est un bean JavaFX contenant le trick et plusieurs autres
 * propriétés
 * 
 * @author Shayan
 *
 */
public final class TrickBean {
    
    // Les propriétés
    private final SimpleObjectProperty<Card.Color> trump;
    private final ObservableMap<PlayerId, Card> trick;
    private final SimpleObjectProperty<PlayerId> winningPlayer;

    /**
     * Le constructeur de la classe
     */
    public TrickBean() {
        trump = new SimpleObjectProperty<Card.Color>();
        trick = FXCollections.observableHashMap();
        for (int i = 0; i < PlayerId.COUNT; i++) {
            trick.put(PlayerId.ALL.get(i), null);
        }
        winningPlayer = new SimpleObjectProperty<PlayerId>();
    }

    /**
     * Méthode qui return la propriété de trump
     * 
     * @return
     */
    public ObjectProperty<Card.Color> trumpProperty() {
        return trump;
    }

    /**
     * Méthode qui permet de modifier la valeur de trump
     * 
     * @param newTrump
     */
    public void setTrump(Card.Color newTrump) {
        trump.set(newTrump);
    }

    /**
     * Méthode qui permet d'obtenir la propriété trick
     * 
     * @return
     */
    public ObservableMap<PlayerId, Card> trick() {
        return FXCollections.unmodifiableObservableMap(trick);
    }

    /**
     * Méthode qui permet de modifier la valeur de trick
     * 
     * @param newTrick
     */
    public void setTrick(Trick newTrick) {
        if (newTrick.isEmpty()) {
            for (int i = 0; i < PlayerId.COUNT; i++) {
                trick.put(newTrick.player(i), null);
            }
            winningPlayer.set(null);
        } else {
            for (int i = 0; i < newTrick.size(); i++) {
                trick.put(newTrick.player(i), newTrick.card(i));
            }
            for (int i = newTrick.size(); i < PlayerId.COUNT; i++) {
                trick.put(newTrick.player(i), null);
            }
            winningPlayer.set(newTrick.winningPlayer());
        }
    }

    /**
     * Méthode qui permet d'obtenir la propriété de winningPlayer
     * 
     * @return
     */
    public ReadOnlyObjectProperty<PlayerId> winningPlayerProperty() {
        return winningPlayer;
    }

}