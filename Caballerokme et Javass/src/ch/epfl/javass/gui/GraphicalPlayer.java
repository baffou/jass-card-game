package ch.epfl.javass.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.Card.Color;
import ch.epfl.javass.jass.Card.Rank;
import ch.epfl.javass.jass.PlayerId;
import ch.epfl.javass.jass.TeamId;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * La classe sert à représenter l'interface graphqique d'un joueur
 * 
 * @author Jérémy
 *
 */
public class GraphicalPlayer {

    private final static int NUMBER_OF_ELEMENTS_IN_SCOREPANE = 10;
    private final static int TRUMP_IMAGE_SIZE = 101;
    private final static int CARD_IMAGE_TRICK_HEIGTH = 180;
    private final static int CARD_IMAGE_TRICK_WIDTH = 120;
    private final static int CARD_IMAGE_HAND_HEIGTH = 120;
    private final static int CARD_IMAGE_HAND_WIDTH = 80;
    private final static int OPACITY_PLAYABLE = 1;
    private final static double OPACITY_NOT_PLAYABLE = 0.2;
    private final static int GAUSSIAN_EFFECT_CTE = 4;

    private final PlayerId ownId;
    private final Map<PlayerId, String> mapimap;
    private final ScoreBean scores;
    private final TrickBean trick;
    private final HandBean hand;
    private final ArrayBlockingQueue<Card> queue;

    private final Map<Integer, List<Integer>> parametersForPlacingPlayersCards;

    private final ObservableMap<Card, Image> cardMap;
    private final ObservableMap<Card, Image> cardHandMap;
    private final ObservableMap<Color, Image> trumpMap;

    /**
     * Constructeur de Graphical Player
     * 
     * @param id
     *            : Id du joueur dont on crée l'interface visuelle
     * @param namesTable
     *            : Table associative associant à chaque joueur son nom
     * @param score
     *            : Bean du score qui contient toutes les propriétées liées au
     *            score (qui évoluent au fil du temps)
     * @param plis
     *            : Bean du trick qui contient toutes les propriétées liées au
     *            plis (qui évoluent avec le temps)
     * @param hand
     *            : Bean de la main qui contient toutes les propriétées liées à
     *            la main (qui évoluent au fil du temps)
     * @param queue
     *            : Queue bloquante qui sert à communiquer quelle carte veur
     *            jouer le jouer graphique
     */
    public GraphicalPlayer(PlayerId id, Map<PlayerId, String> namesTable,
            ScoreBean score, TrickBean plis, HandBean hand,
            ArrayBlockingQueue<Card> queue) {
        ownId = id;
        mapimap = Collections.unmodifiableMap(namesTable);
        scores = score;
        trick = plis;
        this.hand = hand;
        cardMap = FXCollections.observableHashMap();
        trumpMap = FXCollections.observableHashMap();
        cardHandMap = FXCollections.observableHashMap();
        this.queue = queue;
        for (Color c : Color.values()) {
            trumpMap.put(c, trumpToImage(c));
            for (Rank r : Rank.values()) {
                cardMap.put(Card.of(c, r), cardToImage(Card.of(c, r), false));
                cardHandMap.put(Card.of(c, r),
                        cardToImage(Card.of(c, r), true));
            }
        }
        parametersForPlacingPlayersCards = buildMapOfParameters();
    }

    /**
     * Méthode qui s'occupe " d'assembler " tout les panes entre eux pour avoir
     * le graphisme souhaité
     * 
     * @return Stage : La scène finale (qui sera affichée à l'écran)
     */
    public Stage createStage() {
        Stage s = new Stage();
        BorderPane o = new BorderPane(createTrickPanel(ownId, mapimap, trick),
                createScorePanel(scores, mapimap), null, createHandPane(hand),
                null);
        StackPane l = new StackPane(createVictoryPanes(mapimap, scores, trick),
                o);
        o.visibleProperty()
                .bind(Bindings.when(scores.winningTeamProperty().isNotNull())
                        .then(false).otherwise(true));
        Scene scene = new Scene(l);
        s.setScene(scene);
        return s;
    }

    /**
     * Méthode qui s'occupe de modifier la propriété passée en argument en
     * fonction de l'évolution des points du tour (en ajoutant un Listener à
     * cette dernière propriétée)
     * 
     * @param p
     *            : Propriétée qui doit évoluer (elle sera afficher dans le
     *            panneau des scores pour montrer les points du plis
     * @param id
     *            : Id de la team à qui est liée la propriété p
     * @param s
     *            : Bean du score qui contient toutes les propriétées liées au
     *            score (qui évoluent au fil du temps)
     * @param l
     *            : Liste de propriétées dans laquelle la méthode va ajouter la
     *            propriété p après avoir fait ce qu'elle devait faire
     */
    private void initTrickPointsProperty(SimpleStringProperty p, TeamId id,
            ScoreBean s, List<SimpleStringProperty> l) {
        s.turnPointsProperty(id)
                .addListener((observable, oldValue, newValue) -> {
                    int difference = newValue.intValue() - oldValue.intValue();
                    if (difference >= 0)
                        p.set("(+" + (difference) + ')');
                    else
                        p.set("");
                });
        l.add(p);
    }

    /**
     * Méthode s'occupant de créer le panneau des scores (celui qui affiche les
     * scores de chaque équipe)
     * 
     * @param score
     *            : Bean du score qui contient toutes les propriétées liées au
     *            score (qui évoluent au fil du temps)
     * @param table
     *            : Table associative reliant à chaque joueur son nom
     * @return ScorePane : Le panneaa des scores construit
     */
    private Pane createScorePanel(ScoreBean score,
            Map<PlayerId, String> table) {

        GridPane scorePane = new GridPane();
        List<SimpleStringProperty> listOfTrickPoints = new ArrayList<>();

        SimpleStringProperty trickPointTeam1 = new SimpleStringProperty();
        SimpleStringProperty trickPointTeam2 = new SimpleStringProperty();

        // Ici on crée la liaison entre deux propriétées pour pouvoir afficher
        // les points du plis
        initTrickPointsProperty(trickPointTeam1, TeamId.TEAM_1, score,
                listOfTrickPoints);
        initTrickPointsProperty(trickPointTeam2, TeamId.TEAM_2, score,
                listOfTrickPoints);

        // boucle s'occupant d'ajouter tout les éléments au panneau principal du
        // score
        // Elle fait 10 itérations car il y a en tout 5 éléments par équipe
        // (donc 5 * 2 = 10)
        // Les conditions font des échelons de deux à deux, pour que chaque
        // équipe ait son élément
        for (int i = 0; i < NUMBER_OF_ELEMENTS_IN_SCOREPANE; i++) {
            Text t = new Text();
            if (i < 2) {
                t.textProperty().setValue(table.get(PlayerId.values()[i])
                        + " et " + table.get(PlayerId.values()[i + 2]) + " : ");
            }
            if (i < 4 && i > 1) {
                t.textProperty().bind(Bindings.convert(
                        score.turnPointsProperty(TeamId.values()[i % 2])));
            }
            if (i < 6 && i > 3) {
                t.textProperty().bind(listOfTrickPoints.get(i % 2));
            }
            if (i < 8 && i > 5) {
                t.textProperty().set(" /Total : ");
            }
            if (i < 10 && i > 7) {
                t.textProperty().bind(Bindings.convert(
                        score.totalPointsProperty(TeamId.values()[i % 2])));
            }
            scorePane.add(t, (int) i / 2, i % 2);
            GridPane.setHalignment(t, HPos.RIGHT);
        }
        scorePane.setStyle(
                "-fx-font: 16 Optima; -fx-background-color: lightgray; -fx-padding: 5px; -fx-alignment: center");
        return scorePane;
    }

    /**
     * Méthode permettant de créer le panneau du plis courant(c'est à dire
     * l'aire de jeux où se trouvera les cartes du plis, l'atout ainsi que le
     * nom des joueurs).
     * 
     * @param me
     *            : Id du joueur correspondant à celui qui joues .
     * @param map
     *            : Table associative reliant à chaque joueur son nom
     * @param trick
     *            : Bean du trick qui contient toutes les propriétées liées au
     *            plis (qui évoluent avec le temps)
     * @return TrickPane : Le panneau du plis construit
     */
    private Pane createTrickPanel(PlayerId me, Map<PlayerId, String> map,
            TrickBean trick) {

        // création de toutes les imagesView pour chaque joueurs
        List<ImageView> imageViewsByPlayer = new ArrayList<>();
        for (int j = 0; j < PlayerId.COUNT; j++) {
            ImageView view = new ImageView();
            view.imageProperty()
                    .bind(Bindings.valueAt(cardMap,
                            Bindings.valueAt(trick.trick(),
                                    PlayerId.values()[(me.ordinal() + j)
                                            % PlayerId.COUNT])));
            view.setFitHeight(CARD_IMAGE_TRICK_HEIGTH);
            view.setFitWidth(CARD_IMAGE_TRICK_WIDTH);
            imageViewsByPlayer.add(view);
        }

        // création du panneau du trick principal
        GridPane trickPane = new GridPane();
        trickPane.setStyle("-fx-background-color: whitesmoke;\n"
                + "-fx-padding: 5px;\n" + "-fx-border-width: 3px 0px;\n"
                + "-fx-border-style: solid;\n" + "-fx-border-color: gray;\n"
                + "-fx-alignment: center;");

        // Initialisation de l'image de l'atout
        ImageView trump = new ImageView();
        trump.imageProperty()
                .bind(Bindings.valueAt(trumpMap, trick.trumpProperty()));
        trump.setFitHeight(TRUMP_IMAGE_SIZE);
        trump.setFitWidth(TRUMP_IMAGE_SIZE);
        trickPane.add(trump, 1, 1);
        GridPane.setHalignment(trump, HPos.CENTER);

        // boucle s'occupant de créer tout les éléments à ajouter dans
        // l'interface principale du plis
        for (int z = 0; z < PlayerId.COUNT; z++) {
            Text text = new Text(map.get(PlayerId.values()[(me.ordinal() + z) % PlayerId.COUNT])); 
            text.setStyle("-fx-font: 14 Optima;");
            Rectangle rBlur = new Rectangle(CARD_IMAGE_TRICK_WIDTH,
                    CARD_IMAGE_TRICK_HEIGTH);
            rBlur.setStyle(
                    "-fx-arc-width: 20;-fx-arc-height: 20;-fx-fill: transparent;-fx-stroke: lightpink;-fx-stroke-width: 5;-fx-opacity: 0.5;");
            rBlur.setEffect(new GaussianBlur(GAUSSIAN_EFFECT_CTE));
            rBlur.visibleProperty().bind(trick.winningPlayerProperty()
                    .isEqualTo(PlayerId.values()[(me.ordinal() + z) % PlayerId.COUNT]));
            VBox vb;
            StackPane stack = new StackPane(rBlur, imageViewsByPlayer.get(z));
            if (PlayerId.values()[(me.ordinal() + z) % PlayerId.COUNT] == me)
                vb = new VBox(stack, text);
            else
                vb = new VBox(text, stack);
            vb.setStyle("-fx-padding: 5px;-fx-alignment: center;");
            // le z%2 ici sert à savoir dans quel équipe on se trouve
            if (z % TeamId.COUNT == 0)
                trickPane.add(vb,
                        parametersForPlacingPlayersCards.get(z).get(0),
                        parametersForPlacingPlayersCards.get(z).get(1));
            else
                trickPane.add(vb,
                        parametersForPlacingPlayersCards.get(z).get(0),
                        parametersForPlacingPlayersCards.get(z).get(1),
                        parametersForPlacingPlayersCards.get(z).get(2),
                        parametersForPlacingPlayersCards.get(z).get(3));
        }

        return trickPane;

    }

    /**
     * Méthode s'occupant d'initialiser (dans le constructeur) la table
     * associative qui associe à chaque joueur(qui correspond à un index dans la
     * map) une liste de paramètres qui sont ceux à mettre dans la méthode add
     * du trickpane pour être sur de leut bon emplacement
     * 
     * @return mapofParameters : La table associative
     */
    private Map<Integer, List<Integer>> buildMapOfParameters() {
        Map<Integer, List<Integer>> mapOfParameters = new HashMap<>();
        mapOfParameters.put(0, new ArrayList<Integer>(Arrays.asList(1, 2)));
        mapOfParameters.put(1,
                new ArrayList<Integer>(Arrays.asList(2, 0, 1, 3)));
        mapOfParameters.put(2, new ArrayList<Integer>(Arrays.asList(1, 0)));
        mapOfParameters.put(3,
                new ArrayList<Integer>(Arrays.asList(0, 0, 1, 3)));
        return Collections.unmodifiableMap(mapOfParameters);
    }

    /**
     * Méthode attribuant à chaque carte son image
     * 
     * @param c
     *            : La carte dont il faut renvoyer l'image
     * @param isInHand
     *            : Un boolean qui indique si cette carte est dans notre main ou
     *            non (pour changer la résolution de l'image)
     * @return Image : l'image de la carte
     */
    private Image cardToImage(Card c, boolean isInHand) {
        if (c == null)
            return null;
        if (!isInHand)
            return new Image("/card_" + c.color().ordinal() + "_"
                    + c.rank().ordinal() + "_240.png");
        return new Image("/card_" + c.color().ordinal() + "_"
                + c.rank().ordinal() + "_160.png");
    }

    /**
     * Méthode semblable à cardToImage, qui ici renvoie l'image de l'atout donné
     * 
     * @param c
     *            : Couleur de l'atout
     * @return Image : l'image correspondant à l'atout donné
     */
    private Image trumpToImage(Color c) {
        return new Image("/trump_" + c.ordinal() + ".png");
    }

    /**
     * Méthode permettant d'initialiser les paneau de victoire
     * 
     * @param table
     *            : Table associative reliant à chaque joueur son nom
     * @param score
     *            : Bean du score qui contient toutes les propriétées liées au
     *            score (qui évoluent au fil du temps)
     * @param trick
     *            : Bean du trick qui contient toutes les propriétées liées au
     *            plis (qui évoluent avec le temps)
     * @return StackPane : Un stackPane dans lequel se trouve les deux panneaux
     *         de victoire
     */
    private Pane createVictoryPanes(Map<PlayerId, String> table,
            ScoreBean score, TrickBean trick) {
        StackPane lastPane = new StackPane();

        for (int i = 0; i < TeamId.COUNT; i++) {
            Text text = new Text();
            text.textProperty()
                    .bind(Bindings.format(
                            ("%s et %s ont gagné avec %d contre %d."),
                            table.get(PlayerId.values()[i]),
                            table.get(PlayerId.values()[i + 2]),
                            score.totalPointsProperty(TeamId.values()[i]),
                            score.totalPointsProperty(
                                    TeamId.values()[(i + 1) % TeamId.COUNT])));
            BorderPane victoryPane = new BorderPane(text);
            victoryPane.setStyle(
                    "-fx-font: 16 Optima; -fx-background-color: white;");
            victoryPane.setCenter(text);
            victoryPane.visibleProperty()
                    .bind(Bindings
                            .when(score.winningTeamProperty()
                                    .isEqualTo(TeamId.values()[i]))
                            .then(true).otherwise(false));
            lastPane.getChildren().add(victoryPane);
        }

        return lastPane;
    }

    /**
     * Méthode qui s'occupe de créer le panneau représentant la main du joueur.
     * 
     * @param hand
     *            : Bean de la main qui contient toutes les propriétées liées à
     *            la main (qui évoluent au fil du temps)
     * @return Hbox : Panneau de la main
     */
    private Pane createHandPane(HandBean hand) {
        HBox hbb = new HBox();
        for (int i = 0; i < Card.Rank.COUNT; i++) {
            ImageView cardView = new ImageView();
            cardView.imageProperty().bind(Bindings.valueAt(cardHandMap,
                    Bindings.valueAt(hand.hand(), i)));
            final int final_I = i;
            BooleanProperty isPlayable = new SimpleBooleanProperty();
            isPlayable.bind(Bindings.createBooleanBinding(() -> {
                return hand.playableCards().contains(hand.hand().get(final_I));
            }, hand.playableCards(), hand.hand()));
            cardView.setOnMouseClicked(e -> {
                if (isPlayable.get() && queue.isEmpty()) {
                    queue.add(hand.hand().get(final_I));
                }
            });
            cardView.setFitHeight(CARD_IMAGE_HAND_HEIGTH);
            cardView.setFitWidth(CARD_IMAGE_HAND_WIDTH);
            cardView.opacityProperty().bind(Bindings.when(isPlayable)
                    .then(OPACITY_PLAYABLE).otherwise(OPACITY_NOT_PLAYABLE));
            hbb.getChildren().add(cardView);
        }
        hbb.setStyle(
                "-fx-background-color: lightgray; -fx-spacing: 5px; -fx-padding: 5px;");
        return hbb;
    }

}