package ch.epfl.javass.gui;

import ch.epfl.javass.jass.Card;
import ch.epfl.javass.jass.CardSet;
import ch.epfl.javass.jass.Jass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

/**
 * La classe est un bean JavaFX contenant les scores et plusieurs autres
 * propriétés
 * 
 * @author Shayan
 *
 */
public final class HandBean {
    // Les propriétés
    private final ObservableList<Card> hand;
    private final ObservableSet<Card> playableCards;

    /**
     * Le constructeur de classe
     */
    public HandBean() {
        hand = FXCollections.observableArrayList();
        for (int i = 0; i < Card.Rank.COUNT; i++) {
            hand.add(null);
        }
        playableCards = FXCollections.observableSet();
    }

    /**
     * Méthode qui return la propriété de hand
     * 
     * @return : ObservableList<Card> : L'ensemble des cartes de la main
     */
    public ObservableList<Card> hand() {
        return FXCollections.unmodifiableObservableList(hand);
    }

    /**
     * Méthode qui modifie la valeur de hand
     * 
     * @param newHand : Nouvelle main
     */
    public void setHand(CardSet newHand) {
        if (newHand.isEmpty()) {
            for (int i = 0; i < Card.Rank.COUNT; i++) {
                hand.set(i, null);
            }
        } else {
            if (newHand.size() == Jass.HAND_SIZE) {
                for (int i = 0; i < newHand.size(); i++) {
                    hand.set(i, newHand.get(i));
                }
            } else {
                for (int i = 0; i < Jass.HAND_SIZE; i++) {
                    if (hand.get(i) != null && !newHand.contains(hand.get(i))) {
                        hand.set(i, null);
                    }
                }
            }
        }
    }

    /**
     * Méthode qui return la propriété de playableCards
     * 
     * @return
     */
    public ObservableSet<Card> playableCards() {
        return FXCollections.unmodifiableObservableSet(playableCards);
    }

    /**
     * Méthode qui modifie la valeur de playableCards
     * 
     * @param newPlayableCards : Ensembles des cartes jouables
     */
    public void setPlayableCards(CardSet newPlayableCards) {
        if (newPlayableCards.isEmpty())
            playableCards.clear();
        else {
            playableCards.clear();
            for (int i = 0; i < newPlayableCards.size(); i++) {
                playableCards.add(newPlayableCards.get(i));
            }
        }

    }
}
