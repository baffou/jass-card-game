package ch.epfl.javass;

import ch.epfl.javass.gui.GraphicalPlayerAdapter;
import ch.epfl.javass.net.RemotePlayerServer;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Classe servant à se connecter à une partie distante
 * @author Jérémy
 *
 */
public class RemoteMain extends Application{
    
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        RemotePlayerServer pl = new RemotePlayerServer(new GraphicalPlayerAdapter()); 
        
        Thread t = new Thread(() -> {
            pl.run();
        });
        t.setDaemon(true);
        t.start();
    }
      
}
