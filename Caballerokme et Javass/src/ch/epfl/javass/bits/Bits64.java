package ch.epfl.javass.bits;

import ch.epfl.javass.Preconditions;

public final class Bits64 {
    
    //constructeur par défault privé pour la rendre non instanciable
    private Bits64() {}
    
    /**
     * @author Jérémy
     * 
     * @param start : index du bit de départ 
     *        size  : taille du masque
     * 
     * @return sequence : un long dont les bits d'index start à size+start (non inclus) 
     *                    valent 1 et les autres 0
     *
     * @throws: IllegalArgumentException si start et size ne désignent pas une plage de bits valident 
     */
    public static long mask(int start, int size) {
        Preconditions.checkArgument(start + size <= Long.SIZE && size >= 0 && start >= 0);
        long sequence = 0;
        for (int i = start; i < start + size; i++) {
            long maskVar = 1L << i;
            sequence = sequence | maskVar;
        }
        return sequence;
    }
    
    
    /**
     * @author Jérémy
     * @param bits : Entier dont on va extraire certains bits
     * @param start : index du bit de départ
     * @param size : nb de bits à extraire
     * @return extracted : un entier dont les size bits de poids faibles sont égaux à ceux
     *                     de bits (les autres valant 0)
     */
    public static long extract(long bits, int start, int size) {
        long extracted = bits & mask(start, size);
        extracted = extracted >>> start;
        return extracted;
    }

    /**
     * @author Jérémy
     * @param v1 : premier entier à empaqueter sur les s1 bits de poid faible
     * @param s1 : taille que prend le premier entier dans packed
     * @param v2 : second entier à empqueter sur les s2 bits suivants les s1 premiers bits
     * @param s2 : taille que prend le second entier dans packed
     * @return packed : un entier dans lequel v1 et v2 ont été empaquetés
     */
    public static long pack(long v1, int s1, long v2, int s2) {
        Preconditions.checkArgument(s1 + s2 <= Long.SIZE && check(s1, v1) && check(s2, v2));

        long packed = v1 | (v2 << s1);
        return packed;
    }

    /**
     * @author Jérémy
     * @param size : taille de l'entier à tester
     * @param value : valeur de l'entier à tester
     * @return un boolean si la taille est comprise entre 1 et 64(inclus et si la valeur n'
     *         occuper pas plus de bits que ne précise la taille
     */
    private static boolean check(int size, long value) {
        if (size <= 0 || size >= Long.SIZE) {
            return false;
        }
        long masque = mask(0, size);
        if ((value & masque) == value) {
            return true;
        }

        return false;
    }
}