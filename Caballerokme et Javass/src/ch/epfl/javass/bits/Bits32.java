package ch.epfl.javass.bits;

import ch.epfl.javass.Preconditions;

public final class Bits32 {

	// constructeur par défault privé pour la rendre non instanciable
	private Bits32() {
	}

	/**
	 * @author Jérémy
	 * 
	 * @param start
	 *            : index du bit de départ size : taille du masque
	 * 
	 * @return sequence : un int dont les bits d'index start à size+start (non
	 *         inclus) valent 1 et les autres 0
	 *
	 * @throws: IllegalArgumentException
	 *              si start et size ne désignent pas une plage de bits valident
	 */
	public static int mask(int start, int size) {
		Preconditions.checkArgument(!(start + size > Integer.SIZE || size < 0 || start < 0));
		int sequence = 0;
		for (int i = start; i < start + size; i++) {
			int maskVar = 1 << i;
			sequence = sequence | maskVar;
		}
		return sequence;
	}

	/**
	 * @author Jérémy
	 * @param bits
	 *            : Entier dont on va extraire certains bits
	 * @param start
	 *            : index du bit de départ
	 * @param size
	 *            : nb de bits à extraire
	 * @return extracted : un entier dont les size bits de poids faibles sont égaux
	 *         à ceux de bits (les autres valant 0)
	 */
	public static int extract(int bits, int start, int size) {
		int extracted = bits & mask(start, size);
		extracted = extracted >>> start;
		return extracted;
	}

	/**
	 * @author Jérémy
	 * @param v1
	 *            : premier entier à empaqueter sur les s1 bits de poid faible
	 * @param s1
	 *            : taille que prend le premier entier dans packed
	 * @param v2
	 *            : second entier à empqueter sur les s2 bits suivants les s1
	 *            premiers bits
	 * @param s2
	 *            : taille que prend le second entier dans packed
	 * @return packed : un entier dans lequel v1 et v2 ont été empaquetés
	 */
	public static int pack(int v1, int s1, int v2, int s2) {
		Preconditions.checkArgument(s1 + s2 <= Integer.SIZE && check(s1, v1) && check(s2, v2));

		int packed = v1 | (v2 << s1);
		return packed;
	}

	// même méthode que la premiere methode pack sauf qu'elle empaquette 3 valeurs
	// au lieu d'une
	public static int pack(int v1, int s1, int v2, int s2, int v3, int s3) {
		Preconditions
				.checkArgument(!(s1 + s2 + s3 > Integer.SIZE || !check(s1, v1) || !check(s2, v2) || !check(s3, v3)));
		int packed = v1 | (v2 << s1) | (v3 << (s1 + s2));
		return packed;
	}

	// même méthode que la premiere methode pack sauf qu'elle empaquette 7 au lieu
	// d'une
	public static int pack(int v1, int s1, int v2, int s2, int v3, int s3, int v4, int s4, int v5, int s5, int v6,
			int s6, int v7, int s7) {
		Preconditions
				.checkArgument(!(s1 + s2 + s3 + s4 + s5 + s6 + s7 > Integer.SIZE || !check(s1, v1) || !check(s2, v2)
						|| !check(s3, v3) || !check(s4, v4) || !check(s5, v5) || !check(s6, v6) || !check(s7, v7)));

		int packed = v1 | (v2 << s1) | (v3 << (s1 + s2)) | (v4 << (s1 + s2 + s3)) | (v5 << (s1 + s2 + s3 + s4))
				| (v6 << (s1 + s2 + s3 + s4 + s5)) | (v7 << (s1 + s2 + s3 + s4 + s5 + s6));
		return packed;
	}

	/**
	 * @author Jérémy
	 * @param size
	 *            : taille de l'entier à tester
	 * @param value
	 *            : valeur de l'entier à tester
	 * @return un boolean si la taille est comprise entre 1 et 64(inclus et si la
	 *         valeur n' occuper pas plus de bits que ne précise la taille
	 */
	private static boolean check(int size, int value) {
		if (size <= 0 || size >= Integer.SIZE) {
			return false;
		}
		int masque = mask(0, size);
		if ((value & masque) == value) {
			return true;
		}

		return false;
	}
}