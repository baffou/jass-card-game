package ch.epfl.javass;


/**
 * Une précondition
 * 
 * @author Shayan
 */
public final class Preconditions {
    private Preconditions() {}
    /**
     * Vérifie qu'un boolean b est true
     * @param b
     * @throws IllegalArgumentException si b n'est pas true
     */
    public static void checkArgument(boolean b) {
        if(!b) {
            throw new IllegalArgumentException();
        }
    }
    /**
     * Vérifie qu'on a le droit de chercher qqch à un certain index
     * @param index : l'index recherché
     * @param size : la taille de l'objet dans lequel on recherche l'index
     * @throws IndexOutOfBoundsException si l'index est plus petit que 0 ou si l'index est plus grand que la taille
     * @return l'index
     */
    public static int checkIndex(int index, int size) {
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return index;
    } 
}
